-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 22, 2014 at 05:12 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `fetch_staging`
--
CREATE DATABASE IF NOT EXISTS `fetch_staging` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `fetch_staging`;

-- --------------------------------------------------------

--
-- Table structure for table `alert_templates`
--

CREATE TABLE IF NOT EXISTS `alert_templates` (
  `template_id` int(11) NOT NULL AUTO_INCREMENT,
  `template_title` varchar(50) NOT NULL,
  `template_text` text NOT NULL,
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `alert_templates`
--

INSERT INTO `alert_templates` (`template_id`, `template_title`, `template_text`) VALUES
(1, 'Fetch Request Accepted', '##user_name## has accepted your fetch request.'),
(2, 'Fetch Request Rejected', '##user_name## has rejected your fetch request.'),
(3, 'Fetch Request Cancelled', '##user_name## has cancelled fetch request.'),
(4, 'Fetch Request Initiate', '##user_name## initiated fetch request.'),
(5, 'Fetch Request Received', '##user_name## sent you a fetch request.'),
(6, 'Fetch Request Modified', '##user_name## has modified the fetch request.');

-- --------------------------------------------------------

--
-- Table structure for table `ccg_admin`
--

CREATE TABLE IF NOT EXISTS `ccg_admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(100) NOT NULL COMMENT 'Admin Name',
  `admin_email` varchar(200) NOT NULL COMMENT 'Admin Email',
  `admin_password` varchar(200) NOT NULL COMMENT 'Admin Password',
  `admin_type` enum('1','2') NOT NULL COMMENT 'Admin Type',
  `admin_status` enum('0','1') NOT NULL COMMENT 'Admin Status',
  `admin_login_time` datetime DEFAULT NULL COMMENT 'Last Login Time',
  `admin_login_ip` varchar(20) DEFAULT NULL COMMENT 'Last Login IP Address',
  `admin_logout_time` datetime DEFAULT NULL COMMENT 'Last Logout Time',
  `admin_creation_date` datetime NOT NULL COMMENT 'Admin Creation Date',
  `admin_updation_date` datetime NOT NULL COMMENT 'Admin Updation Date',
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ccg_admin`
--

INSERT INTO `ccg_admin` (`admin_id`, `admin_name`, `admin_email`, `admin_password`, `admin_type`, `admin_status`, `admin_login_time`, `admin_login_ip`, `admin_logout_time`, `admin_creation_date`, `admin_updation_date`) VALUES
(1, 'Admin', 'admin@fetch-apps.com', '0192023a7bbd73250516f069df18b500', '1', '1', '2013-12-16 10:29:40', '182.237.168.109', NULL, '2013-12-14 12:46:00', '2013-12-16 10:29:40');

-- --------------------------------------------------------

--
-- Table structure for table `ccg_countries`
--

CREATE TABLE IF NOT EXISTS `ccg_countries` (
  `cc_id` int(5) NOT NULL AUTO_INCREMENT,
  `cc_country` varchar(80) NOT NULL DEFAULT '',
  `cc_code` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`cc_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=251 ;

--
-- Dumping data for table `ccg_countries`
--

INSERT INTO `ccg_countries` (`cc_id`, `cc_country`, `cc_code`) VALUES
(1, 'Afghanistan', '93'),
(3, 'Albania', '355'),
(4, 'Algeria', '213'),
(6, 'Andorra', '376'),
(7, 'Angola', '244'),
(9, 'Antarctica', '672'),
(11, 'Argentina', '54'),
(12, 'Armenia', '374'),
(13, 'Aruba', '297'),
(14, 'Australia', '61'),
(15, 'Austria', '43'),
(16, 'Azerbaijan', '994'),
(18, 'Bahrain', '973'),
(19, 'Bangladesh', '880'),
(21, 'Belarus', '375'),
(22, 'Belgium', '32'),
(23, 'Belize', '501'),
(24, 'Benin', '229'),
(26, 'Bhutan', '975'),
(27, 'Bolivia', '591'),
(29, 'Bosnia and Herzegovina', '387'),
(30, 'Botswana', '267'),
(32, 'Brazil', '55'),
(35, 'Bulgaria', '359'),
(36, 'Burkina Faso', '226'),
(37, 'Burundi', '257'),
(38, 'Cambodia', '855'),
(39, 'Cameroon', '237'),
(40, 'Canada', '1'),
(41, 'Cape Verde', '238'),
(43, 'Central African Republic', '236'),
(44, 'Chad', '235'),
(45, 'Chile', '56'),
(46, 'China', '86'),
(47, 'Christmas Island', '61'),
(48, 'Cocos (Keeling) Islands', '61'),
(49, 'Colombia', '57'),
(50, 'Comoros', '269'),
(51, 'Congo', '242'),
(52, 'Cook Islands', '682'),
(53, 'Costa Rica', '506'),
(55, 'Croatia', '385'),
(56, 'Cuba', '53'),
(58, 'Cyprus', '357'),
(59, 'Czech Republic', '420'),
(61, 'Denmark', '45'),
(62, 'Djibouti', '253'),
(65, 'Ecuador', '593'),
(66, 'Egypt', '20'),
(67, 'El Salvador', '503'),
(68, 'Equatorial Guinea', '240'),
(69, 'Eritrea', '291'),
(70, 'Estonia', '372'),
(71, 'Ethiopia', '251'),
(72, 'Falkland Islands (Malvinas)', '500'),
(73, 'Faroe Islands', '298'),
(74, 'Fiji', '679'),
(75, 'Finland', '358'),
(76, 'France', '33'),
(78, 'French Polynesia', '689'),
(80, 'Gabon', '241'),
(81, 'Gambia', '220'),
(82, 'Georgia', '995'),
(83, 'Germany', '49'),
(84, 'Ghana', '233'),
(85, 'Gibraltar', '350'),
(86, 'Greece', '30'),
(87, 'Greenland', '299'),
(91, 'Guatemala', '502'),
(93, 'Guinea', '224'),
(94, 'Guinea-Bissau', '245'),
(95, 'Guyana', '592'),
(96, 'Haiti', '509'),
(98, 'Honduras', '504'),
(99, 'Hong Kong', '852'),
(100, 'Hungary', '36'),
(102, 'India', '91'),
(103, 'Indonesia', '62'),
(104, 'Iran', '98'),
(105, 'Iraq', '964'),
(106, 'Ireland', '353'),
(107, 'Isle of Man', '44'),
(108, 'Israel', '972'),
(109, 'Italy', '39'),
(111, 'Japan', '81'),
(113, 'Jordan', '962'),
(114, 'Kazakhstan', '7'),
(115, 'Kenya', '254'),
(116, 'Kiribati', '686'),
(118, 'Kuwait', '965'),
(119, 'Kyrgyzstan', '996'),
(120, 'Laos', '856'),
(121, 'Latvia', '371'),
(122, 'Lebanon', '961'),
(123, 'Lesotho', '266'),
(124, 'Liberia', '231'),
(125, 'Libya', '218'),
(126, 'Liechtenstein', '423'),
(127, 'Lithuania', '370'),
(128, 'Luxembourg', '352'),
(129, 'Macao', '853'),
(130, 'Macedonia', '389'),
(131, 'Madagascar', '261'),
(132, 'Malawi', '265'),
(133, 'Malaysia', '60'),
(134, 'Maldives', '960'),
(135, 'Mali', '223'),
(136, 'Malta', '356'),
(137, 'Marshall Islands', '692'),
(139, 'Mauritania', '222'),
(140, 'Mauritius', '230'),
(141, 'Mayotte', '262'),
(142, 'Mexico', '52'),
(143, 'Micronesia', '691'),
(145, 'Monaco', '377'),
(146, 'Mongolia', '976'),
(147, 'Montenegro', '382'),
(149, 'Morocco', '212'),
(150, 'Mozambique', '258'),
(152, 'Namibia', '264'),
(153, 'Nauru', '674'),
(154, 'Nepal', '977'),
(155, 'Netherlands', '31'),
(156, 'New Caledonia', '687'),
(157, 'New Zealand', '64'),
(158, 'Nicaragua', '505'),
(159, 'Niger', '227'),
(160, 'Nigeria', '234'),
(161, 'Niue', '683'),
(165, 'Norway', '47'),
(166, 'Oman', '968'),
(167, 'Pakistan', '92'),
(168, 'Palau', '680'),
(170, 'Panama', '507'),
(171, 'Papua New Guinea', '675'),
(172, 'Paraguay', '595'),
(173, 'Peru', '51'),
(176, 'Poland', '48'),
(177, 'Portugal', '351'),
(179, 'Qatar', '974'),
(181, 'Romania', '40'),
(183, 'Rwanda', '250'),
(184, 'Saint Barthelemy', '590'),
(185, 'Saint Helena', '290'),
(189, 'Saint Pierre and Miquelon', '508'),
(191, 'Samoa', '685'),
(192, 'San Marino', '378'),
(193, 'Sao Tome and Principe', '239'),
(194, 'Saudi Arabia', '966'),
(195, 'Senegal', '221'),
(196, 'Serbia', '381'),
(197, 'Seychelles', '248'),
(198, 'Sierra Leone', '232'),
(199, 'Singapore', '65'),
(201, 'Slovakia', '421'),
(202, 'Slovenia', '386'),
(203, 'Solomon Islands', '677'),
(204, 'Somalia', '252'),
(205, 'South Africa', '27'),
(209, 'Spain', '34'),
(210, 'Sri Lanka', '94'),
(211, 'Sudan', '249'),
(212, 'Suriname', '597'),
(214, 'Swaziland', '268'),
(215, 'Sweden', '46'),
(216, 'Switzerland', '41'),
(218, 'Taiwan', '886'),
(219, 'Tajikistan', '992'),
(220, 'Tanzania', '255'),
(221, 'Thailand', '66'),
(223, 'Togo', '228'),
(224, 'Tokelau', '690'),
(225, 'Tonga', '676'),
(227, 'Tunisia', '216'),
(228, 'Turkey', '90'),
(229, 'Turkmenistan', '993'),
(231, 'Tuvalu', '688'),
(232, 'Uganda', '256'),
(233, 'Ukraine', '380'),
(234, 'United Arab Emirates', '971'),
(235, 'United Kingdom', '44'),
(236, 'United States', '1'),
(238, 'Uruguay', '598'),
(239, 'Uzbekistan', '998'),
(240, 'Vanuatu', '678'),
(242, 'Venezuela', '58'),
(246, 'Wallis and Futuna', '681'),
(248, 'Yemen', '967'),
(249, 'Zambia', '260'),
(250, 'Zimbabwe', '263');

-- --------------------------------------------------------

--
-- Table structure for table `ccg_feedback`
--

CREATE TABLE IF NOT EXISTS `ccg_feedback` (
  `feedback_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `feedback_parent_id` bigint(20) NOT NULL,
  `feedback_user_id` bigint(20) NOT NULL,
  `feedback_admin_flg` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=Normal User, 1 = Admin',
  `feedback_description` text NOT NULL,
  `feedback_rating` tinyint(4) NOT NULL,
  `feedback_creation_date` datetime NOT NULL,
  PRIMARY KEY (`feedback_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This table is used to feedback & it''s comments' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `country_info`
--

CREATE TABLE IF NOT EXISTS `country_info` (
  `c_id` int(3) NOT NULL AUTO_INCREMENT,
  `c_area_code` int(4) NOT NULL,
  `c_text_code` text NOT NULL,
  `c_name` varchar(30) NOT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=445 ;

--
-- Dumping data for table `country_info`
--

INSERT INTO `country_info` (`c_id`, `c_area_code`, `c_text_code`, `c_name`) VALUES
(1, 93, '', ''),
(2, 355, '', ''),
(3, 213, '', ''),
(4, 376, '', ''),
(5, 244, '', ''),
(6, 672, '', ''),
(7, 54, '', ''),
(8, 374, '', ''),
(9, 297, '', ''),
(10, 61, '', ''),
(11, 43, '', ''),
(12, 994, '', ''),
(13, 973, '', ''),
(14, 880, '', ''),
(15, 375, '', ''),
(16, 32, '', ''),
(17, 501, '', ''),
(18, 229, '', ''),
(19, 975, '', ''),
(20, 591, '', ''),
(21, 387, '', ''),
(22, 267, '', ''),
(23, 55, '', ''),
(24, 673, '', ''),
(25, 359, '', ''),
(26, 226, '', ''),
(27, 95, '', ''),
(28, 257, '', ''),
(29, 855, '', ''),
(30, 237, '', ''),
(31, 1, '', ''),
(32, 238, '', ''),
(33, 236, '', ''),
(34, 235, '', ''),
(35, 56, '', ''),
(36, 86, '', ''),
(37, 61, '', ''),
(38, 61, '', ''),
(39, 57, '', ''),
(40, 269, '', ''),
(41, 242, '', ''),
(42, 682, '', ''),
(43, 506, '', ''),
(44, 385, '', ''),
(45, 53, '', ''),
(46, 357, '', ''),
(47, 420, '', ''),
(48, 45, '', ''),
(49, 253, '', ''),
(50, 670, '', ''),
(51, 593, '', ''),
(52, 20, '', ''),
(53, 503, '', ''),
(54, 240, '', ''),
(55, 291, '', ''),
(56, 372, '', ''),
(57, 251, '', ''),
(58, 500, '', ''),
(59, 298, '', ''),
(60, 679, '', ''),
(61, 358, '', ''),
(62, 33, '', ''),
(63, 689, '', ''),
(64, 241, '', ''),
(65, 220, '', ''),
(66, 995, '', ''),
(67, 49, '', ''),
(68, 233, '', ''),
(69, 350, '', ''),
(70, 30, '', ''),
(71, 299, '', ''),
(72, 502, '', ''),
(73, 224, '', ''),
(74, 245, '', ''),
(75, 592, '', ''),
(76, 509, '', ''),
(77, 504, '', ''),
(78, 852, '', ''),
(79, 36, '', ''),
(80, 91, '', ''),
(81, 62, '', ''),
(82, 98, '', ''),
(83, 964, '', ''),
(84, 353, '', ''),
(85, 44, '', ''),
(86, 972, '', ''),
(87, 39, '', ''),
(88, 225, '', ''),
(89, 1876, '', ''),
(90, 81, '', ''),
(91, 962, '', ''),
(92, 7, '', ''),
(93, 254, '', ''),
(94, 686, '', ''),
(95, 965, '', ''),
(96, 996, '', ''),
(97, 856, '', ''),
(98, 371, '', ''),
(99, 961, '', ''),
(100, 266, '', ''),
(101, 231, '', ''),
(102, 218, '', ''),
(103, 423, '', ''),
(104, 370, '', ''),
(105, 352, '', ''),
(106, 853, '', ''),
(107, 389, '', ''),
(108, 261, '', ''),
(109, 265, '', ''),
(110, 60, '', ''),
(111, 960, '', ''),
(112, 223, '', ''),
(113, 356, '', ''),
(114, 692, '', ''),
(115, 222, '', ''),
(116, 230, '', ''),
(117, 262, '', ''),
(118, 52, '', ''),
(119, 691, '', ''),
(120, 373, '', ''),
(121, 377, '', ''),
(122, 976, '', ''),
(123, 382, '', ''),
(124, 212, '', ''),
(125, 258, '', ''),
(126, 264, '', ''),
(127, 674, '', ''),
(128, 977, '', ''),
(129, 31, '', ''),
(130, 687, '', ''),
(131, 64, '', ''),
(132, 505, '', ''),
(133, 227, '', ''),
(134, 234, '', ''),
(135, 683, '', ''),
(136, 850, '', ''),
(137, 47, '', ''),
(138, 968, '', ''),
(139, 92, '', ''),
(140, 680, '', ''),
(141, 507, '', ''),
(142, 675, '', ''),
(143, 595, '', ''),
(144, 51, '', ''),
(145, 63, '', ''),
(146, 870, '', ''),
(147, 48, '', ''),
(148, 351, '', ''),
(149, 1, '', ''),
(150, 974, '', ''),
(151, 40, '', ''),
(152, 7, '', ''),
(153, 250, '', ''),
(154, 590, '', ''),
(155, 685, '', ''),
(156, 378, '', ''),
(157, 239, '', ''),
(158, 966, '', ''),
(159, 221, '', ''),
(160, 381, '', ''),
(161, 248, '', ''),
(162, 232, '', ''),
(163, 65, '', ''),
(164, 421, '', ''),
(165, 386, '', ''),
(166, 677, '', ''),
(167, 252, '', ''),
(168, 27, '', ''),
(169, 82, '', ''),
(170, 34, '', ''),
(171, 94, '', ''),
(172, 290, '', ''),
(173, 508, '', ''),
(174, 249, '', ''),
(175, 597, '', ''),
(176, 268, '', ''),
(177, 46, '', ''),
(178, 41, '', ''),
(179, 963, '', ''),
(180, 886, '', ''),
(181, 992, '', ''),
(182, 255, '', ''),
(183, 66, '', ''),
(184, 228, '', ''),
(185, 690, '', ''),
(186, 676, '', ''),
(187, 216, '', ''),
(188, 90, '', ''),
(189, 993, '', ''),
(190, 688, '', ''),
(191, 971, '', ''),
(192, 256, '', ''),
(193, 44, '', ''),
(194, 380, '', ''),
(195, 598, '', ''),
(196, 1, '', ''),
(197, 998, '', ''),
(198, 678, '', ''),
(199, 39, '', ''),
(200, 58, '', ''),
(201, 84, '', ''),
(202, 681, '', ''),
(203, 967, '', ''),
(204, 260, '', ''),
(205, 263, '', ''),
(444, 666, 'uu', 'yyyt'),
(333, 555, 'rr', 'ttttt');

-- --------------------------------------------------------

--
-- Table structure for table `fetch_chat`
--

CREATE TABLE IF NOT EXISTS `fetch_chat` (
  `fc_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fc_transaction_id` bigint(20) NOT NULL,
  `fc_from_user_id` int(20) NOT NULL,
  `fc_to_user_id` int(20) NOT NULL,
  `fc_preset_message` varchar(50) NOT NULL,
  `fc_created_date` datetime NOT NULL,
  PRIMARY KEY (`fc_id`),
  KEY `fc_transaction_id` (`fc_transaction_id`,`fc_from_user_id`,`fc_to_user_id`),
  KEY `fc_from_user_id` (`fc_from_user_id`,`fc_to_user_id`),
  KEY `fc_to_user_id` (`fc_to_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fetch_pals`
--

CREATE TABLE IF NOT EXISTS `fetch_pals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_FKregister_id` int(11) NOT NULL,
  `to_FKregister_id` int(11) NOT NULL,
  `c_date` datetime NOT NULL,
  `blocked` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Bolcked, 1 = Blocked',
  `favourite` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Not Favourite, 1=Favourite',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fetch_transaction_new`
--

CREATE TABLE IF NOT EXISTS `fetch_transaction_new` (
  `fetch_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fetch_flag` tinyint(4) NOT NULL,
  `fetch_creator_id` int(11) NOT NULL,
  `fetch_location_name` text NOT NULL,
  `fetch_status` tinyint(4) NOT NULL COMMENT '0 = pending, 1 = accepted, 2 =rejected, 3=cancelled ,  4=active',
  `fetch_from_lat` varchar(100) DEFAULT NULL,
  `fetch_from_longi` varchar(100) DEFAULT NULL,
  `fetch_curr_lat` varchar(100) DEFAULT NULL COMMENT 'Current position of Fetcher',
  `fetch_curr_longi` varchar(100) DEFAULT NULL COMMENT 'Current position of Fetcher',
  `fetch_to_lat` varchar(100) DEFAULT NULL,
  `fetch_to_longi` varchar(100) DEFAULT NULL,
  `fetch_datetime` datetime DEFAULT NULL,
  `fetch_timezone` varchar(50) NOT NULL,
  `fetch_message` text NOT NULL,
  `fetch_image` varchar(100) DEFAULT NULL,
  `fetch_creation_date` datetime NOT NULL,
  `fetch_updation_date` datetime DEFAULT NULL,
  PRIMARY KEY (`fetch_id`),
  KEY `fetch_creator_id` (`fetch_creator_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fetch_trasaction_users`
--

CREATE TABLE IF NOT EXISTS `fetch_trasaction_users` (
  `ftu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ftu_transaction_id` bigint(20) NOT NULL,
  `ftu_user_type` char(1) NOT NULL,
  `ftu_user_id` int(11) NOT NULL,
  `ftu_response` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ftu_id`),
  KEY `ftu_transaction_id` (`ftu_transaction_id`),
  KEY `ftu_user_id` (`ftu_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `group_master`
--

CREATE TABLE IF NOT EXISTS `group_master` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(100) NOT NULL,
  `group_owner` int(11) NOT NULL,
  `group_created_on` datetime NOT NULL,
  `group_updated_on` datetime NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `group_master`
--

INSERT INTO `group_master` (`group_id`, `group_name`, `group_owner`, `group_created_on`, `group_updated_on`) VALUES
(2, 'JuzFoods', 2, '2014-04-23 12:34:43', '2014-04-23 12:34:43');

-- --------------------------------------------------------

--
-- Table structure for table `group_members`
--

CREATE TABLE IF NOT EXISTS `group_members` (
  `gm_member_id` int(11) NOT NULL AUTO_INCREMENT,
  `gm_group_id` int(11) NOT NULL,
  `gm_user_id` int(11) NOT NULL,
  PRIMARY KEY (`gm_member_id`),
  KEY `gm_group_id` (`gm_group_id`),
  KEY `gm_group_id_2` (`gm_group_id`),
  KEY `gm_group_id_3` (`gm_group_id`),
  KEY `gm_group_id_4` (`gm_group_id`),
  KEY `gm_user_id` (`gm_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `group_members`
--

INSERT INTO `group_members` (`gm_member_id`, `gm_group_id`, `gm_user_id`) VALUES
(2, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `test_ios_devices`
--

CREATE TABLE IF NOT EXISTS `test_ios_devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `device_id` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users_app_info`
--

CREATE TABLE IF NOT EXISTS `users_app_info` (
  `pk_user_app_info_id` int(100) NOT NULL AUTO_INCREMENT,
  `email_id` varchar(100) NOT NULL,
  `password` varchar(200) CHARACTER SET utf8 NOT NULL,
  `country_code` varchar(10) NOT NULL,
  `phone_no` varchar(12) NOT NULL,
  `social_id` varchar(50) NOT NULL,
  `registered_through` varchar(50) NOT NULL,
  `phone_activation_code` int(10) NOT NULL,
  `phone_activation_status` int(5) NOT NULL DEFAULT '0',
  `download_counter` bigint(100) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`pk_user_app_info_id`),
  UNIQUE KEY `EmailId` (`email_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `users_app_info`
--

INSERT INTO `users_app_info` (`pk_user_app_info_id`, `email_id`, `password`, `country_code`, `phone_no`, `social_id`, `registered_through`, `phone_activation_code`, `phone_activation_status`, `download_counter`, `created_date`, `updated_date`) VALUES
(5, 'sagar@ccs.sg', 'YWRtaW4xMg==', '+91', '9028556654', '', '', 3002, 0, NULL, '2014-04-23 12:54:47', NULL),
(13, 'nachare.reena8@gmail.com', 'ZmV0Y2g=', '+91', '7776912913', '', '', 1610, 1, NULL, '2014-04-25 07:12:13', NULL),
(26, 'nachare.reena58@gmail.com', 'ZmV0Y2g=', '+91', '8308757523', '', '', 7418, 1, NULL, '2014-04-30 06:11:21', NULL),
(27, '', '', '+91', '', '', '', 4370, 0, NULL, '2014-04-30 06:34:47', NULL),
(28, 'nachare.neha@gmail.com', 'ZmV0Y2g=', '+91', '7575462312', '', '', 6061, 0, NULL, '2014-04-30 06:53:35', NULL),
(29, 'reena_nachare@gmail.com', 'ZmV0Y2g=', '+91', '8983142299', '', '', 7615, 1, NULL, '2014-04-30 06:56:57', NULL),
(30, 'michael.kenneth.mauleon@gmail.com', 'bWlrZTE5NzI=', '+65', '90059521', '', '', 9731, 1, NULL, '2014-04-30 07:40:24', NULL),
(31, 'victor@capstone.sg', 'dGlzc290MDQ=', '+65', '81575534', '', '', 3938, 1, NULL, '2014-04-30 11:24:38', NULL),
(32, 'victor@aspiregroup.sg', 'dGlzc290MDQ=', '+65', '97803593', '', '', 4262, 1, NULL, '2014-05-01 07:01:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_personal_info`
--

CREATE TABLE IF NOT EXISTS `users_personal_info` (
  `pk_user_persnl_info_Id` int(3) NOT NULL AUTO_INCREMENT,
  `fk_user_app_info_id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `city` varchar(25) NOT NULL,
  `state` varchar(25) NOT NULL,
  `country` varchar(25) NOT NULL,
  `address` varchar(25) NOT NULL,
  `photo` varchar(25) NOT NULL,
  `location` varchar(25) NOT NULL,
  `dob` date NOT NULL,
  `gender` int(2) NOT NULL COMMENT '0- male, 1- female',
  `accesstoken` varchar(150) NOT NULL,
  `carRegistrationNo` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
  `carModel` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `carColor` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `carDescription` text CHARACTER SET utf8,
  `carMake` varchar(20) NOT NULL,
  `carLicense` varchar(50) NOT NULL,
  `carPhoto` varchar(20) NOT NULL,
  PRIMARY KEY (`pk_user_persnl_info_Id`),
  KEY `fk_user_app_info_id` (`fk_user_app_info_id`),
  KEY `fk_user_app_info_id_2` (`fk_user_app_info_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `users_personal_info`
--

INSERT INTO `users_personal_info` (`pk_user_persnl_info_Id`, `fk_user_app_info_id`, `name`, `last_name`, `status`, `city`, `state`, `country`, `address`, `photo`, `location`, `dob`, `gender`, `accesstoken`, `carRegistrationNo`, `carModel`, `carColor`, `carDescription`, `carMake`, `carLicense`, `carPhoto`) VALUES
(4, 5, 'Sagar', 'Dhomane', '', '', '', 'India', '', '', '', '0000-00-00', 0, '', NULL, NULL, NULL, NULL, '', '', ''),
(13, 13, 'Reena', 'N', '', '', '', 'India', '', '', '', '0000-00-00', 0, '', NULL, NULL, NULL, NULL, '', '', ''),
(30, 26, 'Reena', 'N', '', '', '', 'India', '', '', '', '0000-00-00', 0, '', NULL, '12', 'red', NULL, 'BMW ', 'r346', '26_1.jpg'),
(31, 27, '', '', '', '', '', 'India', '', '', '', '0000-00-00', 0, '', NULL, NULL, NULL, NULL, '', '', ''),
(32, 28, 'Neha', 'N', '', '', '', 'India', '', '', '', '0000-00-00', 0, '', NULL, NULL, NULL, NULL, '', '', ''),
(33, 29, 'Sneha', 'S', '', '', '', 'India', '', '', '', '0000-00-00', 0, '', NULL, 'Rover', 'White', NULL, 'Range ', 'mh1234', '29_1.jpg'),
(34, 30, 'Mike', 'Mauleon', '', '', '', 'Singapore', '', '', '', '0000-00-00', 0, '', NULL, NULL, NULL, NULL, '', '', ''),
(35, 31, 'Victor', 'Pinto', '', '', '', 'Singapore', '', '', '', '0000-00-00', 0, '', NULL, 'Sunny', 'Blue', NULL, 'Nissan', 'EA1234H', '31_1.jpg'),
(36, 32, 'Victor', 'Pinto', '', '', '', 'Singapore', '', '', '', '0000-00-00', 0, '', NULL, 'Sunny', 'Blue', NULL, 'Nissan', 'EA5432D', '32_1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `user_alerts`
--

CREATE TABLE IF NOT EXISTS `user_alerts` (
  `alert_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alert_transaction_id` int(11) NOT NULL,
  `alert_from_user_id` int(11) NOT NULL,
  `alert_to_user_id` int(11) NOT NULL,
  `alert_type` tinyint(4) DEFAULT NULL COMMENT '1 = Accepted, 2 = Rejected, 3= Cancelled, 4=Initated, 5 = Recieved,  6=Modified',
  `alert_creation_time` datetime NOT NULL,
  PRIMARY KEY (`alert_id`),
  KEY `alert_transaction_id` (`alert_transaction_id`),
  KEY `alert_from_user_id` (`alert_from_user_id`,`alert_to_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_blocked_pals`
--

CREATE TABLE IF NOT EXISTS `user_blocked_pals` (
  `user_blocked_pals_id` int(10) NOT NULL AUTO_INCREMENT,
  `fk_user_app_info_id` int(10) NOT NULL,
  `fk_user_app_info_pal_id` int(10) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`user_blocked_pals_id`),
  KEY `fk_user_app_info_id` (`fk_user_app_info_id`,`fk_user_app_info_pal_id`),
  KEY `fk_user_app_info_pal_id` (`fk_user_app_info_pal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_device`
--

CREATE TABLE IF NOT EXISTS `user_device` (
  `user_device_id` int(10) NOT NULL AUTO_INCREMENT,
  `fk_user_app_info_id` int(10) NOT NULL,
  `phone_registration_id` varchar(300) CHARACTER SET latin1 NOT NULL,
  `device_type` int(2) NOT NULL COMMENT '0- android , 1-ios',
  `is_logged_in` int(2) NOT NULL,
  `email_flag` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_device_id`),
  KEY `fk_user_app_info_id` (`fk_user_app_info_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `user_device`
--

INSERT INTO `user_device` (`user_device_id`, `fk_user_app_info_id`, `phone_registration_id`, `device_type`, `is_logged_in`, `email_flag`) VALUES
(3, 13, 'APA91bFz9xQM23af9gB_WDOtlJNXPtyNQyvcLOGzEoRWqsnlqA34M0cdwLkHTdbds7BvUXoBhmamCXEISttqgyPTzKexNo3b-ZeXPqmO7CjCwLz2HPMUnR_aSrRTEhcoomK8vUKPTEjgAuC-PHC7c9WPtfzKRVIgkg', 0, 1, 0),
(8, 26, '', 0, 1, 0),
(9, 29, 'APA91bFx3UilRQVc1wr6eHhGZ2oXIVSB3GGGof05oK6JjgpfcMgAfcnaOGkPyBLWSfPc7lSGsKJY1WHrNPW3hHHlii-H4iwwnCat1EvWfYytpxVinrl2OyaIUj7f0awwc9vIygtZrx2sde5yk_9rnOK_Gxaqjl56wg', 0, 1, 0),
(10, 30, 'APA91bG6LP8EW6W22ltwfmq0zcrBEtpA0RWFN_C_yH914qgSU4k8n7tk3Jtf_LZnFDIj3BJnlfGSTGp7x5fTrnWcb3pwCgRJfJXIx9bBjpHubulR2Z4ZUMH9zOHv4FG9zIsRo0PBJeIkm2GtQjmski8rkvOPRqYIrQ', 0, 1, 0),
(11, 31, 'APA91bGWSkQzGDou87jPqDVFxHwYycqNimyvgR7S30d6HrkdcpF5-MnFMlcQr5Ow5esQCPWVnhENV6PCM2j9_0FPDBqGQAqDRjtc9Q-M6wspkmovLAl9Lp83XX4PjppWR6rCs-Di8f1wuREAQJxcGVBlrflkWe4OTA', 0, 1, 0),
(12, 32, '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_favourite_pals`
--

CREATE TABLE IF NOT EXISTS `user_favourite_pals` (
  `user_favourite_pals_id` int(10) NOT NULL AUTO_INCREMENT,
  `fk_user_app_info_id` int(10) NOT NULL,
  `fk_user_app_info_pal_id` int(10) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`user_favourite_pals_id`),
  KEY `fk_user_app_info_id` (`fk_user_app_info_id`,`fk_user_app_info_pal_id`),
  KEY `fk_user_app_info_pal_id` (`fk_user_app_info_pal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `fetch_chat`
--
ALTER TABLE `fetch_chat`
  ADD CONSTRAINT `fetch_chat_ibfk_1` FOREIGN KEY (`fc_transaction_id`) REFERENCES `fetch_transaction_new` (`fetch_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fetch_trasaction_users`
--
ALTER TABLE `fetch_trasaction_users`
  ADD CONSTRAINT `fetch_trasaction_users_ibfk_1` FOREIGN KEY (`ftu_transaction_id`) REFERENCES `fetch_transaction_new` (`fetch_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `group_members`
--
ALTER TABLE `group_members`
  ADD CONSTRAINT `group_members_ibfk_1` FOREIGN KEY (`gm_group_id`) REFERENCES `group_master` (`group_id`) ON DELETE CASCADE;

--
-- Constraints for table `test_ios_devices`
--
ALTER TABLE `test_ios_devices`
  ADD CONSTRAINT `test_ios_devices_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users_app_info` (`pk_user_app_info_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_personal_info`
--
ALTER TABLE `users_personal_info`
  ADD CONSTRAINT `users_personal_info_ibfk_1` FOREIGN KEY (`fk_user_app_info_id`) REFERENCES `users_app_info` (`pk_user_app_info_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_blocked_pals`
--
ALTER TABLE `user_blocked_pals`
  ADD CONSTRAINT `user_blocked_pals_ibfk_1` FOREIGN KEY (`fk_user_app_info_id`) REFERENCES `users_app_info` (`pk_user_app_info_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_blocked_pals_ibfk_2` FOREIGN KEY (`fk_user_app_info_pal_id`) REFERENCES `users_app_info` (`pk_user_app_info_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_device`
--
ALTER TABLE `user_device`
  ADD CONSTRAINT `user_device_ibfk_1` FOREIGN KEY (`fk_user_app_info_id`) REFERENCES `users_app_info` (`pk_user_app_info_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
