<?php
echo form_open('webservices/initiateFetchRequest');
echo form_hidden('flag','as');

$data = array(	'name'	=>	'user_id',
				'type'	=>	'text'
			);
echo "User ID : ".form_input($data).br(2);

$data = array( 'name' => 'fetch_id',
			   'type' => 'text'
			);
echo "Fetch ID : ".form_input($data).br(2);

$data = array( 'name' => 'from_lat',
			   'type' => 'text',
			);
echo "From Latitude : ".form_input($data).br(2);

$data = array( 'name' => 'from_longi',
			   'type' => 'text',
			);
echo "From Longitude : ".form_input($data).br(2);

echo form_submit("Submit", "Submit");
?>

<?php echo form_close();?>