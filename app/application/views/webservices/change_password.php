<?php
echo form_open('webservices/changePassword', array('name'=>'frm_change_password'));

echo form_hidden('flag','as');

$data = array( 'name' => 'user_id',
			   'type' => 'text',
			 );
echo "User ID : ".form_input($data).br(2);

$data = array( 'name' => 'old_password',
			   'type' => 'text',
			 );
echo "Current Password : ".form_input($data).br(2);

$data = array( 'name' => 'new_password',
			   'type' => 'text',
			 );
echo "New Password : ".form_input($data).br(2);

echo form_submit('Submit','Submit');
echo form_close();
?>