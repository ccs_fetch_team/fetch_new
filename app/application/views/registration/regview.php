<html>
<head>
<script src="<?php echo base_url();?>js/jquery/jquery-1.4.2.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/jquery/jquery.ui.core.js" type="text/javascript"></script>
<script type="text/javascript">

window.onload=function()
{
	document.getElementById("devicediv").style.display = 'none';
};

function countryselected()
{
	var x= document.getElementById("country").value;
	//alert(x);
}

function getCountryCode(countryName)
{
	$.ajax({ url: "<?php echo site_url();?>/betaregister/getCountryCode/"+countryName+"",
		     context: document.body,
		     success: function(new_value){
					 var country_code = document.getElementById('country_code');
					 var country_code1 = document.getElementById('country_code1');
				     document.frm_registration.country_code.value = new_value;
				     document.frm_registration.country_code1.value = new_value;
				   }
  		});
}

function radiochange(flag)
{
	if(flag==1)
	{	
		document.getElementById("devicediv").style.display = ""; 
	}
	else
	{
		document.getElementById("devicediv").style.display = "none";
	}
}
</script>
<title>Fetch Beta Launch</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
body,td,th {
	font-size: 12px;
	font-family: Arial, Helvetica, sans-serif;
}
</style>
</head>
<body bgcolor="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!-- Save for Web Slices (Bet-Layout.psd) -->
<table id="Table_01" width="1025" height="769" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3"><img src="<?php echo base_url();?>html_images/Bet-Layout_01.png" width="276" height="2" alt=""></td>
		<td colspan="4" rowspan="3"><img src="<?php echo base_url();?>html_images/Bet-Layout_02.png" width="748" height="253" alt=""></td>
		<td><img src="<?php echo base_url();?>html_images/spacer.gif" width="1" height="2" alt=""></td>
	</tr>
	<tr>
		<td rowspan="6">
			<img src="<?php echo base_url();?>html_images/Bet-Layout_03.png" width="38" height="766" alt=""></td>
		<td colspan="2">
			<img src="<?php echo base_url();?>html_images/Bet-Layout_04.png" width="238" height="238" alt=""></td>
		<td>
			<img src="<?php echo base_url();?>html_images/spacer.gif" width="1" height="238" alt=""></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="<?php echo base_url();?>html_images/Bet-Layout_05.png" width="238" height="13" alt=""></td>
		<td>
			<img src="<?php echo base_url();?>html_images/spacer.gif" width="1" height="13" alt=""></td>
	</tr>
	<tr>
		<td rowspan="4">
			<img src="<?php echo base_url();?>html_images/Bet-Layout_06.png" width="60" height="515" alt=""></td>
		<td colspan="2" rowspan="3" bgcolor="white">
	  <?php 
		$frm_arr = array( 'name'	=>	'frm_registration' );
		echo form_open_multipart('betaregister', $frm_arr);
		echo form_hidden('flag','as');

		$data = array(	'name'	=>	'name',
						'type'	=>	'text',
						'value'	=>	@$name,
					);
		echo "Name : ".form_input($data);

		if(form_error('name'))
			echo nbs(3).form_error('name');
		
		echo br(2);

		$js = 'id="country" onChange="getCountryCode(this.value);"';
		echo "Country : ".form_dropdown('country', $countryList,$country,$js);
		
		if(form_error('country'))
			echo nbs(3).form_error('country');
		
		echo br(2);

		$data = array(	'name'	=>	'country_code1',
						'id'	=>	'country_code1',
						'type'	=>	'text',
						'disabled' => 'true'
					);
		echo "Country Code : ".form_input($data).br(2);
		echo '<input type="hidden" id="country_code" name="country_code" value="">';
		
		$data = array(	'name'	=>	'phone',
						'type'	=>	'text',
						'value'	=>	@$phone,
					);
		echo "Phone Number : ".form_input($data);
		
		if(form_error('phone'))
			echo nbs(3).form_error('phone');
		
		echo br(2);
		 
		$data = array(	'name'	=>	'city',
						'type'	=>	'text',
						'value'	=>	@$city,
					 );
		echo "City : ".form_input($data);
		
		if(form_error('city'))
			echo nbs(3).form_error('city');
		
		echo br(2);
		
		if(form_error('device_type'))
			echo nbs(3).form_error('device_type').br(1);
		
		 
		$data = array(  'name'      => 'device_type',
					    'id'        => 'device_type',
					    'value'     => '1',
						'checked'	=> 'checked',
						'onclick'	=> 'radiochange(0)',	
					 );
		echo "Device Type:".form_radio($data)."Android";
		
		$data = array(  'name'        => 'device_type',
					    'id'          => 'device_type',
					    'value'       => '2',
						'onclick'	 => 'radiochange(1)'
					 );
		echo form_radio($data)." iPhone".br(2);
		?>
		<div id="devicediv">
		<?php
		$data = array(	'name'	=> 'deviceid',
						'id' 	=> 'deviceid',
						'type'	=> 'text',
						'value'	=>	@$device_id
					 );
		echo "Device ID : ".form_input($data);
		
		if(form_error('deviceid'))
			echo nbs(3).form_error('deviceid');
		
		echo br(2);
		?>
		</div>
		<?php
		$data = array(	'name'	=>	'email',
						'type'	=>	'text',
						'value'	=>	@$email,
					 );
		echo "Email-id : ".form_input($data);
		
		if(form_error('email'))
			echo nbs(3).form_error('email');
		
		echo br(2);
		 
		$data = array(	'name'	=>	'password',
						'type'	=>	'password'
					  );
		echo "Password : ".form_input($data);
		
		if(form_error('password'))
			echo nbs(3).form_error('password');
		
		echo br(2);
		
		$data = array(	'name'	=>	'confirm_password',
						'type'	=>	'password'
					 );
		echo "Confirm Password : ".form_input($data);
		
		if(form_error('confirm_password'))
			echo nbs(3).form_error('confirm_password');
		
		echo br(2);
		
		echo form_submit("Submit", "Submit");
		echo form_close();
	?> 
	<script>
	<?php if($country) { ?>
		getCountryCode("<?php echo $country;?>");
	<?php } else {?>
		getCountryCode("<?php echo "Singapore";?>");
	<?php } ?>
	</script>
 
      </td>
		<td colspan="3">
			<img src="<?php echo base_url();?>html_images/Bet-Layout_08.png" width="497" height="344" alt=""></td>
		<td>
			<img src="<?php echo base_url();?>html_images/spacer.gif" width="1" height="344" alt=""></td>
	</tr>
	<tr>
		<td rowspan="3">
			<img src="<?php echo base_url();?>html_images/Bet-Layout_09.png" width="234" height="171" alt=""></td>
		<td>
			<img src="<?php echo base_url();?>html_images/Bet-Layout_10.png" width="124" height="62" alt=""></td>
		<td rowspan="3">
			<img src="<?php echo base_url();?>html_images/Bet-Layout_11.png" width="139" height="171" alt=""></td>
		<td>
			<img src="<?php echo base_url();?>html_images/spacer.gif" width="1" height="62" alt=""></td>
	</tr>
	<tr>
		<td rowspan="2">
			<img src="<?php echo base_url();?>html_images/Bet-Layout_12.png" width="124" height="109" alt=""></td>
		<td>
			<img src="<?php echo base_url();?>html_images/spacer.gif" width="1" height="51" alt=""></td>
	</tr>
	<tr>
		<td colspan="2">
			<img src="<?php echo base_url();?>html_images/Bet-Layout_13.png" width="429" height="58" alt="">
		</td>
		<td>
			<img src="<?php echo base_url();?>html_images/spacer.gif" width="1" height="58" alt="">
		</td>
	</tr>
	<tr>
		<td>
			<img src="<?php echo base_url();?>html_images/spacer.gif" width="38" height="1" alt=""></td>
		<td>
			<img src="<?php echo base_url();?>html_images/spacer.gif" width="60" height="1" alt=""></td>
		<td>
			<img src="<?php echo base_url();?>html_images/spacer.gif" width="178" height="1" alt=""></td>
		<td>
			<img src="<?php echo base_url();?>html_images/spacer.gif" width="251" height="1" alt=""></td>
		<td>
			<img src="<?php echo base_url();?>html_images/spacer.gif" width="234" height="1" alt=""></td>
		<td>
			<img src="<?php echo base_url();?>html_images/spacer.gif" width="124" height="1" alt=""></td>
		<td>
			<img src="<?php echo base_url();?>html_images/spacer.gif" width="139" height="1" alt=""></td>
		<td></td>
	</tr>
</table>
<!-- End Save for Web Slices -->
</body>
</html>