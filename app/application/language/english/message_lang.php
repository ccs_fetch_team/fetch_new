<?php
$lang['allowed_types']='gif|jpg|png';

//SMS format
$lang['SMS_ACTIVATION'] = 'Hi <user_name>, Your verification code is <activation_code>.';

$lang['REGISTRATION_MAIL_SUBJECT'] = 'Registration Confirmation from http://fetch-apps.com';

$lang['REGISTRATION_MAIL_BODY'] = 
'Hi ##user_name##,<br>
<br>
Thanks for registering on http://www.fetch-apps.com.<br>
You can download App using these links,<br>
For Android,<br>
##android_link##<br><br>

For iPhone,<br>
##iphone_link##<br>
Note : For iPhone you can use this if you have registered your device UDID with Fetch.
<br>
<br>
Thanks,<br>
Fetch Apps Admin';
?>