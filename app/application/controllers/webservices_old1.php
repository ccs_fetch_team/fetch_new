<?php
class Webservices extends CI_Controller
{
	function __construct()
	{
		parent::__Construct();
		$this->load->model('Usermodel', '', TRUE);
		$this->load->model('Groupmodel', '', TRUE);
		$this->load->model('Commfuncmodel','', TRUE);
		$this->load->model('Transactionmodel', '', TRUE);
		$this->load->model('Alertmodel','',TRUE);
		$this->load->model('Notificationmodel', '', TRUE);
		$this->lang->load('message', 'english');
	}

	function index()
	{
		echo "Welcome";
	}

	function getCarProfile()
	{
		if($this->input->post('flag')=='as')
		{
			$user_id = $this->input->post('user_id');

			if($user_id)
			{
				$result = array();
				$select = 'carRegistrationNo, carModel, carColor, carDescription';
				$carData = $this->Usermodel->getPersonalInfo($user_id, $select);

				if( !empty($carData) )
				{
					$result['Result']['ErrorCode'] = 0;
					$result['Result']['ErrorMessage']= 'Success';
					$result['Result']['carProfile'] = $carData;
				}
				else
				{
					$result['Result']['ErrorCode'] = '-2';
					$result['Result']['ErrorMessage'] = "No records found"; 
				}
			}
			else
			{
				$result['Result']['ErrorCode'] = 1;
				$result['Result']['ErrorMessage']= 'Failure';
			}
			
			echo json_encode($result);
		}
		else
		{
			$this->load->view('webservices/getCarProfile');
		}
	}

	function getUserProfile()
	{
		if($this->input->post('flag')=='as')
		{
			$user_id = $this->input->post('user_id');

			if($user_id)
			{
				$result = array();
				$select = 'carRegistrationNo, carModel, carColor, carDescription';
				$carData = $this->Usermodel->getPersonalInfo($user_id, $select);

				if( !empty($carData) )
				{
					$result['Result']['ErrorCode'] = 0;
					$result['Result']['ErrorMessage']= 'Success';
					$result['Result']['carProfile'] = $carData;
				}
				else
				{
					$result['Result']['ErrorCode'] = '-2';
					$result['Result']['ErrorMessage'] = "No records found"; 
				}
			}
			else
			{
				$result['Result']['ErrorCode'] = 1;
				$result['Result']['ErrorMessage']= 'Failure';
			}
			
			echo json_encode($result);
		}
		else
		{
			$this->load->view('webservices/getUserProfile');
		}
	}
	
	function carProfile()
	{
		if($this->input->post('flag')=='as')
		{
			$data = $result = array();

			$userID = $this->input->post('user_id');

			$data = array(	'carRegistrationNo' => $this->input->post('reg_number'),
							'carModel'			=> $this->input->post('model'),
							'carColor' 			=> $this->input->post('color'),
							'carDescription'	=> $this->input->post('description')
						);

			if($userID && !empty($data))
			{
				$this->Usermodel->updatePersonalInfo($userID, $data);
				
				$result['Result']['ErrorCode']='0';
	    		$result['Result']['ErrorMessage']="Success";
			}
			else
			{
				$result['Result']['ErrorCode']='1';
	    		$result['Result']['ErrorMessage']="Failure";
			}
			
			echo json_encode($result);
		}
		else
		{
			$this->load->view('webservices/carProfile');
		}
	}

	function changePassword()
	{
		$data = array();
		$result = array();
		
		if($this->input->post('flag')=='as')
		{
			$userID = $this->input->post('user_id');
			$oldPassword = $this->input->post('old_password');
			$newPassword = $this->input->post('new_password');

			if($userID && $oldPassword && $newPassword)
			{
				$update_flag = $this->Usermodel->changePassword($userID, base64_encode($oldPassword), base64_encode($newPassword));

				switch($update_flag)
				{
					case 1 : //Password changed successfully
						$result['Result']['ErrorCode'] = 0;
						$result['Result']['ErrorMessage'] = "Success";
						break;

					case 2 : //Invalid current Password 
						$result['Result']['ErrorCode'] = 1;
						$result['Result']['ErrorMessage'] = "Failure";
						break;
				}
			}
			else
			{
				$result['Result']['ErrorCode']='3';
			    $result['Result']['ErrorMessage']="All fields are mandatory";
			}

			echo json_encode($result);
		}
		else
		{
			$this->load->view('webservices/change_password', @$data);
		}
	}

	function createGroup()
	{
		if($this->input->post('flag')=='as')
		{
			$result = array();
			$user_id = $this->input->post('user_id');
			
			$data = array(	'group_name'  => $this->input->post('group_name'),
			 				'group_owner' => $this->input->post('user_id'),
			 			  );
			
			$group_members = $this->input->post('group_members');

			if($group_members)
			{
				$group_members = explode(',', $group_members);
			//}
			//$group_members[] = $user_id;

				if($data['group_name'] && $data['group_owner'])
				{
					if($this->Groupmodel->groupAlreadyExists($data))
					{
						$group_flag = $this->Groupmodel->addGroup($data);
	
						if($group_flag)
						{
							$group_id = $this->db->insert_id();
	
							$this->Groupmodel->addGroupMembers($group_id, $group_members);
	
							$result['Result']['ErrorCode'] = 0;
							$result['Result']['ErrorMessage'] = "Success";
						}
						else
						{
							$result['Result']['ErrorCode'] = 1;
							$result['Result']['ErrorMessage'] = "Failure";
						}
					}
					else
					{
						$result['Result']['ErrorCode'] = 4;
						$result['Result']['ErrorMessage'] = "Group Already Exists";
					}
					
				}
				else
				{
					$result['Result']['ErrorCode']='3';
				    $result['Result']['ErrorMessage']="All fields are mandatory";
				}
			}
			else
			{
				$result['Result']['ErrorCode']='3';
				$result['Result']['ErrorMessage']="All fields are mandatory";
			}
			echo json_encode($result);
		}
		else
		{
			$this->load->view('webservices/createGroup');
		}
	}

	function updateGroup()
	{
		if($this->input->post('flag')=='as')
		{
			$result = array();
			$user_id = $this->input->post('user_id');
			$group_id = $this->input->post('group_id');

			$data_update = array( 'group_name' => $this->input->post('group_name'),
								  'group_owner' =>	$user_id,
								 );

			if(@$data_update['group_name'])
			{
				//if($this->Groupmodel->groupAlreadyExists($data_update, $group_id))
				{
					$this->Groupmodel->updateGroup($group_id, $data_update);
					
					$group_members = $this->input->post('group_members');
					//$rows_affected1 = $this->db->affected_rows();

					if($group_members)
					{
						$this->Groupmodel->deleteGroupMembers($group_id);
						$group_members = explode(',', $group_members);

						$this->Groupmodel->addGroupMembers($group_id, $group_members);
					
					}

					//if($this->db->affected_rows()>0)
					{
						$result['Result']['ErrorCode'] = 0;
						$result['Result']['ErrorMessage'] = "Success";
					}
					/*else
					{
						$result['Result']['ErrorCode'] = '1';
						$result['Result']['ErrorMessage'] = "Failure";
					}*/
				}
				/*else
				{
					$result['Result']['ErrorCode'] = 4;
					$result['Result']['ErrorMessage'] = "Group with this name already exists";
				}*/
			}
			else
			{
				$result['Result']['ErrorCode'] = 1;
				$result['Result']['ErrorMessage'] = "Failure";
			}

			echo json_encode($result);
		}
		else
		{
			$this->load->view('webservices/updateGroup');
		}
	}

	function deleteGroup()
	{
		if($this->input->post('flag')=='as')
		{
			$result = array();
			$user_id = $this->input->post('user_id');
			$group_id = $this->input->post('group_id');
			
			if($user_id && $group_id)
			{
				$this->Groupmodel->deleteGroup($group_id, $user_id);
				
				if( $this->db->affected_rows()>0 )
				{
					$result['Result']['ErrorCode'] = 0;
					$result['Result']['ErrorMessage'] = "Success";
				}
				else
				{
					$result['Result']['ErrorCode'] = 1;
					$result['Result']['ErrorMessage'] = "Failure";
				}
			}
			else
			{
				$result['Result']['ErrorCode'] = 1;
				$result['Result']['ErrorMessage'] = "Failure";
			}
			echo json_encode($result);
		}
		else
		{
			$this->load->view('webservices/deleteGroup');
		}	
	}

	function getUserGroups()
	{
		if($this->input->post('flag')=='as')
		{
			$user_id = $this->input->post('user_id');
			$result = array();
			
			if($user_id)
			{
				$groups_list = $this->Groupmodel->getGroupsList($user_id);

				$result['Result']['ErrorCode'] = 0;
				$result['Result']['ErrorMessage'] = "Success";
				$result['Result']['groupList'] = $groups_list;
			}
			else
			{
				$result['Result']['ErrorCode'] = 1;
				$result['Result']['ErrorMessage'] = "Failure";
			}

			//echo "<pre>"; print_r($result); die("<hr>yyyy");
			echo json_encode($result);
		}
		else
		{
			$this->load->view('webservices/group_list');
		}
	}

	function setFavouritePals()
	{
		if($this->input->post('flag')=='as')
		{
			$user_id = $this->input->post('user_id');
			$favourite_pals = $this->input->post('favourite_pals');
			$unfavourite_pals = $this->input->post('unfavourite_pals');
			
			if($favourite_pals)
			{
				$favourite_list = explode(',', $favourite_pals);
				
				foreach($favourite_pals as $k=>$v)
				{
					$data_update = array( '');
				}
			}
		}
	}

	function getDuration()
	{
		if($this->input->post('flag')=='as')
		{
			$fromLat = $this->input->post('fromLat');
			$fromLongi = $this->input->post('fromLongi');
			
			$toLat = $this->input->post('toLat');
			$toLongi = $this->input->post('toLongi');
			
			$result = $this->Commfuncmodel->getDuration($fromLat, $fromLongi, $toLat, $toLongi);
			
			echo "<pre>"; print_r($result);
		}
		else
		{
			$this->load->view('webservices/get_duration');
		}
	}

	function createFetchRequest()
	{
		if($this->input->post('flag')=='as')
		{	
			$result = array();
			$photo_arr = array();
			
			$user_id = $this->input->post('user_id');

			$data = array(	'fetch_creator_id'		=>	$user_id,
							'fetch_flag'			=>	$this->input->post('fetch_flag'),
							'fetch_location_name'	=>	$this->input->post('location_name'),
							'fetch_to_lat'			=>	$this->input->post('location_lat'),
							'fetch_to_longi'		=>	$this->input->post('location_longi'),
							'fetch_datetime'		=>	$this->input->post('fetch_datetime'),
							'fetch_timezone'		=>	$this->input->post('fetch_timezone'),
							'fetch_message'			=>	$this->input->post('message'),
						);

			//Code for uploading file to temporary directory
			/*=========== Begins Here =========== */
			if(@$_FILES['fetch_photo']["name"]!="")
			{
				$ext=substr($_FILES['fetch_photo']["name"],strrpos($_FILES['fetch_photo']["name"],".")+1);
				$ext=strtolower($ext);

				$types_arr=explode("|",lang('allowed_types'));

				if(in_array($ext, $types_arr))
				{
					if($_FILES['fetch_photo']['size'] <= MAX_FILE_SIZE)
					{
						$uploaddir = realpath(DIR_FETCH_PHOTOS).'/tmp/';
						$uploadfile = $user_id."_".date("Ymd_His").".".$ext;

						if (move_uploaded_file($_FILES['fetch_photo']['tmp_name'], $uploaddir .$uploadfile))
						{
							$photo_arr["fetch_photo"]["file_name"] = $uploadfile;
							$photo_arr["fetch_photo"]['file_ext'] = $ext;
							$photo_arr["fetch_photo"]["name"] = $uploadfile;
							$success = 'y';
						}
						else
						{
							$result['Result']['ErrorCode']='2';
							$result['Result']['ErrorMessage'] = "Error in file uploading";
							$success="n";
							echo json_encode($result);
			        		return;
						}
					}
					else
					{
						$result['Result']['ErrorCode']='2';
			        	$result['Result']['ErrorMessage']="File size exceeds";
			        	echo json_encode($result);
			        	return;
					}
				}
				else
				{
					$success="n";
					$result['Result']['ErrorCode']='2';
		        	$result['Result']['ErrorMessage']="Invalid file type";
		        	echo json_encode($result);
		        	return;
				}
			}
			/* =========== Ends Here =========== */
						
			$data['fetch_creation_date'] = date('Y-m-d H:i:s');
			$data['fetch_updation_date'] = date('Y-m-d H:i:s');

			$this->Transactionmodel->add($data);
			$fetch_id = $this->db->insert_id();
			
			$contact_ids = $this->input->post('contact_ids');
			
			$contacts_arr = explode(',', $contact_ids);
			foreach($contacts_arr as $k=>$v)
			{
				$data_users = array( 'ftu_transaction_id'	=>	$fetch_id,
									 'ftu_user_type'		=>	$this->input->post('user_type'),
									 'ftu_user_id'			=>	$v,
									 'ftu_response'			=>	0,
								  );

				$this->Transactionmodel->addTransactionUsers($data_users);
			}

			if($fetch_id)
			{
				if(!empty($photo_arr["fetch_photo"]))
				{
					$dir=realpath(DIR_FETCH_PHOTOS);

					$org_file=$dir."/tmp/".$photo_arr["fetch_photo"]["file_name"];
					$file=$dir."/fetch".$fetch_id.".".$photo_arr["fetch_photo"]['file_ext'];
					rename($org_file,$file);

					$data_update = array(	'fetch_image'		  =>	"fetch".$fetch_id.".".$photo_arr["fetch_photo"]['file_ext'],
											'fetch_updation_date' =>	date('Y-m-d H:i:s'),
										);

					$this->Transactionmodel->update($fetch_id, $data_update);
				}//inner if ends here
				$result['Result']['ErrorCode'] = '0';
				$result['Result']['ErrorMessage'] = 'Success';
				$result['Result']['fetch_id'] = $fetch_id;
			}
			else
			{
				$result['Result']['ErrorCode'] = '1';
				$result['Result']['ErrorMessage'] = 'Failure';
			}
			echo json_encode($result);

			if($fetch_id)
			{
				$result1 = array();
				
				//Retrieve User data form DB
				$user_data = $this->Usermodel->getUserAppInfo($data['fetch_creator_id'], 'phone_no');
				$user_data1 = $this->Usermodel->getPersonalInfo($data['fetch_creator_id'],'name');
				$alert_template = $this->Alertmodel->getAlertTemplate(1);
				$data['fetch_id'] = $fetch_id;

				unset($data['fetch_creation_date']);
				unset($data['fetch_updation_date']);
				$result1['Result']	=	array(	'fetch_data'	=> 	$data, 
												'name'			=>	$user_data1['name'],
												'phone_no'		=>	$user_data['phone_no']		
											);
				
				//Code for adding Alert into database
				foreach($contacts_arr as $k=>$v)
				{
					$alert_data = array(	'alert_transaction_id'	=>	$fetch_id,
											'alert_from_user_id'	=>	$data['fetch_creator_id'],
											'alert_to_user_id'		=>	$v,
											'alert_type'			=>	1,
											'alert_creation_time'	=>	date('Y-m-d H:i:s'),
										);

					$this->Alertmodel->addUserAlert($alert_data);

					//Code to send notifications
					$result1['Result']['message'] = $alert_template['template_text'];
					$msg = json_encode($result1);

					$this->Notificationmodel->callnotification($data['fetch_creator_id'], $v, $msg);
				}
			}
		}
		else
		{
			$data['method_name'] = 'createFetchRequest';
			$this->load->view('webservices/create_fetch_request', @$data);
		}
	}

	function updateFetchRequest()
	{
		if($this->input->post('flag')=='as')
		{
			$result = array();
			$photo_arr = array();
			
			$user_id = $this->input->post('user_id');
			$fetch_id = $this->input->post('fetch_id');

			$data_update = array(	//'fetch_creator_id'		=>	$user_id,
							//'fetch_flag'			=>	$this->input->post('fetch_flag'),
							'fetch_location_name'	=>	$this->input->post('location_name'),
							'fetch_to_lat'			=>	$this->input->post('location_lat'),
							'fetch_to_longi'		=>	$this->input->post('location_longi'),
							'fetch_datetime'		=>	$this->input->post('fetch_datetime'),
							'fetch_timezone'		=>	$this->input->post('fetch_timezone'),
							'fetch_message'			=>	$this->input->post('message'),
						);

						//Code for uploading file to temporary directory
			/*=========== Begins Here =========== */
			if(@$_FILES['fetch_photo']["name"]!="")
			{
				$ext=substr($_FILES['fetch_photo']["name"],strrpos($_FILES['fetch_photo']["name"],".")+1);
				$ext=strtolower($ext);

				$types_arr=explode("|",lang('allowed_types'));

				if(in_array($ext, $types_arr))
				{
					if($_FILES['fetch_photo']['size'] <= MAX_FILE_SIZE)
					{
						$uploaddir = realpath(DIR_FETCH_PHOTOS).'/tmp/';
						$uploadfile = $user_id."_".date("Ymd_His").".".$ext;

						if (move_uploaded_file($_FILES['fetch_photo']['tmp_name'], $uploaddir .$uploadfile))
						{
							$photo_arr["fetch_photo"]["file_name"] = $uploadfile;
							$photo_arr["fetch_photo"]['file_ext'] = $ext;
							$photo_arr["fetch_photo"]["name"] = $uploadfile;
							$success = 'y';
						}
						else
						{
							$result['Result']['ErrorCode']='2';
							$result['Result']['ErrorMessage'] = "Error in file uploading";
							$success="n";
							echo json_encode($result);
			        		return;
						}
					}
					else
					{
						$result['Result']['ErrorCode']='2';
			        	$result['Result']['ErrorMessage']="File size exceeds";
			        	echo json_encode($result);
			        	return;
					}
				}
				else
				{
					$success="n";
					$result['Result']['ErrorCode']='2';
		        	$result['Result']['ErrorMessage']="Invalid file type";
		        	echo json_encode($result);
		        	return;
				}
			}
			/* =========== Ends Here =========== */

			$data_update['fetch_updation_date'] = date('Y-m-d H:i:s');

			$this->Transactionmodel->update($fetch_id, $data_update);

			$contact_ids = $this->input->post('contact_ids');
			$contacts_arr = explode(',', $contact_ids);

			foreach($contacts_arr as $k=>$v)
			{
				$data_users = array( 'ftu_transaction_id'	=>	$fetch_id,
									 'ftu_user_type'		=>	$this->input->post('user_type'),
									 'ftu_user_id'			=>	$v,
									 'ftu_response'			=>	0,
								   );

				$this->Transactionmodel->addTransactionUsers($data_users);
			}

			if($fetch_id)
			{
				if(!empty($photo_arr["fetch_photo"]))
				{
					$dir=realpath(DIR_FETCH_PHOTOS);

					$org_file=$dir."/tmp/".$photo_arr["fetch_photo"]["file_name"];
					$file=$dir."/fetch".$fetch_id.".".$photo_arr["fetch_photo"]['file_ext'];

					rename($org_file,$file);
					$data_update = array(	'fetch_image'		  =>	"fetch".$fetch_id.".".$photo_arr["fetch_photo"]['file_ext'],
											'fetch_updation_date' =>	date('Y-m-d H:i:s'),
										);

					$this->Transactionmodel->update($fetch_id, $data_update);
				}//inner if ends here

				$result['Result']['ErrorCode'] = '0';
				$result['Result']['ErrorMessage'] = 'Success';
				$result['Result']['fetch_id'] = $fetch_id;
			}
			else
			{
				$result['Result']['ErrorCode'] = '1';
				$result['Result']['ErrorMessage'] = 'Failure';
			}
			echo json_encode($result);

			if($fetch_id)
			{
				//Code for adding Alert into database
				foreach($contacts_arr as $k=>$v)
				{
					$alert_data = array(	'alert_transaction_id'	=>	$fetch_id,
											'alert_from_user_id'	=>	$data['fetch_creator_id'],
											'alert_to_user_id'		=>	$v,
											'alert_type'			=>	6,
											'alert_creation_time'	=>	date('Y-m-d H:i:s'),
										);

					$this->Alertmodel->addUserAlert($alert_data);

					//Code to send notifications
					
				}
			}
		}
		else
		{
			$this->load->view('webservices/update_fetch_request');
		}
	}

	function initiateFetchRequest()
	{
		if($this->input->post('flag')=='as')
		{
			$user_id = $this->input->post('user_id');
			$fetch_id = $this->input->post('fetch_id');

			$data = array(	'fetch_status' 			=>	4,
							'fetch_from_lat'		=>	$this->input->post('from_lat'),
							'fetch_from_longi'		=>	$this->input->post('from_longi'),
							'fetch_updation_date' 	=>	date('Y-m-d H:i:s'),
						 );

			$this->Transactionmodel->update($fetch_id, $data);

			if($this->db->affected_rows()>0)
			{
				$select = 'fetch_from_lat, fetch_from_longi, fetch_to_lat, fetch_to_longi';
				$fetch_data = $this->Transactionmodel->getTransactionDetails($fetch_id, $select);

				$res = $this->Commfuncmodel->getDuration($fetch_data['fetch_from_lat'], $fetch_data['fetch_from_longi'], $fetch_data['fetch_to_lat'], $fetch_data['fetch_to_longi']);	

				$result['Result']['ErrorCode'] = '0';
				$result['Result']['ErrorMessage'] = 'Success';
				$result['Result']['duration'] = $res['duration'];
				$result['Result']['distance'] = $res['distance'];
				
				/*
				if($fetch_id)
				{
					//Code for adding Alert into database
					$alert_data = array(	'alert_transaction_id'	=>	$fetch_id,
											'alert_from_user_id'	=>	$user_id,
											'alert_to_user_id'		=>	$fetch_data['fetch_creator_id'],
											'alert_type'			=>	5,
											'alert_creation_time'	=>	date('Y-m-d H:i:s'),
										);

					$this->Alertmodel->addUserAlert($alert_data);
	
					//Code to send notifications
					$res = $this->Alertmodel->getAlertTemplate(5);
				}
				*/
			}
			else
			{
				$result['Result']['ErrorCode'] = '1';
				$result['Result']['ErrorMessage'] = 'Failed';
			}
			echo json_encode($result);
		}
		else
		{
			$this->load->view('webservices/fetch_request_initiate');			
		}
	}

	function getETA()
	{
		if($this->input->post('flag')=='as')
		{
			$res = array();

			$user_id = $this->input->post('user_id');
			$fetch_id = $this->input->post('fetch_id');
			$fetch_flag = $this->input->post('fetch_flag');

			if($fetch_flag == '0')
			{
				$data = array(	'fetch_from_lat'		=>	$this->input->post('from_lat'),
								'fetch_from_longi'		=>	$this->input->post('from_longi'),
								'fetch_updation_date'	=>	date('Y-m-d H:i:s'),
							);

				$this->Transactionmodel->update($fetch_id, $data);
			}

			$fetch_data = $this->Transactionmodel->getTransactionDetails($fetch_id, 'fetch_from_lat, fetch_from_longi, fetch_to_lat, fetch_to_longi');
			
			$res = $this->Commfuncmodel->getDuration($fetch_data['fetch_from_lat'], $fetch_data['fetch_from_longi'], $fetch_data['fetch_to_lat'], $fetch_data['fetch_to_longi']);
			
			//echo "<pre>"; print_r($res); die('<hr>1111');
			$result = array();
			if(!empty($res))
			{
				$result['Result']['ErrorCode'] = '0';
				$result['Result']['ErrorMessage'] = 'Success';
				$result['Result']['duration'] = $res['duration'];
				$result['Result']['distance'] = $res['distance'];
			}
			else
			{
				$result['Result']['ErrorCode'] = '1';
				$result['Result']['ErrorMessage'] = 'Failure';
			}
			echo json_encode($result);
		}
		else
		{
			$this->load->view('webservices/getETA');
		}
	}

	function respondToFetchRequest()
	{
		if($this->input->post('flag')=='as')
		{
			$result = array();
			$user_id = $this->input->post('user_id');
			$fetch_id = $this->input->post('fetch_id');
			$response = $this->input->post('response');

			$fetch_data = $this->Transactionmodel->getTransactionDetails($fetch_id);

			if($user_id == $fetch_data['fetch_creator_id'])
			{
				$data_update = array(	'fetch_status'	=>	3 );
				$this->Transactionmodel->update($fetch_id, $data_update);
			}
			else
			{
				$success_flg1 = FALSE;
				$data_update = array(	'ftu_response'	=>	$this->input->post('fetch_response') );
				$success_flg1 = $this->Transactionmodel->updateTransactionUsers($fetch_id, $user_id, $data_update);

				$total_cnt = $this->Transactionmodel->getTransactionResponseCount($fetch_id);
				$accept_cnt = $this->Transactionmodel->getTransactionResponseCount($fetch_id, 1);
				$reject_cnt = $this->Transactionmodel->getTransactionResponseCount($fetch_id, 2);
				//$cancel_cnt = $this->Transactionmodel->getTransactionResponseCount($fetch_id, 3);
				//$pending_cnt =  $this->Transactionmodel->getTransactionResponseCount($fetch_id, 0);

				$data_update = array();
				$success_flg2 = FALSE;
				if($accept_cnt >0)
				{
					$data_update  = array(	'fetch_status' =>	1,
											'fetch_updation_date'	=>	date('Y-m-d H:i:s')					
										 );

					$success_flg2 = $this->Transactionmodel->update($fetch_id, $data_update);
				}
				else if($reject_cnt == $total_cnt)
				{
					$data_update  = array(	 'fetch_status' =>	2,
											 'fetch_updation_date'	=>	date('Y-m-d H:i:s')
										 );

					$success_flg2 = $this->Transactionmodel->update($fetch_id, $data_update);
				}
			}

			if($success_flg1 || $success_flg2)
			{
				$result['Result']['ErrorCode'] = '0';
				$result['Result']['ErrorMessage'] = 'Success';
				$result['Result']['fetch_id'] = $fetch_id;
			}
			else
			{
				$result['Result']['ErrorCode'] = '1';
				$result['Result']['ErrorMessage'] = 'Failure';
			}

			echo json_encode($result);
		}
		else
		{
			$this->load->view('webservices/fetch_request_response');
		}
	}

	function getAlertsList()
	{
		if($this->input->post('flag')=='as')
		{
			$user_id = $this->input->post('user_id');
			$page_no = $this->input->post('page_no');

			$result = array();
			$alerts_list = array();
			$alerts_list = $this->Alertmodel->getUserAlerts($user_id, $page_no);
			
			echo br(2).$this->db->last_query().br(2);
			echo "<pre>"; print_r($alerts_list); die('1111');
			$name = array();
			$alert_template=array();
			$refined_template=array();
			foreach ($alerts_list as $alertarry)
			{
				$name[] = $this->Usermodel->getPersonalInfo($alertarry['alert_from_user_id'],'name');
				$alert_template[] = $this->Alertmodel->getAlertTemplate($alertarry['alert_type'],'template_text');	
			}
			$i=0;
			$alertnew = array();
			foreach ($alert_template as $alertreplace)
			{
				$alertnew[]= str_replace("##user_name##",$name[$i]['name'],$alertreplace);
				$i++;
			}
			if(!empty($alertnew))
			{
				$result['Result']['ErrorCode'] = '0';
				$result['Result']['ErrorMessage'] = 'Success';
				$result['Result']['alertsList'] = $alertnew;
				echo json_encode($result);
			}
			else
			{
				
			}
		}
		else
		{
			$this->load->view('webservices/getAlertsList');
		}
	}

}
?>