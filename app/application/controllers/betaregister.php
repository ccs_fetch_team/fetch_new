<?php
class Betaregister extends CI_Controller
{
	function __construct()
	{
		parent::__Construct();
		$this->load->model('Usermodel', '', TRUE);
		$this->load->model('Groupmodel', '', TRUE);
		$this->load->model('Commfuncmodel','', TRUE);
		$this->load->model('Transactionmodel', '', TRUE);
		$this->load->model('Alertmodel','',TRUE);
		$this->load->model('Notificationmodel', '', TRUE);
		$this->load->model('Fetchmodel','', TRUE);
		$this->lang->load('message', 'english');
		
		$this->form_validation->set_error_delimiters('<span style="color:#FF0000;margin-left:15dp;">', '</span>');
	}

	function index()
	{
		$data = array();
		if($this->input->post('flag')=='as')
		{
			$data = array(	'name'			=>	$this->input->post('name'),
							'email'			=>	$this->input->post('email'),
							'password'		=>	$this->input->post('password'),
							'country'		=>	$this->input->post('country'),
							'country_code'	=>	$this->input->post('country_code'),
							'city'			=>	$this->input->post('city'),
							'phone'			=>	$this->input->post('phone'),
							'device_type'	=>	$this->input->post('device_type'),
							'device_id'		=>	$this->input->post('deviceid'),
						);

			$this->form_validation->set_rules('name', 'Name', 'required|is_alpha');
			$this->form_validation->set_rules('phone','Phone Number', 'required|is_numeric|is_unique[users_app_info.phone_no]');
			$this->form_validation->set_rules('password','Password', 'required');
			$this->form_validation->set_rules('confirm_password','Confirm Password','required|matches[password]');
			$this->form_validation->set_rules('country', 'Country', 'required');
			$this->form_validation->set_rules('city','City','required');
			$this->form_validation->set_rules('email','Email','required|valid_email|is_unique[users_app_info.email_id]');
			
			if($data['device_type']==2)
				$this->form_validation->set_rules('deviceid','Device ID','required');

			if($this->form_validation->run()==TRUE)
			{
				$app_info = array(	'email_id'		=>	$data['email'],
									'password'		=>	base64_encode($data['password']),
									'country_code'	=>	$data['country_code'],
									'phone_no'		=>	$data['phone'],
									'created_date'	=>	date('Y-m-d H:i:s'),
									'updated_date'	=>	date('Y-m-d H:i:s'),
									'phone_activation_status'	=>	1,
								);

				$this->Usermodel->addUser($app_info);
				$user_id = $this->db->insert_id();

				$personal_info = array(	'fk_user_app_info_id'	=>	$user_id,
										'name'					=>	$data['name'],
										'city'					=>	$data['city'],
										'country'				=>	$data['country'],
									  );

				$this->Usermodel->addUserPersonalInfo($personal_info);
				
				if($data['device_type']==2)
				{
					$data_device = array(	'user_id'	=>	$user_id,	
											'device_id'	=>	$data['device_id'],				
										);

					$this->db->insert(TABLE_TEST_DEVICES, $data_device);
				}
				
				//Code for sending mail
				$sub = lang('REGISTRATION_MAIL_SUBJECT');
				$mail_body = lang('REGISTRATION_MAIL_BODY');

				$old_words = array('##user_name##','##android_link##','##iphone_link##');
				$new_words = array($data['name'], base_url().'installer/Fetch.apk', base_url().'installer/Fetch.ipa');

				$this->Commfuncmodel->sendEmail($data['email'],'noreply@fetch-apps.com',$sub,$mail_body, $old_words, $new_words);

				redirect('betaregister/success');
			}
		}

		if(!@$data['country'])
			$data['country'] = 'Singapore';

		$data['countryList'] = $this->Commfuncmodel->getCountries();
		//$this->load->view('registration/registrationview',@$data);
		$this->load->view('registration/regview',@$data);
	}

	function success()
	{
		echo "<h3>Registration successfull.".br(1)."Please check you email.</h3>";
	}

	function getCountryCode($countryname)
	{
		if($countryname!="")
		{
			$countryname = urldecode($countryname);
			$this->db->select('*')->where('cc_country',$countryname);	
			$query = $this->db->get('ccg_countries');	
			if($query->num_rows()>0)
			{
				$res = $query->result_array();
				$country_code = $res[0]['cc_code'];
				echo $country_code;
			}
			else
			{
				echo '';
			}
		}
	}	
}
?>