<?php

class Webservices extends CI_Controller {

    function __construct() {
        parent::__Construct();
        $this->load->model('Usermodel', '', TRUE);
        $this->load->model('Groupmodel', '', TRUE);
        $this->load->model('Commfuncmodel', '', TRUE);
        $this->load->model('Transactionmodel', '', TRUE);
        $this->load->model('Alertmodel', '', TRUE);
        $this->load->model('Notificationmodel', '', TRUE);
        $this->load->model('Fetchmodel', '', TRUE);
        $this->lang->load('message', 'english');
    }

    function getCarProfile() {
        if ($this->input->post('flag') == 'as') {
            $user_id = $this->input->post('user_id');

            if ($user_id) {
                $result = array();
                $select = 'carRegistrationNo,carLicense, carModel, carColor, carMake, carPhoto';
                $carData = $this->Usermodel->getPersonalInfo($user_id, $select);
                
                if (!empty($carData)) {
                    $result['Result']['ErrorCode'] = '0';
                    $result['Result']['ErrorMessage'] = 'Success';
                    if ($carData['carRegistrationNo'] == "" && $carData['carModel'] == "" && $carData['carColor'] == "" && $carData['carMake'] == "") {
                        $result['Result']['isProfileSet'] = 0;
                    } else {
                        $result['Result']['isProfileSet'] = 1;
                    }
                    $carData['image_url']=  base_url()."/user_car_images/".$carData['carPhoto'];
                    $result['Result']['carProfile'] = $carData;
                } else {
                    $result['Result']['ErrorCode'] = '-2';
                    $result['Result']['ErrorMessage'] = "No records found";
                }
            } else {
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = 'Failure';
            }

            echo json_encode($result);
        } else {
            $this->load->view('webservices/getCarProfile');
        }
    }

    function getUserProfile() {
        if ($this->input->post('flag') == 'as') {
            $user_id = $this->input->post('user_id');

            if ($user_id) {
                $result = array();
                $select = 'carRegistrationNo, carModel, carColor, carDescription';
                $carData = $this->Usermodel->getPersonalInfo($user_id, $select);

                if (!empty($carData)) {
                    $result['Result']['ErrorCode'] = '0';
                    $result['Result']['ErrorMessage'] = 'Success';
                    $result['Result']['carProfile'] = $carData;
                } else {
                    $result['Result']['ErrorCode'] = '-2';
                    $result['Result']['ErrorMessage'] = "No records found";
                }
            } else {
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = 'Failure';
            }

            echo json_encode($result);
        } else {
            $this->load->view('webservices/getUserProfile');
        }
    }

    function carProfile() {
        if ($this->input->post('flag') == 'as') {
            $data = $result = array();

            $userID = $this->input->post('user_id');

            $data = array('carRegistrationNo' => $this->input->post('reg_number'),
                'carModel' => $this->input->post('model'),
                'carColor' => $this->input->post('color'),
                'carMake' => $this->input->post('make')
                    //'carDescription'	=> $this->input->post('description')
            );

            //Code for uploading file to temporary directory
            /* =========== Begins Here =========== */
            if (@$_FILES['car_photo']["name"] != "") {
                $ext = substr($_FILES['car_photo']["name"], strrpos($_FILES['car_photo']["name"], ".") + 1);
                $ext = strtolower($ext);

                $types_arr = explode("|", lang('allowed_types'));
                $photo_arr = array();

                if (in_array($ext, $types_arr)) {
                    if ($_FILES['car_photo']['size'] <= MAX_FILE_SIZE) {
                        $uploaddir = realpath(DIR_CAR_PHOTOS) . '/tmp/';
                        $uploadfile = $userID . "_" . date("Ymd_His") . "." . $ext;

                        if (move_uploaded_file($_FILES['car_photo']['tmp_name'], $uploaddir . $uploadfile)) {

                            $photo_arr["car_photo"]["file_name"] = $uploadfile;
                            $photo_arr["car_photo"]['file_ext'] = $ext;
                            $photo_arr["car_photo"]["name"] = $uploadfile;
                            $success = 'y';
                        } else {
                            $result['Result']['ErrorCode'] = '2';
                            $result['Result']['ErrorMessage'] = "Error in file uploading";
                            $success = "n";
                            echo json_encode($result);
                            return;
                        }
                    } else {
                        $result['Result']['ErrorCode'] = '2';
                        $result['Result']['ErrorMessage'] = "File size exceeds";
                        echo json_encode($result);
                        return;
                    }
                } else {
                    $success = "n";
                    $result['Result']['ErrorCode'] = '2';
                    $result['Result']['ErrorMessage'] = "Invalid file type";
                    echo json_encode($result);
                    return;
                }
            }


            /* =========== Ends Here =========== */
            //print_r($photo_arr);

            if ($userID && !empty($data)) {

                $this->Usermodel->updatePersonalInfo($userID, $data);
                if (!empty($photo_arr["car_photo"])) {
                    $dir = realpath(DIR_CAR_PHOTOS);
                    $org_file = $dir . "/tmp/" . $photo_arr["car_photo"]["file_name"];
                    $file = $dir . "/" . $userID . "_1." . $photo_arr["car_photo"]['file_ext'];
                    rename($org_file, $file);

                    $data_update = array('carPhoto' => $userID . "_1." . $photo_arr["car_photo"]['file_ext']);
                    $this->Usermodel->updatePersonalInfo($userID, $data_update);
                }
                $result['Result']['ErrorCode'] = '0';
                $result['Result']['ErrorMessage'] = "Success";
            } else {
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = "Failure";
            }

            echo json_encode($result);
        } else {
            $this->load->view('webservices/carProfile');
        }
    }

    function changePassword() {
        $data = array();
        $result = array();

        if ($this->input->post('flag') == 'as') {
            $userID = $this->input->post('user_id');
            $oldPassword = $this->input->post('old_password');
            $newPassword = $this->input->post('new_password');

            if ($userID && $oldPassword && $newPassword) {
                $update_flag = $this->Usermodel->changePassword($userID, base64_encode($oldPassword), base64_encode($newPassword));

                switch ($update_flag) {
                    case 1 : //Password changed successfully
                        $result['Result']['ErrorCode'] = '0';
                        $result['Result']['ErrorMessage'] = "Success";
                        break;

                    case 2 : //Invalid current Password 
                        $result['Result']['ErrorCode'] = '1';
                        $result['Result']['ErrorMessage'] = "Failure";
                        break;
                }
            } else {
                $result['Result']['ErrorCode'] = '3';
                $result['Result']['ErrorMessage'] = "All fields are mandatory";
            }

            echo json_encode($result);
        } else {
            $this->load->view('webservices/change_password', @$data);
        }
    }

    function createGroup() {
        if ($this->input->post('flag') == 'as') {
            $result = array();
            $user_id = $this->input->post('user_id');

            $data = array('group_name' => $this->input->post('group_name'),
                'group_owner' => $this->input->post('user_id'),
            );

            $group_members = $this->input->post('group_members');

            if ($group_members) {
                $group_members = explode(',', $group_members);

                if ($data['group_name'] && $data['group_owner']) {
                    if ($this->Groupmodel->groupAlreadyExists($data)) {
                        $group_flag = $this->Groupmodel->addGroup($data);

                        if ($group_flag) {
                            $group_id = $this->db->insert_id();

                            $this->Groupmodel->addGroupMembers($group_id, $group_members);

                            $result['Result']['ErrorCode'] = '0';
                            $result['Result']['ErrorMessage'] = "Success";
                            $result['Result']['group_id'] = $group_id;
                        } else {
                            $result['Result']['ErrorCode'] = '1';
                            $result['Result']['ErrorMessage'] = "Failure";
                        }
                    } else {
                        $result['Result']['ErrorCode'] = '4';
                        $result['Result']['ErrorMessage'] = "Group Already Exists";
                    }
                } else {
                    $result['Result']['ErrorCode'] = '3';
                    $result['Result']['ErrorMessage'] = "All fields are mandatory";
                }
            } else {
                $result['Result']['ErrorCode'] = '3';
                $result['Result']['ErrorMessage'] = "All fields are mandatory";
            }
            echo json_encode($result);
        } else {
            $this->load->view('webservices/createGroup');
        }
    }

    function updateGroup() {
        if ($this->input->post('flag') == 'as') {
            $result = array();
            $user_id = $this->input->post('user_id');
            $group_id = $this->input->post('group_id');

            $data_update = array('group_name' => $this->input->post('group_name'),
                'group_owner' => $user_id,
            );

            if (@$data_update['group_name']) {
                //if($this->Groupmodel->groupAlreadyExists($data_update, $group_id))
                {
                    $this->Groupmodel->updateGroup($group_id, $data_update);

                    $group_members = $this->input->post('group_members');
                    //$rows_affected1 = $this->db->affected_rows();

                    if ($group_members) {
                        $this->Groupmodel->deleteGroupMembers($group_id);
                        $group_members = explode(',', $group_members);

                        $this->Groupmodel->addGroupMembers($group_id, $group_members);
                    }

                    //if($this->db->affected_rows()>0)
                    {
                        $result['Result']['ErrorCode'] = '0';
                        $result['Result']['ErrorMessage'] = "Success";
                    }
                    /* else
                      {
                      $result['Result']['ErrorCode'] = '1';
                      $result['Result']['ErrorMessage'] = "Failure";
                      } */
                }
                /* else
                  {
                  $result['Result']['ErrorCode'] = '4';
                  $result['Result']['ErrorMessage'] = "Group with this name already exists";
                  } */
            } else {
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = "Failure";
            }

            echo json_encode($result);
        } else {
            $this->load->view('webservices/updateGroup');
        }
    }

    function deleteGroup() {
        if ($this->input->post('flag') == 'as') {
            $result = array();
            $user_id = $this->input->post('user_id');
            $group_id = $this->input->post('group_id');

            if ($user_id && $group_id) {
                $this->Groupmodel->deleteGroup($group_id, $user_id);

                if ($this->db->affected_rows() > 0) {
                    $result['Result']['ErrorCode'] = '0';
                    $result['Result']['ErrorMessage'] = "Success";
                } else {
                    $result['Result']['ErrorCode'] = '1';
                    $result['Result']['ErrorMessage'] = "Failure";
                }
            } else {
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = "Failure";
            }
            echo json_encode($result);
        } else {
            $this->load->view('webservices/deleteGroup');
        }
    }

    function getUserGroups() {
        if ($this->input->post('flag') == 'as') {
            $user_id = $this->input->post('user_id');
            $result = array();

            if ($user_id) {
                $groups_list = $this->Groupmodel->getGroupsList($user_id);

                $result['Result']['ErrorCode'] = '0';
                $result['Result']['ErrorMessage'] = "Success";
                $result['Result']['groupList'] = $groups_list;
            } else {
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = "Failure";
            }

            echo json_encode($result);
        } else {
            $this->load->view('webservices/group_list');
        }
    }

    function setFavouritePals() {
        if ($this->input->post('flag') == 'as') {
            $user_id = $this->input->post('user_id');
            $favourite_pals = $this->input->post('favourite_pals');
            $unfavourite_pals = $this->input->post('unfavourite_pals');

            if ($favourite_pals) {
                $favourite_list = explode(',', $favourite_pals);

                foreach ($favourite_pals as $k => $v) {
                    $data_update = array('');
                }
            }
        }
    }

    function getDuration() {
        if ($this->input->post('flag') == 'as') {
            $fromLat = $this->input->post('fromLat');
            $fromLongi = $this->input->post('fromLongi');

            $toLat = $this->input->post('toLat');
            $toLongi = $this->input->post('toLongi');

            $result = $this->Commfuncmodel->getDuration($fromLat, $fromLongi, $toLat, $toLongi);

            //echo "<pre>"; print_r($result);
            echo json_encode($result);
        } else {
            $this->load->view('webservices/get_duration');
        }
    }

    function createFetchRequest() {
        if ($this->input->post('flag') == 'as') {
            $result = array();
            $photo_arr = array();

            $user_id = $this->input->post('user_id');

            $data = array('fetch_creator_id' => $user_id,
                'fetch_flag' => $this->input->post('fetch_flag'),
                'fetch_location_name' => $this->input->post('location_name'),
                'fetch_to_lat' => $this->input->post('location_lat'),
                'fetch_to_longi' => $this->input->post('location_longi'),
                'fetch_datetime' => $this->input->post('fetch_datetime'),
                'fetch_timezone' => $this->input->post('fetch_timezone'),
                //Added Newly
                'fetch_headline' => $this->input->post('fetch_headline'),
                'fetch_description' => $this->input->post('fetch_description'),
                'fetch_image' => '',
            );

            //Code for uploading file to temporary directory
            /* =========== Begins Here =========== */
            if (@$_FILES['fetch_photo']["name"] != "") {
                $ext = substr($_FILES['fetch_photo']["name"], strrpos($_FILES['fetch_photo']["name"], ".") + 1);
                $ext = strtolower($ext);

                $types_arr = explode("|", lang('allowed_types'));

                if (in_array($ext, $types_arr)) {
                    if ($_FILES['fetch_photo']['size'] <= MAX_FILE_SIZE) {
                        $uploaddir = realpath(DIR_FETCH_PHOTOS) . '/tmp/';
                        $uploadfile = $user_id . "_" . date("Ymd_His") . "." . $ext;

                        if (move_uploaded_file($_FILES['fetch_photo']['tmp_name'], $uploaddir . $uploadfile)) {
                            $photo_arr["fetch_photo"]["file_name"] = $uploadfile;
                            $photo_arr["fetch_photo"]['file_ext'] = $ext;
                            $photo_arr["fetch_photo"]["name"] = $uploadfile;
                            $success = 'y';
                        } else {
                            $result['Result']['ErrorCode'] = '2';
                            $result['Result']['ErrorMessage'] = "Error in file uploading";
                            $success = "n";
                            echo json_encode($result);
                            return;
                        }
                    } else {
                        $result['Result']['ErrorCode'] = '2';
                        $result['Result']['ErrorMessage'] = "File size exceeds";
                        echo json_encode($result);
                        return;
                    }
                } else {
                    $success = "n";
                    $result['Result']['ErrorCode'] = '2';
                    $result['Result']['ErrorMessage'] = "Invalid file type";
                    echo json_encode($result);
                    return;
                }
            }
            /* =========== Ends Here =========== */
            $data['fetch_creation_date'] = date('Y-m-d H:i:s');
            $data['fetch_updation_date'] = date('Y-m-d H:i:s');

            $this->Transactionmodel->add($data);
            $fetch_id = $this->db->insert_id();

            $contact_ids = $this->input->post('contact_ids');

            $contacts_arr = explode(',', $contact_ids);
            foreach ($contacts_arr as $k => $v) {
                $data_users = array('ftu_transaction_id' => $fetch_id,
                    'ftu_user_type' => $this->input->post('user_type'),
                    'ftu_user_id' => $v,
                    'ftu_response' => 0,
                );

                $this->Transactionmodel->addTransactionUsers($data_users);
            }

            if ($fetch_id) {
                if (!empty($photo_arr["fetch_photo"])) {
                    $dir = realpath(DIR_FETCH_PHOTOS);

                    $org_file = $dir . "/tmp/" . $photo_arr["fetch_photo"]["file_name"];
                    $file = $dir . "/fetch" . $fetch_id . "." . $photo_arr["fetch_photo"]['file_ext'];
                    rename($org_file, $file);

                    $data_update = array('fetch_image' => "fetch" . $fetch_id . "." . $photo_arr["fetch_photo"]['file_ext'],
                        'fetch_updation_date' => date('Y-m-d H:i:s'),
                    );

                    $this->Transactionmodel->update($fetch_id, $data_update);

                    $data['fetch_image'] = base_url() . '/fetch_photos/fetch' . $fetch_id . "." . $photo_arr["fetch_photo"]['file_ext'];
                }//inner if ends here
                $result['Result']['ErrorCode'] = '0';
                $result['Result']['ErrorMessage'] = 'Success';
                $result['Result']['fetch_id'] = $fetch_id;
            } else {
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = 'Failure';
            }
            echo json_encode($result);

            if ($fetch_id) {
                $result1 = array();

                //Retrieve User data form DB
                $user_data = $this->Usermodel->getUserAppInfo($data['fetch_creator_id'], 'phone_no');
                $user_data1 = $this->Usermodel->getPersonalInfo($data['fetch_creator_id'], 'name');
                $alert_template = $this->Alertmodel->getAlertTemplate(5);

                $data['fetch_id'] = $fetch_id;
                //Fetch request generated
                $data['alert_type'] = 5;

                unset($data['fetch_creation_date']);
                unset($data['fetch_updation_date']);
                $result1['Result'] = array('fetch_data' => $data,
                    'name' => $user_data1['name'],
                    'phone_no' => $user_data['phone_no'],
                    'user_image_url' => $this->Commfuncmodel->getUserImage($data['fetch_creator_id']),
                );

                //Code for adding Alert into database
                foreach ($contacts_arr as $k => $v) {
                    $alert_data = array('alert_transaction_id' => $fetch_id,
                        'alert_from_user_id' => $data['fetch_creator_id'],
                        'alert_to_user_id' => $v,
                        'alert_type' => 5,
                        'alert_creation_time' => date('Y-m-d H:i:s'),
                    );

                    $this->Alertmodel->addUserAlert($alert_data);

                    //Code to send notifications
                    $result1['Result']['message'] = $alert_template['template_text'];
                    $msg = json_encode($result1);

                    $this->Notificationmodel->callnotification($data['fetch_creator_id'], $v, $msg);
                }
            }
        } else {
            $this->load->view('webservices/create_fetch_request');
        }
    }

    function updateFetchRequest() {
        if ($this->input->post('flag') == 'as') {
            $result = array();
            $photo_arr = array();

            $user_id = $this->input->post('user_id');
            $fetch_id = $this->input->post('fetch_id');

            $data_update = array(//'fetch_creator_id'		=>	$user_id,
                //'fetch_flag'			=>	$this->input->post('fetch_flag'),
                'fetch_location_name' => $this->input->post('location_name'),
                'fetch_to_lat' => $this->input->post('location_lat'),
                'fetch_to_longi' => $this->input->post('location_longi'),
                'fetch_datetime' => $this->input->post('fetch_datetime'),
                'fetch_timezone' => $this->input->post('fetch_timezone'),
                //Added Newly
                'fetch_headline' => $this->input->post('fetch_headline'),
                'fetch_description' => $this->input->post('fetch_description'),
            );

            //Code for uploading file to temporary directory
            /* =========== Begins Here =========== */
            if (@$_FILES['fetch_photo']["name"] != "") {
                $ext = substr($_FILES['fetch_photo']["name"], strrpos($_FILES['fetch_photo']["name"], ".") + 1);
                $ext = strtolower($ext);

                $types_arr = explode("|", lang('allowed_types'));

                if (in_array($ext, $types_arr)) {
                    if ($_FILES['fetch_photo']['size'] <= MAX_FILE_SIZE) {
                        $uploaddir = realpath(DIR_FETCH_PHOTOS) . '/tmp/';
                        $uploadfile = $user_id . "_" . date("Ymd_His") . "." . $ext;

                        if (move_uploaded_file($_FILES['fetch_photo']['tmp_name'], $uploaddir . $uploadfile)) {
                            $photo_arr["fetch_photo"]["file_name"] = $uploadfile;
                            $photo_arr["fetch_photo"]['file_ext'] = $ext;
                            $photo_arr["fetch_photo"]["name"] = $uploadfile;
                            $success = 'y';
                        } else {
                            $result['Result']['ErrorCode'] = '2';
                            $result['Result']['ErrorMessage'] = "Error in file uploading";
                            $success = "n";
                            echo json_encode($result);
                            return;
                        }
                    } else {
                        $result['Result']['ErrorCode'] = '2';
                        $result['Result']['ErrorMessage'] = "File size exceeds";
                        echo json_encode($result);
                        return;
                    }
                } else {
                    $success = "n";
                    $result['Result']['ErrorCode'] = '2';
                    $result['Result']['ErrorMessage'] = "Invalid file type";
                    echo json_encode($result);
                    return;
                }
            }
            /* =========== Ends Here =========== */
            $data_update['fetch_updation_date'] = date('Y-m-d H:i:s');
            $this->Transactionmodel->update($fetch_id, $data_update);

            /*
              $contact_ids = $this->input->post('contact_ids');
              $contacts_arr = explode(',', $contact_ids);

              foreach($contacts_arr as $k=>$v)
              {
              $data_users = array( 'ftu_transaction_id'	=>	$fetch_id,
              'ftu_user_type'		=>	$this->input->post('user_type'),
              'ftu_user_id'			=>	$v,
              'ftu_response'			=>	0,
              );

              $this->Transactionmodel->addTransactionUsers($data_users);
              }
             */

            if ($fetch_id) {
                if (!empty($photo_arr["fetch_photo"])) {
                    $dir = realpath(DIR_FETCH_PHOTOS);

                    $org_file = $dir . "/tmp/" . $photo_arr["fetch_photo"]["file_name"];
                    $file = $dir . "/fetch" . $fetch_id . "." . $photo_arr["fetch_photo"]['file_ext'];

                    rename($org_file, $file);
                    $data_update = array('fetch_image' => "fetch" . $fetch_id . "." . $photo_arr["fetch_photo"]['file_ext'],
                        'fetch_updation_date' => date('Y-m-d H:i:s'),
                    );

                    $this->Transactionmodel->update($fetch_id, $data_update);

                    $data['fetch_image'] = base_url() . '/fetch_photos/fetch' . $fetch_id . "." . $photo_arr["fetch_photo"]['file_ext'];
                }//inner if ends here

                $result['Result']['ErrorCode'] = '0';
                $result['Result']['ErrorMessage'] = 'Success';
                $result['Result']['fetch_id'] = $fetch_id;
            } else {
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = 'Failure';
            }
            echo json_encode($result);

            if ($fetch_id) {
                $fetch_data = $this->Transactionmodel->getTransactionDetails($fetch_id);

                if (@$fetch_data['fetch_image'])
                    $fetch_data['fetch_image'] = base_url() . '/fetch_photos/' . $fetch_data['fetch_image'];

                //Code for adding Alert into database & send Notifications
                $alert_template = $this->Alertmodel->getAlertTemplate(6);

                //Code for adding Alert into database
                unset($fetch_data['fetch_creation_date']);
                unset($fetch_data['fetch_updation_date']);

                //Fetch request modified
                $fetch_data['alert_type'] = 6; //Fetch request is modified
                //Retrieve User Info from DB
                $user_data = $this->Usermodel->getUserAppInfo($user_id, 'phone_no');
                $user_data1 = $this->Usermodel->getPersonalInfo($user_id, 'name');

                if ($user_id != $fetch_data['fetch_creator_id']) {
                    $alert_data = array('alert_transaction_id' => $fetch_id,
                        'alert_from_user_id' => $user_id,
                        'alert_to_user_id' => $fetch_data['fetch_creator_id'],
                        'alert_type' => 6,
                        'alert_creation_time' => date('Y-m-d H:i:s'),
                    );

                    $this->Alertmodel->addUserAlert($alert_data);

                    $result1['Result'] = array('fetch_data' => $fetch_data,
                        'name' => $user_data1['name'],
                        'phone_no' => $user_data['phone_no'],
                        'user_image_url' => $this->Commfuncmodel->getUserImage($fetch_data['fetch_creator_id']),
                        'message' => $alert_template['template_text'],
                    );
                    $msg = json_encode($result1);

                    $this->Notificationmodel->callnotification($user_id, $fetch_data['fetch_creator_id'], $msg);
                }

                $transaction_users = array();
                $select1 = 'ftu_user_id';
                $transaction_users = $this->Transactionmodel->getTransactionUsers($fetch_id, $user_id, $select1);

                if (!empty($transaction_users)) {
                    foreach ($transaction_users as $k => $v) {
                        $alert_data = array('alert_transaction_id' => $fetch_id,
                            'alert_from_user_id' => $user_id,
                            'alert_to_user_id' => $v['ftu_user_id'],
                            'alert_type' => 6,
                            'alert_creation_time' => date('Y-m-d H:i:s'),
                        );

                        $this->Alertmodel->addUserAlert($alert_data);

                        //Code to send notifications
                        $result1['Result'] = array('fetch_data' => $fetch_data,
                            'name' => $user_data1['name'],
                            'phone_no' => $user_data['phone_no'],
                            'user_image_url' => $this->Commfuncmodel->getUserImage($fetch_data['fetch_creator_id']),
                            'message' => $alert_template['template_text'],
                        );

                        $msg = json_encode($result1);

                        $this->Notificationmodel->callnotification($user_id, $v['ftu_user_id'], $msg);
                    }
                }
            }
        } else {
            $this->load->view('webservices/update_fetch_request');
        }
    }

    function initiateFetchRequest() {
        if ($this->input->post('flag') == 'as') {
            $user_id = $this->input->post('user_id');
            $fetch_id = $this->input->post('fetch_id');

            $data = array('fetch_status' => 4,
                'fetch_from_lat' => $this->input->post('from_lat'),
                'fetch_from_longi' => $this->input->post('from_longi'),
                'fetch_curr_lat' => $this->input->post('from_lat'),
                'fetch_curr_longi' => $this->input->post('from_longi'),
                'fetch_updation_date' => date('Y-m-d H:i:s'),
            );

            $this->Transactionmodel->update($fetch_id, $data);

            if ($this->db->affected_rows() > 0) {
                //$select = 'fetch_curr_lat, fetch_curr_longi, fetch_to_lat, fetch_to_longi';
                $fetch_data = $this->Transactionmodel->getTransactionDetails($fetch_id);

                if (@$fetch_data['fetch_image'])
                    $fetch_data['fetch_image'] = base_url() . '/fetch_photos/' . $fetch_data['fetch_image'];

                //Calculating distance & time
                $res = $this->Commfuncmodel->getDuration($fetch_data['fetch_curr_lat'], $fetch_data['fetch_curr_longi'], $fetch_data['fetch_to_lat'], $fetch_data['fetch_to_longi']);

                $result['Result']['ErrorCode'] = '0';
                $result['Result']['ErrorMessage'] = 'Success';
                $result['Result']['fetch_id'] = $fetch_id;
                $result['Result']['duration'] = $res['duration'];
                $result['Result']['distance'] = $res['distance'];

                if ($fetch_id) {
                    //Code for adding Alert into database
                    //Code to send notifications
                    $alert_template = $this->Alertmodel->getAlertTemplate(4);

                    unset($fetch_data['fetch_creation_date']);
                    unset($fetch_data['fetch_updation_date']);

                    //Fetch request generated
                    $fetch_data['alert_type'] = 4; //Fetch request is intiated
                    //Code for adding Alert into database
                    if ($user_id != $fetch_data['fetch_creator_id']) {
                        $alert_data = array('alert_transaction_id' => $fetch_id,
                            'alert_from_user_id' => $user_id,
                            'alert_to_user_id' => $fetch_data['fetch_creator_id'],
                            'alert_type' => 4,
                            'alert_creation_time' => date('Y-m-d H:i:s'),
                        );

                        $this->Alertmodel->addUserAlert($alert_data);

                        //Retrieve User Info from DB
                        $user_data = $this->Usermodel->getUserAppInfo($fetch_data['fetch_creator_id'], 'phone_no');
                        $user_data1 = $this->Usermodel->getPersonalInfo($fetch_data['fetch_creator_id'], 'name');

                        $result1['Result'] = array('fetch_data' => $fetch_data,
                            'name' => $user_data1['name'],
                            'phone_no' => $user_data['phone_no'],
                            'user_image_url' => $this->Commfuncmodel->getUserImage($fetch_data['fetch_creator_id']),
                            'message' => $alert_template['template_text'],
                        );

                        $msg = json_encode($result1);

                        $this->Notificationmodel->callnotification($user_id, $fetch_data['fetch_creator_id'], $msg);
                    }

                    $transaction_users = array();
                    $select1 = 'ftu_user_id';
                    $where_str = 'ftu_response=1';
                    $transaction_users = $this->Transactionmodel->getTransactionUsers($fetch_id, $user_id, $select1, $where_str);

                    if (!empty($transaction_users)) {
                        foreach ($transaction_users as $k => $v) {
                            $alert_data = array('alert_transaction_id' => $fetch_id,
                                'alert_from_user_id' => $user_id,
                                'alert_to_user_id' => $v['ftu_user_id'],
                                'alert_type' => 4,
                                'alert_creation_time' => date('Y-m-d H:i:s'),
                            );

                            $this->Alertmodel->addUserAlert($alert_data);

                            //Code to send notifications
                            //Retrieve User Info from DB
                            $user_data = $this->Usermodel->getUserAppInfo($fetch_data['fetch_creator_id'], 'phone_no');
                            $user_data1 = $this->Usermodel->getPersonalInfo($fetch_data['fetch_creator_id'], 'name');

                            $result1['Result'] = array('fetch_data' => $fetch_data,
                                'name' => $user_data1['name'],
                                'phone_no' => $user_data['phone_no'],
                                'user_image_url' => $this->Commfuncmodel->getUserImage($fetch_data['fetch_creator_id']),
                                'message' => $alert_template['template_text'],
                            );

                            $msg = json_encode($result1);

                            $this->Notificationmodel->callnotification($user_id, $v['ftu_user_id'], $msg);
                        }
                    }
                }
            } else {
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = 'Failed';
            }
            echo json_encode($result);
        } else {
            $this->load->view('webservices/fetch_request_initiate');
        }
    }

    function respondToFetchRequest() {
        if ($this->input->post('flag') == 'as') {
            $result = array();
            $user_id = $this->input->post('user_id');
            $fetch_id = $this->input->post('fetch_id');
            $response = $this->input->post('fetch_response');
            $success_flg1 = $success_flg2 = $success_flg3 = FALSE;
            $fetch_data = $this->Transactionmodel->getTransactionDetails($fetch_id);

            if (@$fetch_data['fetch_image'])
                $fetch_data['fetch_image'] = base_url() . 'fetch_photos/' . $fetch_data['fetch_image'];

            // code added by ashwini on 11-04-2014
            if ($response == 5) {
                $where_str = 'ftu_response = 1';
                $select = 'ftu_user_id';
                $transaction_users_id = $this->Transactionmodel->getTransactionUsers($fetch_id, $user_id, $select, $where_str);
                $reset_users_id = array_map('reset', $transaction_users_id);
                foreach ($reset_users_id as $usr_id) {
                    $message = 'Your contact has ended this Fetch event. Look out for them. They should be nearby!';
                    $msg = json_encode($message);
                    $this->Notificationmodel->callnotification($user_id, $usr_id, $msg);
                }

                //chat history delete
                $this->Transactionmodel->deleteChatHistory($fetch_id);

                //update the users status as completed
                $data_update = array('ftu_response' => $response
                );

                $success_flg3 = $this->Transactionmodel->updateTransactionUsers($fetch_id, $user_id, $data_update);

                //update the transaction status as completed
                $data_update = array('fetch_status' => $response,
                    'fetch_updation_date' => date('Y-m-d H:i:s')
                );

                $success_flg3 = $this->Transactionmodel->update($fetch_id, $data_update);
                $success_flg3 = TRUE;
            }// code ends here 
            else if ($user_id == $fetch_data['fetch_creator_id']) {
                $data_update = array('fetch_status' => 3);
                $this->Transactionmodel->update($fetch_id, $data_update);
                $success_flg1 = TRUE;
            } else if ($response == 1 && ($fetch_data['fetch_flag'] == 2 && $fetch_data['fetch_status'] == 1)) {
                $result['Result']['ErrorCode'] = '8';
                $result['Result']['ErrorMessage'] = 'Fetchee has withdrawn their Fetch request';
                $this->Transactionmodel->deleteTrasactionSingleUser($fetch_id, $user_id);
                echo json_encode($result);
                return;
            } else {
                $success_flg1 = FALSE;

                //code added by ashwini on 14-04-2014
                $total_count = $this->Transactionmodel->getTransactionResponseCount($fetch_id);
                $accept_count = $this->Transactionmodel->getTransactionResponseCount($fetch_id, 1);

                if ($response == 1 && $accept_count == 0 && $accept_count < $total_count) {
                    $result['Result']['FetcheeLeader'] = '0'; //Fetchee Leader
                } else {
                    $result['Result']['FetcheeLeader'] = '1'; //not a Fetchee Leader
                }
                // code ends here


                $data_update = array('ftu_response' => $this->input->post('fetch_response'));
                $success_flg1 = $this->Transactionmodel->updateTransactionUsers($fetch_id, $user_id, $data_update);

                $total_cnt = $this->Transactionmodel->getTransactionResponseCount($fetch_id);
                $accept_cnt = $this->Transactionmodel->getTransactionResponseCount($fetch_id, 1);
                $reject_cnt = $this->Transactionmodel->getTransactionResponseCount($fetch_id, 2);
                $cancel_cnt = $this->Transactionmodel->getTransactionResponseCount($fetch_id, 3);
                //$pending_cnt =  $this->Transactionmodel->getTransactionResponseCount($fetch_id, 0);

                $data_update = array();
                $success_flg2 = FALSE;
                if ($accept_cnt > 0) {
                    $fetch_status = 1;
                } else if ($reject_cnt == $total_cnt) {
                    $fetch_status = 2;
                } else if ($cancel_cnt == $total_cnt) {
                    $fetch_status = 3;
                } else if (($reject_cnt + $cancel_cnt) == $total_cnt) {
                    if ($reject_cnt >= $cancel_cnt)
                        $fetch_status = 2;
                    else //if($reject_cnt <$cancel_cnt)
                        $fetch_status = 3;
                }
                else {
                    $fetch_status = 0;
                }

                $data_update = array('fetch_status' => $fetch_status,
                    'fetch_updation_date' => date('Y-m-d H:i:s')
                );

                $success_flg2 = $this->Transactionmodel->update($fetch_id, $data_update);
            }

            if ($success_flg1 || $success_flg2 || $success_flg3) {
                $result['Result']['ErrorCode'] = '0';
                $result['Result']['ErrorMessage'] = 'Success';
                $result['Result']['fetch_id'] = $fetch_id;
            } else {
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = 'Failure';
            }

            echo json_encode($result);

            //Code for Alerts & Notifications
            if ($success_flg1 || @$success_flg2 || $success_flg3) {
                $fetch_data['alert_type'] = $response; //Fetch request response
                unset($fetch_data['fetch_creation_date']);
                unset($fetch_data['fetch_updation_date']);

                if ($user_id == $fetch_data['fetch_creator_id']) {
                    $users_list = $this->Transactionmodel->getTransactionUsers($fetch_id);
                    $alert_template = $this->Alertmodel->getAlertTemplate($response);

                    //Retrieve User Info from DB
                    $user_data = $this->Usermodel->getUserAppInfo($user_id, 'phone_no');
                    $user_data1 = $this->Usermodel->getPersonalInfo($user_id, 'name');

                    //Code to send notifications
                    $result1['Result'] = array('fetch_data' => $fetch_data,
                        'name' => $user_data1['name'],
                        'phone_no' => $user_data['phone_no'],
                        'user_image_url' => $this->Commfuncmodel->getUserImage($fetch_data['fetch_creator_id']),
                        'message' => $alert_template['template_text'],
                            //'alert_type'	=>	$response,	
                    );

                    $msg = json_encode($result1);

                    foreach ($users_list as $k => $v) {
                        //Code for adding Alert into database
                        $alert_data = array('alert_transaction_id' => $fetch_id,
                            'alert_from_user_id' => $user_id,
                            'alert_to_user_id' => $v['ftu_user_id'],
                            'alert_type' => $response,
                            'alert_creation_time' => date('Y-m-d H:i:s'),
                        );

                        $this->Alertmodel->addUserAlert($alert_data);

                        //Send Notification
                        $this->Notificationmodel->callnotification($user_id, $v['ftu_user_id'], $msg);
                    }
                } else if ($user_id != $fetch_data['fetch_creator_id']) {
                    //Retrieve User Info from DB
                    $user_data = $this->Usermodel->getUserAppInfo($user_id, 'phone_no');
                    $user_data1 = $this->Usermodel->getPersonalInfo($user_id, 'name');
                    //Alert Template
                    $alert_template = $this->Alertmodel->getAlertTemplate($response);

                    //Code for adding Alert into database
                    $alert_data = array('alert_transaction_id' => $fetch_id,
                        'alert_from_user_id' => $user_id,
                        'alert_to_user_id' => $fetch_data['fetch_creator_id'],
                        'alert_type' => $response,
                        'alert_creation_time' => date('Y-m-d H:i:s'),
                    );

                    $this->Alertmodel->addUserAlert($alert_data);

                    //Code to send notifications
                    $result1['Result'] = array('fetch_data' => $fetch_data,
                        'name' => $user_data1['name'],
                        'phone_no' => $user_data['phone_no'],
                        'user_image_url' => $this->Commfuncmodel->getUserImage($user_id),
                        'message' => $alert_template['template_text'],
                            //'alert_type'	=>	$response,
                    );

                    $msg = json_encode($result1);

                    //Send Notification
                    $this->Notificationmodel->callnotification($user_id, $fetch_data['fetch_creator_id'], $msg);
                }
            }
        } else {
            $this->load->view('webservices/fetch_request_response');
        }
    }

    //delete fetch request
    function deleteFetchRequest() {
        if ($this->input->post('flag') == 'as') {
            $user_id = $this->input->post('user_id');
            $fetch_transaction_id = $this->input->post('fetch_transaction_id');
            $ary = array();
            $result = array();
            $ary = $this->Transactionmodel->getTransactionDetails($fetch_transaction_id);
            if (!empty($ary)) {
                //delete transaction
                $this->Transactionmodel->deleteTrasaction($user_id, $fetch_transaction_id);
                $result['Result']['ErrorCode'] = '0';
                $result['Result']['ErrorMessage'] = 'Success';
            } else {
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = 'Failure';
            }
            echo json_encode($result);
        } else {
            $this->load->view('webservices/delete_fetch_request');
        }
    }

    /*
     * This method is used to mark transaction as Finished
     * It accepts 2 values user ID & transaction ID via POST method
     */

    function completeFetchRequest() {
        if ($this->input->post('flag') == 'as') {
            $user_id = $this->input->post('user_id');
            $fetch_id = $this->input->post('fetch_transaction_id');

            //Retrieve fetch data from DB
            $fetch_data = $this->Transactionmodel->getTransactionDetails($fetch_id, 'fetch_id,fetch_flag,fetch_creator_id,fetch_status');

            if ($fetch_data['fetch_status'] == 4) {
                if ($user_id == $fetch_data['fetch_creator_id']) { //User is fetcher in this Transaction
                    $data_update = array('fetch_status' => 7,
                        'fetch_updation_date' => date("Y-m-d H:i:s"),
                    );

                    $this->Transactionmodel->update($fetch_id, $data_update);

                    //Here control goes only when User fetchee in this Trasnaction
                    $result['Result']['ErrorCode'] = '0';
                    $result['Result']['ErrorMessage'] = 'Succcess';

                    //Send notification code

                    if ($fetch_id) {
                        $fetch_data = $this->Transactionmodel->getTransactionDetails($fetch_id);

                        if (@$fetch_data['fetch_image'])
                            $fetch_data['fetch_image'] = base_url() . '/fetch_photos/' . $fetch_data['fetch_image'];

                        //Code for adding Alert into database & send Notifications
                        $alert_template = $this->Alertmodel->getAlertTemplate(7);

                        //Code for adding Alert into database
                        unset($fetch_data['fetch_creation_date']);
                        unset($fetch_data['fetch_updation_date']);

                        //Fetch request modified
                        $fetch_data['alert_type'] = 7; //Fetch request is modified
                        //Retrieve User Info from DB
                        $user_data = $this->Usermodel->getUserAppInfo($user_id, 'phone_no');
                        $user_data1 = $this->Usermodel->getPersonalInfo($user_id, 'name');

                        if ($user_id != $fetch_data['fetch_creator_id']) {
                            $alert_data = array('alert_transaction_id' => $fetch_id,
                                'alert_from_user_id' => $user_id,
                                'alert_to_user_id' => $fetch_data['fetch_creator_id'],
                                'alert_type' => 7,
                                'alert_creation_time' => date('Y-m-d H:i:s'),
                            );

                            $this->Alertmodel->addUserAlert($alert_data);

                            $result1['Result'] = array('fetch_data' => $fetch_data,
                                'name' => $user_data1['name'],
                                'phone_no' => $user_data['phone_no'],
                                'user_image_url' => $this->Commfuncmodel->getUserImage($fetch_data['fetch_creator_id']),
                                'message' => $alert_template['template_text'],
                            );
                            $msg = json_encode($result1);

                            $this->Notificationmodel->callnotification($user_id, $fetch_data['fetch_creator_id'], $msg);
                        }

                        $transaction_users = array();
                        $select1 = 'ftu_user_id';
                        $transaction_users = $this->Transactionmodel->getTransactionUsers($fetch_id, $user_id, $select1);

                        if (!empty($transaction_users)) {
                            foreach ($transaction_users as $k => $v) {
                                $alert_data = array('alert_transaction_id' => $fetch_id,
                                    'alert_from_user_id' => $user_id,
                                    'alert_to_user_id' => $v['ftu_user_id'],
                                    'alert_type' => 7,
                                    'alert_creation_time' => date('Y-m-d H:i:s'),
                                );

                                $this->Alertmodel->addUserAlert($alert_data);

                                //Code to send notifications
                                $result1['Result'] = array('fetch_data' => $fetch_data,
                                    'name' => $user_data1['name'],
                                    'phone_no' => $user_data['phone_no'],
                                    'user_image_url' => $this->Commfuncmodel->getUserImage($fetch_data['fetch_creator_id']),
                                    'message' => $alert_template['template_text'],
                                );

                                $msg = json_encode($result1);

                                $this->Notificationmodel->callnotification($user_id, $v['ftu_user_id'], $msg);
                            }
                        }
                    }
                } else { //() //User is fetchee in this Transaction
                    $where_str = 'ftu_user_id=' . $user_id . '';
                    $fetch_users = $this->Transactionmodel->getTransactionUsers($fetch_id, NULL, '*', $where_str);

                    if (empty($fetch_users)) {
                        //Here control goes only when user is invalid in this Transaction
                        $result['Result']['ErrorCode'] = '1';
                        $result['Result']['ErrorMessage'] = 'Failure';
                    } else {
                        $data_update = array('fetch_status' => 7,
                            'fetch_updation_date' => date("Y-m-d H:i:s"),
                        );

                        $this->Transactionmodel->update($fetch_id, $data_update);

                        //Here control goes only when User fetchee in this Trasnaction
                        $result['Result']['ErrorCode'] = '0';
                        $result['Result']['ErrorMessage'] = 'Success';

                        //new line code
                        //
                  
                        if ($fetch_id) {
                            $fetch_data = $this->Transactionmodel->getTransactionDetails($fetch_id);

                            if (@$fetch_data['fetch_image'])
                                $fetch_data['fetch_image'] = base_url() . '/fetch_photos/' . $fetch_data['fetch_image'];

                            //Code for adding Alert into database & send Notifications
                            $alert_template = $this->Alertmodel->getAlertTemplate(7);

                            //Code for adding Alert into database
                            unset($fetch_data['fetch_creation_date']);
                            unset($fetch_data['fetch_updation_date']);

                            //Fetch request modified
                            $fetch_data['alert_type'] = 7; //Fetch request is modified
                            //Retrieve User Info from DB
                            $user_data = $this->Usermodel->getUserAppInfo($user_id, 'phone_no');
                            $user_data1 = $this->Usermodel->getPersonalInfo($user_id, 'name');

                            if ($user_id != $fetch_data['fetch_creator_id']) {
                                $alert_data = array('alert_transaction_id' => $fetch_id,
                                    'alert_from_user_id' => $user_id,
                                    'alert_to_user_id' => $fetch_data['fetch_creator_id'],
                                    'alert_type' => 7,
                                    'alert_creation_time' => date('Y-m-d H:i:s'),
                                );

                                $this->Alertmodel->addUserAlert($alert_data);

                                $result1['Result'] = array('fetch_data' => $fetch_data,
                                    'name' => $user_data1['name'],
                                    'phone_no' => $user_data['phone_no'],
                                    'user_image_url' => $this->Commfuncmodel->getUserImage($fetch_data['fetch_creator_id']),
                                    'message' => $alert_template['template_text'],
                                );
                                $msg = json_encode($result1);

                                $this->Notificationmodel->callnotification($user_id, $fetch_data['fetch_creator_id'], $msg);
                            }

                            $transaction_users = array();
                            $select1 = 'ftu_user_id';
                            $transaction_users = $this->Transactionmodel->getTransactionUsers($fetch_id, $user_id, $select1);

                            if (!empty($transaction_users)) {
                                foreach ($transaction_users as $k => $v) {
                                    $alert_data = array('alert_transaction_id' => $fetch_id,
                                        'alert_from_user_id' => $user_id,
                                        'alert_to_user_id' => $v['ftu_user_id'],
                                        'alert_type' => 7,
                                        'alert_creation_time' => date('Y-m-d H:i:s'),
                                    );

                                    $this->Alertmodel->addUserAlert($alert_data);

                                    //Code to send notifications
                                    $result1['Result'] = array('fetch_data' => $fetch_data,
                                        'name' => $user_data1['name'],
                                        'phone_no' => $user_data['phone_no'],
                                        'user_image_url' => $this->Commfuncmodel->getUserImage($fetch_data['fetch_creator_id']),
                                        'message' => $alert_template['template_text'],
                                    );

                                    $msg = json_encode($result1);

                                    $this->Notificationmodel->callnotification($user_id, $v['ftu_user_id'], $msg);
                                }
                            }
                        }
                        //new line code ended
                    }
                }
            } else if ($fetch_data['fetch_status'] == 7) {
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = 'Failure. This fetch request is already completed.';
            } else { //if($fetch_data['fetch_status'] <4 )
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = 'Failure. This fetch request is not initiated yet.';
            }
            echo json_encode($result);
        } else {
            $this->load->view('webservices/fetch_request_completed');
        }
    }

    function getAlertsList() {
        if ($this->input->post('flag') == 'as') {
            $user_id = $this->input->post('user_id');
            $page_no = $this->input->post('page_no');

            $result = array();
            $alerts_list = array();
            $alerts_list = $this->Alertmodel->getUserAlerts($user_id, $page_no);
            $name = array();
            $alert_template = array();

            foreach ($alerts_list as $k => $v) {
                $user_data = $this->Usermodel->getPersonalInfo($v['alert_from_user_id'], 'name');
                $user_data1 = $this->Usermodel->getUserAppInfo($v['alert_from_user_id'], 'phone_no');

                $alert_template = $this->Alertmodel->getAlertTemplate($v['alert_type'], 'template_text');
                $alerts_list[$k]['alert_message'] = $alert_template['template_text'];
                $alerts_list[$k]['name'] = $user_data['name'];
                $alerts_list[$k]['phone_no'] = $user_data1['phone_no'];
                $alerts_list[$k]['user_image'] = base_url() . '/images/' . $v['alert_from_user_id'] . '.jpg';
            }

            if (!empty($alerts_list)) {
                $result['Result']['ErrorCode'] = '0';
                $result['Result']['ErrorMessage'] = 'Success';
                $result['Result']['alerts_list'] = $alerts_list;
                echo json_encode($result);
            } else {
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = 'Failure';
                echo json_encode($result);
            }
        } else {
            $this->load->view('webservices/getAlertsList');
        }
    }

    function getBlockContacts() {
        if ($this->input->post('flag') == 'as') {
            $user_id = $this->input->post('user_id');
            $blockedcontacts = $this->Usermodel->getBlockedPals($user_id, 'fk_user_app_info_pal_id');
            if (!empty($blockedcontacts)) {
                $result['Result']['ErrorCode'] = '0';
                $result['Result']['ErrorMessage'] = 'Success';
                $result['Result']['blocked_list'] = $blockedcontacts;
                echo json_encode($result);
            } else {
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = 'Failure';
                echo json_encode($result);
            }
        } else {
            $this->load->view('webservices/getBlockContacts');
        }
    }

    function getETA() {
        if ($this->input->post('flag') == 'as') {
            $res = array();

            $user_id = $this->input->post('user_id');
            $fetch_id = $this->input->post('fetch_id');
            $fetch_flag = $this->input->post('fetch_flag');

            if ($fetch_flag == '1') {
                $data = array('fetch_curr_lat' => $this->input->post('from_lat'),
                    'fetch_curr_longi' => $this->input->post('from_longi'),
                    'fetch_updation_date' => date('Y-m-d H:i:s'),
                );

                $this->Transactionmodel->update($fetch_id, $data);
            }

            $select = 'fetch_from_lat, fetch_from_longi, fetch_curr_lat, fetch_curr_longi, fetch_to_lat, fetch_to_longi';
            $fetch_data = $this->Transactionmodel->getTransactionDetails($fetch_id, $select);

            $res = $this->Commfuncmodel->getDuration($fetch_data['fetch_from_lat'], $fetch_data['fetch_from_longi'], $fetch_data['fetch_to_lat'], $fetch_data['fetch_to_longi']);
            $res1 = $this->Commfuncmodel->getDuration($fetch_data['fetch_curr_lat'], $fetch_data['fetch_curr_longi'], $fetch_data['fetch_to_lat'], $fetch_data['fetch_to_longi']);


            $result = array();
            if (!empty($res)) {
                $result['Result']['ErrorCode'] = '0';
                $result['Result']['ErrorMessage'] = 'Success';
                $result['Result']['total_duration'] = $res['duration'];
                $result['Result']['total_distance'] = $res['distance'];
                $result['Result']['curr_duration'] = $res1['duration'];
                $result['Result']['curr_distance'] = $res1['distance'];
            } else {
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = 'Failure';
            }
            echo json_encode($result);
        } else {
            $this->load->view('webservices/getETA');
        }
    }

    function getETALocation() {
        if ($this->input->post('flag') == 'as') {
            $res = array();

            $user_id = $this->input->post('user_id');
            $fetch_id = $this->input->post('fetch_id');
            $fetch_flag = $this->input->post('fetch_flag');

            if ($fetch_flag == '1') {
                $data = array('fetch_curr_lat' => $this->input->post('from_lat'),
                    'fetch_curr_longi' => $this->input->post('from_longi'),
                    'fetch_updation_date' => date('Y-m-d H:i:s'),
                );

                $this->Transactionmodel->update($fetch_id, $data);
            }

            $select = 'fetch_from_lat, fetch_from_longi, fetch_curr_lat, fetch_curr_longi, fetch_to_lat, fetch_to_longi';
            $fetch_data = $this->Transactionmodel->getTransactionDetails($fetch_id, $select);



//            $res = $this->Commfuncmodel->getDuration($fetch_data['fetch_from_lat'], $fetch_data['fetch_from_longi'], $fetch_data['fetch_to_lat'], $fetch_data['fetch_to_longi']);
//            $res1 = $this->Commfuncmodel->getDuration($fetch_data['fetch_curr_lat'], $fetch_data['fetch_curr_longi'], $fetch_data['fetch_to_lat'], $fetch_data['fetch_to_longi']);


            $result = array();
            if (!empty($fetch_data)) {

                $result['Result']['ErrorCode'] = '0';
                $result['Result']['ErrorMessage'] = 'Success';
                $result['Result']['fetch_curr_lat'] = $fetch_data['fetch_curr_lat'];
                $result['Result']['fetch_curr_longi'] = $fetch_data['fetch_curr_longi'];
//              
            } else {
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = 'Failure';
            }
            echo json_encode($result);
        } else {
            $this->load->view('webservices/getCurLocation');
        }
    }

    function BlockUnblockContacts() {
        if ($this->input->post('flag') == 'as') {
            $user_id = $this->input->post('user_id');
            $change_block_user_id = $this->input->post('change_block_user_id');
            $blockflag = $this->input->post('blockflag');
            $blockedcontacts = $this->Usermodel->Unblockpals($user_id, $change_block_user_id, $blockflag);
            if ($blockedcontacts == 1) {
                $result['Result']['ErrorCode'] = '0';
                $result['Result']['ErrorMessage'] = 'Success';
                echo json_encode($result);
            } else {
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = 'Failure';
                echo json_encode($result);
            }
        } else {
            $this->load->view('webservices/BlockUnblockContacts');
        }
    }

    function test() {

        if ($this->input->post('flag') == 'as') {
            $user_id = $this->input->post('user_id');
            $contact_ids = $this->input->post('contact_ids');
            $msg = "hie";
            $blockedcontacts = $this->Notificationmodel->callnotification($user_id, $contact_ids, $msg);
            //$this->Notificationmodel->snd();
        } else {
            $this->load->view('webservices/test');
        }
    }

    function logout() {
        if ($this->input->post('flag') == 'as') {
            $user_id = $this->input->post('user_id');

            $data_update = array('device_type' => NULL,
                'phone_registration_id' => '',
                'is_logged_in' => 0,
            );

            //$where_arr = array(	'is_logged_in'	=>	1	);
            $flag = $this->Usermodel->updateUserDevices($user_id, $data_update); //, $where_arr);

            if ($flag) {
                $result['Result']['ErrorCode'] = '0';
                $result['Result']['ErrorMessage'] = 'Success';
            } else {
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = 'Failure';
            }
            echo json_encode($result);
        } else {
            $this->load->view('webservices/logout');
        }
    }

    function movefbimage() {
        if ($this->input->post('flag') == 'as') {
            $user_id = $this->input->post('user_id');
            $social_id = $this->input->post('social_id');
            $validateuserdata = $this->Usermodel->getUserAppInfo($user_id, 'social_id');
            if (!empty($validateuserdata)) {
                $dbsocial_id = $validateuserdata['social_id'];

                if ($social_id == $dbsocial_id) {

                    $headers = get_headers('https://graph.facebook.com/' . $social_id . '/picture', 1);
                    // just a precaution, check whether the header isset...
                    if (isset($headers['Location'])) {
                        $url = $headers['Location']; // string
                    } else {
                        $url = false; // nothing there? .. weird, but okay!
                    }
                    $destination = '/home1/capstoq5/public_html/fetch_new/app/images/' . $user_id . '.jpg';
                    $content = file_get_contents($url);
                    $fp = fopen($destination, "w");
                    fwrite($fp, $content);
                    fclose($fp);
                }
            }
        } else {
            $this->load->view('webservices/movefbimage');
        }
    }

    function getTransaction() {
        if ($this->input->post('flag') == 'as') {
            $user_id = $this->input->post('user_id');
            $fetch_transaction_id = $this->input->post('fetch_transaction_id');
            $fetchdetails = $this->Transactionmodel->getTransactionJoinUsers($fetch_transaction_id, $user_id);
            if (!empty($fetchdetails)) {
                $result['Result']['ErrorCode'] = '0';
                $result['Result']['ErrorMessage'] = 'Success';
                $result['Result']['transaction_details'] = $fetchdetails;
                echo json_encode($result);
            } else {
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = 'Failure';
                echo json_encode($result);
            }
        } else {
            $this->load->view('webservices/getTransaction');
        }
    }

    function getGroupMembers() {
        if ($this->input->post('flag') == 'as') {
            $result = array();
            $group_id = $this->input->post('group_id');
            $group_members = $this->Groupmodel->getGroupMembers($group_id);

            if (!empty($group_members)) {
                $result['Result']['ErrorCode'] = '0';
                $result['Result']['ErrorMessage'] = 'Success';
                $result['Result']['group_members'] = $group_members;
            } else {
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = 'Failure';
            }
            echo json_encode($result);
        } else {
            $this->load->view('webservices/getGroupMembers');
        }
    }

    //new schedular
    function schedularNew() {
        if ($this->input->post('flag') == 'as') {
            $user_id = $this->input->post('user_id');
            $last_updated_time = $this->input->post('last_updated_time');
            $ary = $this->Fetchmodel->schedular_new($user_id, $last_updated_time);
        } else {
            $this->load->view('webservices/schedular_new');
        }
    }

    function carUpload() {
        if ($this->input->post('flag') == 'as') {
            $userID = $this->input->post('user_id');

            //Code for uploading file to temporary directory
            /* =========== Begins Here =========== */
            if (@$_FILES['car_photo']["name"] != "") {
                $ext = substr($_FILES['car_photo']["name"], strrpos($_FILES['car_photo']["name"], ".") + 1);
                $ext = strtolower($ext);

                $types_arr = explode("|", lang('allowed_types'));
                $photo_arr = array();

                if (in_array($ext, $types_arr)) {
                    if ($_FILES['car_photo']['size'] <= MAX_FILE_SIZE) {
                        $uploaddir = realpath(DIR_CAR_PHOTOS) . '/tmp/';
                        $uploadfile = $userID . "_" . date("Ymd_His") . "." . $ext;

                        if (move_uploaded_file($_FILES['car_photo']['tmp_name'], $uploaddir . $uploadfile)) {

                            $photo_arr["car_photo"]["file_name"] = $uploadfile;
                            $photo_arr["car_photo"]['file_ext'] = $ext;
                            $photo_arr["car_photo"]["name"] = $uploadfile;
                            $success = 'y';
                        } else {
                            $result['Result']['ErrorCode'] = '2';
                            $result['Result']['ErrorMessage'] = "Error in file uploading";
                            $success = "n";
                            echo json_encode($result);
                            return;
                        }
                    } else {
                        $result['Result']['ErrorCode'] = '2';
                        $result['Result']['ErrorMessage'] = "File size exceeds";
                        echo json_encode($result);
                        return;
                    }
                } else {
                    $success = "n";
                    $result['Result']['ErrorCode'] = '2';
                    $result['Result']['ErrorMessage'] = "Invalid file type";
                    echo json_encode($result);
                    return;
                }
            }


            /* =========== Ends Here =========== */
            //print_r($photo_arr);

            if ($userID) {
                if (!empty($photo_arr["car_photo"])) {
                    $dir = realpath(DIR_CAR_PHOTOS);
                    $org_file = $dir . "/tmp/" . $photo_arr["car_photo"]["file_name"];
                    $file = $dir . "/" . "V_" . $userID . "." . $photo_arr["car_photo"]['file_ext'];
                    rename($org_file, $file);

                    $data_update = array('carPhoto' => "V_" . $userID . "." . $photo_arr["car_photo"]['file_ext']);
                    $this->Usermodel->updatePersonalInfo($userID, $data_update);
                }
                $result['Result']['ErrorCode'] = '0';
                $result['Result']['ErrorMessage'] = "Success";
                $result['Result']['imageurl'] = base_url() . "user_car_images/";
            } else {
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = "Failure";
            }

            echo json_encode($result);
        } else {
            $this->load->view('webservices/carUpload');
        }
    }

    //carRegistration
    function carRegister() {
        if ($this->input->post('flag') == 'as') {
            $data = $result = array();

            $userID = $this->input->post('user_id');

            $data = array('carLicense' => $this->input->post('license'),
                'carModel' => $this->input->post('model'),
                'carColor' => $this->input->post('color'),
                'carMake' => $this->input->post('make')
                    //'carDescription'	=> $this->input->post('description')
            );

            //Code for uploading file to temporary directory
            /* =========== Begins Here =========== */
            if (@$_FILES['car_photo']["name"] != "") {
                $ext = substr($_FILES['car_photo']["name"], strrpos($_FILES['car_photo']["name"], ".") + 1);
                $ext = strtolower($ext);

                $types_arr = explode("|", lang('allowed_types'));
                $photo_arr = array();

                if (in_array($ext, $types_arr)) {
                    if ($_FILES['car_photo']['size'] <= MAX_FILE_SIZE) {
                        $uploaddir = realpath(DIR_CAR_PHOTOS) . '/tmp/';
                        $uploadfile = $userID . "_" . date("Ymd_His") . "." . $ext;

                        if (move_uploaded_file($_FILES['car_photo']['tmp_name'], $uploaddir . $uploadfile)) {

                            $photo_arr["car_photo"]["file_name"] = $uploadfile;
                            $photo_arr["car_photo"]['file_ext'] = $ext;
                            $photo_arr["car_photo"]["name"] = $uploadfile;
                            $photo_arr["car_photo"]["file_url"]=base_url()."/".$uploaddir."".$uploadfile;
                            $success = 'y';
                        } else {
                            $result['Result']['ErrorCode'] = '2';
                            $result['Result']['ErrorMessage'] = "Error in file uploading";
                            $success = "n";
                            echo json_encode($result);
                            return;
                        }
                    } else {
                        $result['Result']['ErrorCode'] = '2';
                        $result['Result']['ErrorMessage'] = "File size exceeds";
                        echo json_encode($result);
                        return;
                    }
                } else {
                    $success = "n";
                    $result['Result']['ErrorCode'] = '2';
                    $result['Result']['ErrorMessage'] = "Invalid file type";
                    echo json_encode($result);
                    return;
                }
            }


            /* =========== Ends Here =========== */
            //print_r($photo_arr);

            if ($userID && !empty($data)) {

                $this->Usermodel->updatePersonalInfo($userID, $data);
                if (!empty($photo_arr["car_photo"])) {
                    $dir = realpath(DIR_CAR_PHOTOS);
                    $org_file = $dir . "/tmp/" . $photo_arr["car_photo"]["file_name"];
                    $file = $dir . "/" . $userID . "_1." . $photo_arr["car_photo"]['file_ext'];
                    rename($org_file, $file);

                    $data_update = array('carPhoto' => $userID . "_1." . $photo_arr["car_photo"]['file_ext']);
                    $this->Usermodel->updatePersonalInfo($userID, $data_update);
                }
                $result['Result']['ErrorCode'] = '0';
                $result['Result']['ErrorMessage'] = "Success";
            } else {
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = "Failure";
            }

            echo json_encode($result);
        } else {
            $this->load->view('webservices/carRegister');
        }
        
        
    }

}

?>