<?php

class Fetch extends CI_Controller {

    function __Construct() {
        parent::__Construct();

        $this->load->model('Usermodel', '', TRUE);
        $this->load->model('Groupmodel', '', TRUE);
        $this->load->model('Commfuncmodel', '', TRUE);
        $this->load->model('Transactionmodel', '', TRUE);
        $this->load->model('Alertmodel', '', TRUE);
        $this->load->model('Notificationmodel', '', TRUE);
        $this->load->model('Fetchmodel', '', TRUE);
        $this->lang->load('message', 'english');
    }

    public function login() {
        $this->load->view('login.php');
    }

    public function registration() {
        $this->load->view('registration.php');
    }

    public function socialregistration() {
        $this->load->view('socialregistration.php');
    }

    public function activation() {
        $this->load->view('activation.php');
    }

    public function forgot() {

        $this->load->view('forgot.php');
    }

    public function resend() {

        $this->load->view('resend.php');
    }

    public function upload() {

        $this->load->view('upload.php');
    }

    public function contact() {

        $this->load->view('contact.php');
    }

    public function createfetchrequest() {

        $this->load->view('createfetchrequest.php');
    }

    public function blockcontact() {

        $this->load->view('blockcontact');
    }

    public function favouritecontact() {

        $this->load->view('favouritecontact');
    }

    public function editprofile() {

        $this->load->view('editprofile.php');
    }

    public function ETArefresh() {

        $this->load->view('ETArefresh.php');
    }

    public function fetchhistory() {

        $this->load->view('fetchhistory.php');
    }

    public function retrieveeditprofile() {

        $this->load->view('retrieveeditprofile.php');
    }

    public function schedular() {

        $this->load->view('schedular.php');
    }

    public function initiatefetchrequest() {

        $this->load->view('initiatefetchrequest.php');
    }

    public function testnotification() {

        $this->load->view('testnotification.php');
    }

    public function callogin() {

        $this->load->model('Fetchmodel');

        $em = $_POST['em'];

        $password = $_POST['password'];

        $devicereg = $_POST['devicereg'];

        $devicetype = $_POST['devicetype'];

        $flag = $_POST['flag'];

        $res = $this->Fetchmodel->login($em, $password, $devicereg, $devicetype, $flag);

        print_r($res);
    }

    public function callregister() {

        $this->load->model('Fetchmodel');

        $phone = $_POST['phone'];

        $name = $_POST['name'];

        $email = $_POST['email'];

        $password = $_POST['password'];

        $countrycode = $_POST['countrycode'];

        $city = $_POST['city'];

        $gender = $_POST['gender'];

        $dob = $_POST['dob'];

        $fbid = $_POST['fbid'];

        $accesstoken = $_POST['accesstoken'];

        $res = $this->Fetchmodel->registration($phone, $countrycode, $name, $email, $password, $city, $gender, $dob, $fbid, $accesstoken);

        print_r($res);
    }

    public function callsocialregister() {

        $this->load->model('Fetchmodel');

        $phone = $_POST['phone'];

        $name = $_POST['name'];

        $email = $_POST['email'];

        $countrycode = $_POST['countrycode'];

        $city = $_POST['city'];

        $id = $_POST['id'];

        $flag = $_POST['flag'];

        $res = $this->Fetchmodel->socialregistration($phone, $countrycode, $name, $email, $city, $id, $flag);

        print_r($res);
    }

    function callupload() {

        $userid = $_POST['userid'];

        $this->load->model('Fetchmodel');

        $res = $this->Fetchmodel->upload($userid);

        print_r($res);
    }

    public function callactivation() {

        $phone = $_POST['phone'];

        $code = $_POST['code'];

        $devicereg = $_POST['devicereg'];

        $devicetype = $_POST['devicetype'];

        $this->load->model('Fetchmodel');

        $res = $this->Fetchmodel->activation($code, $phone, $devicereg, $devicetype);

        print_r($res);
    }

    public function callforgot() {

        $em = $_POST['em'];

        $flag = $_POST['flag'];

        $this->load->model('Fetchmodel');

        $res = $this->Fetchmodel->forgot($em, $flag);

        print_r($res);
    }

    public function callresend() {

        $phone = $_POST['phone'];

        $this->load->model('Fetchmodel');

        $res = $this->Fetchmodel->resendcode($phone);

        print_r($res);
    }

    public function callcontact() {

        $fromfetchid = $_POST['from_fetch_id'];

        $phonearray = $_POST['phone_array'];

        $this->load->model('Fetchmodel');

        $res = $this->Fetchmodel->contact($fromfetchid, $phonearray);

        print_r($res);
    }

    public function callcreatefetchrequest() {

        $fromfetchid = $_POST['from_fetch_id'];

        $tofetchid = $_POST['to_fetch_id'];

        $datetime = $_POST['datetime'];

        $fromlat = $_POST['fromlat'];

        $fromlong = $_POST['fromlong'];

        $fromaddress = $_POST['fromaddress'];

        $this->load->model('Fetchmodel');

        $res = $this->Fetchmodel->createfetchrequest($fromfetchid, $tofetchid, $datetime, $fromlat, $fromlong, $fromaddress);

        print_r($res);
    }

    public function callblockcontact() {

        $fromfetchid = $_POST['from_fetch_id'];

        $tofetchid = $_POST['to_fetch_id'];

        $this->load->model('Fetchmodel');

        $res = $this->Fetchmodel->blockcontact($fromfetchid, $tofetchid);

        print_r($res);
    }

    public function callfavouritecontact() {

        $fromfetchid = $_POST['from_fetch_id'];

        $tofetchid = $_POST['to_fetch_id'];

        $this->load->model('Fetchmodel');

        $res = $this->Fetchmodel->favouritecontact($fromfetchid, $tofetchid);

        print_r($res);
    }

    public function callretrieveeditprofile() {

        $userid = $_POST['userid'];

        $this->load->model('Fetchmodel');

        $res = $this->Fetchmodel->retrieveeditprofile($userid);
        log_message('ERROR',print_r($res,true));
        print_r($res);
    }

    public function calleditprofile() {

        $userid = $_POST['userid'];

        $name = $_POST['name'];
        
        $last_name=$_POST['last_name'];

        $status = $_POST['status'];

        $phone = $_POST['phone'];

        $email = $_POST['email'];

        $dob = $_POST['dob'];

        $gender = $_POST['gender'];

        $city = $_POST['city'];
        
        
        





        $this->load->model('Fetchmodel');

        $res = $this->Fetchmodel->editprofile($userid, $name,$last_name, $status, $phone, $email, $dob, $gender, $city);
        
        

        print_r($res);
    }

    public function callETArefresh() {

        $fromfetchid = $_POST['from_fetch_id'];

        $tofetchid = $_POST['to_fetch_id'];

        $time = $_POST['time'];

        $this->load->model('Fetchmodel');

        $res = $this->Fetchmodel->ETArefresh($fromfetchid, $tofetchid, $time);

        print_r($res);
    }

    public function callfetchhistory() {

        $userid = $_POST['userid'];

        $page_no = $_POST['page_no'];

        $this->load->model('Fetchmodel');

        $res = $this->Fetchmodel->fetchhistory($userid, $page_no);

        print_r($res);
    }

    public function callschedular() {

        $userid = $_POST['userid'];
        $tz = $_POST['tz'];

        $this->load->model('Fetchmodel');

        $res = $this->Fetchmodel->schedular($userid, $tz);

        print_r($res);
    }

    public function callinitiatefetchrequest() {

        $fetch_id = $_POST['fetch_id'];

        $this->load->model('Fetchmodel');

        $res = $this->Fetchmodel->initiatefetchrequest($fetch_id);

        print_r($res);
    }

    public function calltestnotification() {

        $regid = $_POST['regid'];

        $registrationids = array($regid);

        $msg = $_POST['msg'];

        $devicetype = $_POST['devicetype'];

        if ($devicetype == 0) {

            $message = "You have a new fetch request from " . $msg . "";

            $message = array("price" => $message);



            $this->load->model('Fetchmodel');

            $res = $this->Fetchmodel->send_notification_android($registrationids, $message);

            print_r($res);
        } else if ($devicetype == 1) {

            $message = "You have a new fetch request from " . $msg . "";

            $message = array("price" => $message);



            $this->load->model('Fetchmodel');

            $res = $this->Fetchmodel->send_ios_notification($registrationids, $message);

            print_r($res);
        }
    }

    public function contactNew() {
        $this->load->view('contactNew.php');
    }

    public function callcontactNew() {

        $fromfetchid = $_POST['from_fetch_id'];

        $phonearray = $_POST['phone_array'];

        $this->load->model('Fetchmodel');

        $res = $this->Fetchmodel->contactNew($fromfetchid, $phonearray);

        print_r($res);
    }

    public function forgotPassword() {
        if ($this->input->post('viewflag') == 'as') {
            $em = $this->input->post('em');
            $flag = $this->input->post('flag');
            $this->load->model('Fetchmodel');
            $res = $this->Fetchmodel->forgotPassword($em, $flag);
            print_r($res);
        } else {
            $this->load->view('forgotPassword.php');
        }
    }

    public function register() {
        if ($this->input->post('viewflag') == 'as') {

            $res = array();
            $result = array();
            $phone = $this->input->post('phone');
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $country = $this->input->post('country');
            $countrycode = $this->input->post('country_code');

            $insert_data = array('phone_no' => $phone,
                'email_id' => $email,
                'password' => base64_encode($password),
                'country_code' => $countrycode,
                'created_date' => date('Y-m-d H:i:s'),
            );
            $this->load->model('Fetchmodel');
            $res = $this->Fetchmodel->registerUser($insert_data);

            if ($res == 0) {
                $result['Result']['ErrorCode'] = '4';
                $result['Result']['ErrorMessage'] = "Duplicate records";
                $result['Result']['Flag'] = "b";
            } elseif ($res == 1) {
                $result['Result']['ErrorCode'] = '4';
                $result['Result']['ErrorMessage'] = "Duplicate records";
                $result['Result']['Flag'] = "m";
            } elseif ($res == 1) {
                $result['Result']['ErrorCode'] = '4';
                $result['Result']['ErrorMessage'] = "Duplicate records";
                $result['Result']['Flag'] = "e";
            } else {
                $last_inserted_id = $res;
                $personal_insert = array('fk_user_app_info_id' => $last_inserted_id,
                    'name' => $first_name,
                    'last_name' => $last_name,
                    'country' => $country
                );
                $this->load->model('Usermodel');
                $flg = $this->Usermodel->addUserPersonalInfo($personal_insert);
                $result['Result']['ErrorCode'] = '0';
                $result['Result']['ErrorMessage'] = "Success";
                $result['Result']['UserId'] = $last_inserted_id;

                if ($flg == 1) {

                    //upload the user image
                    if (@$_FILES['photo']["name"] != "") {
                        $ext = substr($_FILES['photo']["name"], strrpos($_FILES['photo']["name"], ".") + 1);
                        $ext = strtolower($ext);

                        $types_arr = explode("|", lang('allowed_types'));

                        if (in_array($ext, $types_arr)) {
                            if ($_FILES['photo']['size'] <= MAX_FILE_SIZE) {
                                $uploaddir = realpath(DIR_USER_IMAGES) . '/';
                                $uploadfile = $last_inserted_id . '.' . $ext;

                                if (move_uploaded_file($_FILES['photo']['tmp_name'], $uploaddir . $uploadfile)) {
                                    $photo_arr["photo"]["file_name"] = $uploadfile;
                                    $photo_arr["photo"]['file_ext'] = $ext;
                                    $photo_arr["photo"]["name"] = $uploadfile;
                                    $success = 'y';

                                    $result['Result']['ErrorCode'] = '0';
                                    $result['Result']['ErrorMessage'] = "Success";
                                    $result['Result']['UserId'] = $last_inserted_id;
                                    $result['Result']['imageurl'] = base_url() . "images/";
                                    //echo json_encode($result);
                                } else {
                                    $result['Result']['ErrorCode'] = '2';
                                    $result['Result']['ErrorMessage'] = "Error in file uploading";
                                    $success = "n";
                                    //echo json_encode($result);
                                }
                            } else {
                                $result['Result']['ErrorCode'] = '2';
                                $result['Result']['ErrorMessage'] = "File size exceeds";
                                //echo json_encode($result);
                            }
                        } else {
                            $success = "n";
                            $result['Result']['ErrorCode'] = '2';
                            $result['Result']['ErrorMessage'] = "Invalid file type";
                            //echo json_encode($result);
                        }
                    }

                    //generate the activation code and update the status in the db
                    $random = rand(1111, 9999);

                    //update code in db
                    $data = array('phone_activation_code' => $random);

                    $this->db->where('pk_user_app_info_id', $last_inserted_id);
                    //$this->db->where('phone_registration_id', $phonereg);

                    $flag = $this->db->update(TABLE_USER_MASTER, $data);

                    //sending the activation code via sms
                    $phonenew = $countrycode . $phone;

                    $from = '918484069295';
                    $to = $phonenew;
                    //$message = array( 'text' => 'Hi '.$name.'! Your verification code is '.$random.'.');
                    $message = array('text' => 'Welcome to Fetch-Apps! Here is your verification code: ' . $random . '');

                    $response = $this->nexmo->send_message($from, $to, $message);

                    $result['Result']['activation_code'] = $data['phone_activation_code'];
                }
            }
            echo json_encode($result);
        } else {
            $this->load->view('register.php');
        }
    }

    //registration thru facebook
    public function register_fb() {
        if ($this->input->post('viewflag') == 'as') {
            $res = array();
            $result = array();
            $phone = $this->input->post('phone');
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $country = $this->input->post('country');
            $fb_id = $this->input->post('fbid');
            $accesstoken = $this->input->post('accesstoken');
            $countrycode = $this->input->post('country_code');

            $insert_data = array('phone_no' => $phone,
                'email_id' => $email,
                'password' => base64_encode($password),
                'country_code' => $countrycode,
                'social_id' => $fb_id,
                'created_date' => date('Y-m-d H:i:s'),
            );
            $this->load->model('Fetchmodel');
            $res = $this->Fetchmodel->registerUser($insert_data);



            if ($res == 0) {
                $result['Result']['ErrorCode'] = '4';
                $result['Result']['ErrorMessage'] = "Duplicate records";
                $result['Result']['Flag'] = "b";
            } elseif ($res == 1) {
                $result['Result']['ErrorCode'] = '4';
                $result['Result']['ErrorMessage'] = "Duplicate records";
                $result['Result']['Flag'] = "m";
            } elseif ($res == 1) {
                $result['Result']['ErrorCode'] = '4';
                $result['Result']['ErrorMessage'] = "Duplicate records";
                $result['Result']['Flag'] = "e";
            } else {
                $last_inserted_id = $res;

                if ($fb_id != "") {
                    $headers = get_headers('https://graph.facebook.com/' . $fb_id . '/picture', 1);
                    // just a precaution, check whether the header isset...
                    if (isset($headers['Location'])) {
                        $url = $headers['Location']; // string
                    } else {
                        $url = false; // nothing there? .. weird, but okay!
                    }

                    //home1/capstoq5/public_html/fetch_new/app/images/
                    $destination = '/home1/capstoq5/public_html/fetch_new/app/images/' . $last_inserted_id . '.jpg';
                    $content = file_get_contents($url);
                    $fp = fopen($destination, "w");
                    fwrite($fp, $content);
                    fclose($fp);
                }

                $personal_insert = array(
                    'fk_user_app_info_id' => $last_inserted_id,
                    'name' => $first_name,
                    'last_name' => $last_name,
                    'country' => $country,
                    'accesstoken' => $accesstoken
                );
                $this->load->model('Usermodel');
                $flg = $this->Usermodel->addUserPersonalInfo($personal_insert);
                $result['Result']['ErrorCode'] = '0';
                $result['Result']['ErrorMessage'] = "Success";
                $result['Result']['UserId'] = $last_inserted_id;


                //generate the activation code and update the status in the db
                $random = rand(1111, 9999);

                //update code in db
                $data = array('phone_activation_code' => $random);

                $this->db->where('pk_user_app_info_id', $last_inserted_id);
                //$this->db->where('phone_registration_id', $phonereg);

                $flag = $this->db->update(TABLE_USER_MASTER, $data);

                //sending the activation code via sms
                $phonenew = $countrycode . $phone;

                $from = '918484069295';
                $to = $phonenew;
                //$message = array( 'text' => 'Hi '.$name.'! Your verification code is '.$random.'.');
                $message = array('text' => 'Welcome to Fetch-Apps! Here is your verification code: ' . $random . '');

                $response = $this->nexmo->send_message($from, $to, $message);
            }
            echo json_encode($result);
        } else {
            $this->load->view('register_fb.php');
        }
    }

    //change password
    public function changePassword() {
        $this->load->model('Usermodel');
        if ($this->input->post('viewflag') == 'as') {
            $em = $this->input->post('em');
            $flag = $this->input->post('flag');
            $current_password = $this->input->post('current_password');
            $new_password = $this->input->post('new_password');
            $select = array();
            $where_str = array();
            $pass = base64_encode($current_password);
            //checking whether email/phone number registered or not and current password is associated or not
            if ($flag == "e") {
                $where_str = ' email_id= "' . $em . '" AND password= "' . $pass . '"';
            } else if ($flag == "m") {
                $where_str = 'phone_no= "' . $em . '" AND password= "' . $pass . '"';
            }

            $res = $this->Usermodel->getUserInfo($em, $select, $where_str);
            if (count($res) > 0) {
                $user_id = $res['pk_user_app_info_id'];
                $new_pass = base64_encode($new_password);
                $data = array('password' => $new_pass);
                $this->Usermodel->updateUserAppInfo($user_id, $data);
                $result['Result']['ErrorCode'] = '0';
                $result['Result']['ErrorMessage'] = 'Success';
            } else {
                $result['Result']['ErrorCode'] = '-2';
                $result['Result']['ErrorMessage'] = 'No Records Found';
            }
            echo json_encode($result);
        } else {
            $this->load->view('changePassword');
        }
    }

    //sendMessage
    function sendMessage() {
        if ($this->input->post('viewflag') == 'as') {
            $fetch_id = $this->input->post('fetch_id');
            $from_user_id = $this->input->post('from_user_id');
            $to_user_id = $this->input->post('to_user_id');
            $preset_message = $this->input->post('preset_message');
            $current_datetime = date("Y-m-d H:i:s");
            $data_insert = array('fc_transaction_id' => $fetch_id,
                'fc_from_user_id' => $from_user_id,
                'fc_to_user_id' => $to_user_id,
                'fc_preset_message' => $preset_message,
                'fc_created_date' => $current_datetime
            );
            $res = $this->Transactionmodel->addTransactionMessage($data_insert);

            if ($res) {
                $data_insert['alert_type']=10;
                $push_result['Result']=$data_insert;
                $msg = json_encode($push_result);
                $this->Notificationmodel->callnotification($from_user_id, $to_user_id, $msg);

                $result['Result']['ErrorCode'] = '0';
                $result['Result']['ErrorMessage'] = 'Success';
            } else {
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = 'Failure';
            }
            echo json_encode($result);
        } else {
            $this->load->view('sendMessage');
        }
    }

    function chatHistory() {
        if ($this->input->post('viewflag') == 'as') {
            $select = array();
            $where_str = array();
            $fetch_id = $this->input->post('fetch_id');
            $res = $this->Transactionmodel->getChatHistory($fetch_id, $select, $where_str);
            if (count($res) > 0) {
                $result['Result']['ErrorCode'] = '0';
                $result['Result']['ErrorMessage'] = 'Success';
                $result['Result']['ChatHistory'] = $res;
            } else {
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = 'Failure';
            }

            echo json_encode($result);
        } else {
            $this->load->view('chatHistory');
        }
    }

    function helpCentre() {
        if ($this->input->post('viewflag') == 'as') {
            $email = 'ashwini.mca.89@gmail.com';
            $feedback = $this->input->post('feedback');
            // subject
            $subject = 'Fetch Help Centre';
            // message
            $message = "'" . $feedback . "'";
            // To send HTML mail, the Content-type header must be set
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            // Additional headers
            $headers .= '';
            // Mail it	
            if (mail($email, $subject, $message, $headers)) {
                $result['Result']['ErrorCode'] = '0';
                $result['Result']['ErrorMessage'] = "Success";
            } else {
                $result['Result']['ErrorCode'] = '1';
                $result['Result']['ErrorMessage'] = "Failure";
            }

            echo json_encode($result);
        } else {
            $this->load->view('helpCentre');
        }
    }

    public function send_err(){
        $this->load->view('viewSendErr');
    }
    public function err_log() {
        $user_id=  $this->input->post('user_id');
        $device_id=  $this->input->post('device_id');
        $err_msg=  $this->input->post('err_msg');
        $timezone=  $this->input->post('timezone');
        if($this->Fetchmodel->log_err($user_id, $device_id, $err_msg, $timezone)){
           echo "Message sent";
        }else{
             echo 'Message sending failed';
        }
    }

}

?>