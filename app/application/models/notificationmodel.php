<?php
class Notificationmodel extends CI_Model
{
	function __Construct()
	{
		parent::__Construct();
	}
	
	function callnotification($user_id,$contact_id,$message)
	{
		//retrieve registration ids
		$select = 'phone_registration_id,device_type';
		$userDeviceInfo = $this->Usermodel->getUserDeviceInfo($contact_id, $select);
		//retrieve name
		$select = 'name';
		$fetcherName = $this->Usermodel->getPersonalInfo($user_id, $select);
		if(count($fetcherName)>0)
		{
			$name= $fetcherName['name'];
		}
		else
		{
			$name="";
		}
		//echo $name;
		
		//$message = array("msg" => $message);

		if(!empty($userDeviceInfo))
		{
			foreach ($userDeviceInfo as $ud)
			{
				//$message="You have a new fetch request from ".$name."";
				if($ud['device_type']==0)
				{
					$message = array("msg" => $message);
					$registrationid = $ud['phone_registration_id'];
					$registration_ids = array($registrationid);
					//print_r($registration_ids);die('<hr>');
					$this->send_notification_android($registration_ids, $message);
				}
				else if ($ud['device_type']==1)
				{
				
					$registrationid = $ud['phone_registration_id'];
					$registration_ids = $registrationid;
					$this->send_ios_notification($registration_ids, $message);
					//echo "entered";
					//$this->snd();
				}
			}
		}
	}
	
	function send_notification_android($registration_ids, $message) 
	{
		//echo $registration_ids[0].",". $message."<br><pre>"; print_r($message); die('<hr>');

		// Set POST variables
		$url = 'https://android.googleapis.com/gcm/send';
		
		$fields = array(
			'registration_ids' => $registration_ids,
			'data' => $message,
		);
		
		/*
		$headers = array(
			'Authorization: key = AIzaSyA1h59nxzuleriJyXBHvRKbcGyhmorcboA',
			'Content-Type: application/json'
		);
		*/

		/*
		$headers = array(
			'Authorization: key = AIzaSyDhiRx4LVtiW3cbr2Bbjfxb3LtuY8qYCPY',
			'Content-Type: application/json'
		);
		*/
		$headers = array(
			'Authorization: key = AIzaSyBLZDrqHfjeLBMv5CBuU2O8RvP8D9sTLIk',
			'Content-Type: application/json'
		);

		// Open connection
		$ch = curl_init();
		
		// Set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $url);
		
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		// Disabling SSL Certificate support temporarly
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		//echo $ch;
		// Execute post
		$result = curl_exec($ch);
		if ($result === FALSE) {
			die('Curl failed: ' . curl_error($ch));
		}
		
		// Close connection
		curl_close($ch);
		//echo '<hr>'.$result.'<hr>';
	}
	
	//send notification to ios
	function send_ios_notification_old($registrationIDs, $message =  "") 
	{
		// Put your device token here (without spaces):
		$deviceToken = $registrationIDs;
                //$deviceToken = 'f571bd68f25af6f952282f3fdaf3bf82abfaff5381953d605acd333f34bc37b4';
		
		// Put your private key's passphrase here:
		$passphrase = 'arvind979ravi';
		
		// Put your alert message here:
		$message = 'My first push notification!';
		
		////////////////////////////////////////////////////////////////////////////////
		//print_r($deviceToken); echo "<br>"; 
        //echo base_url().'FetchCK.pem : '.(file_exists(base_url().'FetchCK.pem'))?1:0; die('<hr>');

		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', base_url().'FetchCK.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

		// Open a connection to the APNS server
		$fp = stream_socket_client(
			'ssl://gateway.sandbox.push.apple.com:2195', $err,
			$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		
		if (!$fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);
		
		echo 'Connected to APNS' . PHP_EOL;
		
		// Create the payload body
		$body['aps'] = array( 'alert' => $message,
			              'sound' => 'default'
			            );
		
		// Encode the payload as JSON
		$payload = json_encode($body);
		
		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
		
		// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));
		
		if (!$result)
			echo 'Message not delivered' . PHP_EOL;
		else
			echo 'Message successfully delivered' . PHP_EOL;
		
		// Close the connection to the server
		fclose($fp);
		echo "end";
            
	}
	
	
	//send notification to ios
	function send_ios_notification($registrationIDs, $msg1) 
	{
		//Keep only necessary parameters from message string
		$this->load->model('Commfuncmodel','',TRUE);
		$result = json_decode($msg1);
		$data1 = $this->Commfuncmodel->objectToArray($result);
		$data2	= $this->Commfuncmodel->objectToArray($data1['Result']);
		$fetch_data = $this->Commfuncmodel->objectToArray($data2['fetch_data']);

		//$result1 
		$notification_data = array(	'alert'	=>	$fetch_data['alert_type'],
									'phone'	=>	$data2['phone_no'],
									'fid'	=>	$fetch_data['fetch_id'],
									'fflg'	=>	$fetch_data['fetch_flag'],
									'tolat'	=>	$fetch_data['fetch_to_lat'],
									'tolon'	=>	$fetch_data['fetch_to_lat'],
								);
		
		//$notification_data = json_encode($result1);


		// Put your device token here (without spaces):
		$deviceToken = $registrationIDs;

		// Put your private key's passphrase here:
		$passphrase = 'arvind979ravi';
		
		// Put your alert message here:
		$message = ucfirst(str_replace("##user_name##", $data2['name'], $data2['message']));//$msg;
		
		////////////////////////////////////////////////////////////////////////////////
		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', 'FetchCK.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

		// Open a connection to the APNS server
		$fp = stream_socket_client( 'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 120, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

		if (!$fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);

		//echo 'Connected to APNS' . PHP_EOL;

		// Create the payload body
		/*$body['aps'] = array(	'alert' => $message,
					'sound' => 'default'
				);
		*/
		
		$body['aps'] = array(	'alert' => $message,
					'sound' => 'default'
				);
		
		$body['data'] =	$notification_data;

		// Encode the payload as JSON
		$payload = json_encode($body);
		
		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
		
		// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));

		/*
		if (!$result)
			echo 'Message not delivered' . PHP_EOL;
		else
			echo 'Message successfully delivered' . PHP_EOL;
		*/
		// Close the connection to the server
		fclose($fp);
	}
	
	
	function snd()
	{
		echo "entered";
		// Put your device token here (without spaces):
		$deviceToken = 'f571bd68f25af6f952282f3fdaf3bf82abfaff5381953d605acd333f34bc37b4';
		//$deviceToken = 'd1c86d3ba846695ff4e2d94c15f5577279a363dddaf46cf005822e0e1aadd6e6";
		
		// Put your private key's passphrase here:
		$passphrase = 'arvind979ravi';
		
		// Put your alert message here:
		$message = 'Hi Makrand, My first push notification!';
		
		////////////////////////////////////////////////////////////////////////////////
		
		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', 'FetchCK.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		
		// Open a connection to the APNS server
		$fp = stream_socket_client(
			'ssl://gateway.sandbox.push.apple.com:2195', $err,
			$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		
		if (!$fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);
		
		echo 'Connected to APNS' . PHP_EOL;
		
		// Create the payload body
		$body['aps'] = array(
			'alert' => $message,
			'sound' => 'default'
			);
		
		// Encode the payload as JSON
		$payload = json_encode($body);
		
		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
		
		// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));
		
		if (!$result)
			echo 'Message not delivered' . PHP_EOL;
		else
			echo 'Message successfully delivered' . PHP_EOL;
		
		// Close the connection to the server
		fclose($fp);

	}
}