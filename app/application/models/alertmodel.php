<?php
class Alertmodel extends CI_Model
{
	function __Construct()
	{
		parent::__Construct();
	}
	
	function addUserAlert($data_insert)
	{
		$this->db->insert(TABLE_USER_ALERTS, $data_insert);
		return TRUE;
	}
	
	function updateUserAlert($fetch_id, $user_id, $data_update)
	{
		$this->db->where('alert_transaction_id', $fetch_id);
		$this->db->where('alert_user_id', $user_id);
		$this->db->update(TABLE_USER_ALERTS, $data_update);
		return TRUE;
	}

	function deleteUserAlert($fetch_id, $user_id)
	{
		$this->db->where('alert_transaction_id', $alert_id);
		$this->db->delete(TABLE_USER_ALERTS);
		return TRUE;
	}

	function getUserAlerts($user_id, $page_no=0)
	{
		$res = array();

		$this->db->where('alert_to_user_id', $user_id);
		$this->db->order_by('alert_creation_time','DESC');
		$query = $this->db->get(TABLE_USER_ALERTS, ($page_no+1)*10, ($page_no)*10);

		if($query->num_rows()>0)
		{
			$res = $query->result_array();
		}
		return $res;
	}
	
	function getAlertTemplate($template_id,$select=NULL)
	{
		if($select)
			$this->db->select($select);
		
		$this->db->where('template_id', $template_id);
		$query = $this->db->get(TABLE_ALERT_TEMPLATES);
		$res = $query->result_array();
		return $res[0];
	}
}
?>