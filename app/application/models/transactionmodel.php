<?php
class Transactionmodel extends CI_Model
{
	function __Construct()
	{
		parent::__Construct();
	}

	function add($data_insert)
	{
		$this->db->insert(TABLE_FETCH_TRANSACTION, $data_insert);
		return TRUE;
	}

	function update($fetch_id, $data_update, $where_arr=NULL)
	{
		if($where_arr)
		{
			$this->db->where($where_arr);
		}

		$this->db->where('fetch_id', $fetch_id);
		$this->db->update(TABLE_FETCH_TRANSACTION, $data_update);
		return TRUE;
	}

	function getTransactionDetails($fetch_id, $select=NULL, $where_str=NULL)
	{
		if($select)
			$this->db->select($select);

		$this->db->where('fetch_id', $fetch_id);
		
		if(@$where_str)
			$this->db->where($where_str);

		$query = $this->db->get(TABLE_FETCH_TRANSACTION);
		$res = $query->result_array();

		if(!empty($res))
		{
			return $res[0];	
		}

	}



	function addTransactionUsers($data_insert)

	{

		$this->db->insert(TABLE_FETCH_TRANSACTION_USERS, $data_insert);

		return TRUE;

	}

	

	function updateTransactionUsers($fetch_id, $user_id, $data_update)

	{

		$this->db->where('ftu_transaction_id', $fetch_id);

		$this->db->where('ftu_user_id', $user_id);

		$this->db->update(TABLE_FETCH_TRANSACTION_USERS, $data_update);

		return TRUE;

	}



	function deleteTrasactionUsers($fetch_id)

	{

		$this->db->where('ftu_transaction_id', $fetch_id);

		$this->db->delete(TABLE_FETCH_TRANSACTION_USERS);

		return TRUE;

	}

	function getTransactionUsers($fetch_id, $user_id=NULL, $select=NULL, $where_str=NULL)
	{
		if($select)
			$this->db->select($select);

		if($where_str)
			$this->db->where('('.$where_str.')', NULL, false);

		$this->db->where('ftu_transaction_id', $fetch_id);

		if($user_id)
			$this->db->where('ftu_user_id!=', $user_id,false);

		$query = $this->db->get(TABLE_FETCH_TRANSACTION_USERS);
		$res = $query->result_array();
		return $res;
	}

	function getTransactionJoinUsers($fetch_id, $user_id=NULL, $select=NULL, $where_str=NULL)
	{
		if($select)
			$this->db->select($select);

		if($where_str)
			$this->db->where('('.$where_str.')', NULL, false);

		$this->db->where('fetch_id', $fetch_id);

		if($user_id)
			$this->db->where('(fetch_creator_id='.$user_id.' OR ftu_user_id='.$user_id.')');

		$this->db->join('fetch_trasaction_users', 'fetch_transaction_new.fetch_id = fetch_trasaction_users.ftu_transaction_id');

		$query = $this->db->get(TABLE_FETCH_TRANSACTION);

		$res = $query->result_array();

		if($query->num_rows()>0)
		{
			$res = $query->result_array();
			return $res[0];
		}
		else
		{
			return 0;
		}
	}

	function getTransactionResponseCount($fetch_id, $status=NULL)
	{
		$this->db->where('ftu_transaction_id', $fetch_id);

		if($status)
			$this->db->where('ftu_response', $status);

		$query = $this->db->get(TABLE_FETCH_TRANSACTION_USERS);
		return $query->num_rows();
	}

	function getUserTransactionResponse($fetch_id, $user_id)
	{
		$this->db->select('ftu_response')->where('ftu_transaction_id', $fetch_id);
		$this->db->where('ftu_user_id', $user_id);
		$query = $this->db->get(TABLE_FETCH_TRANSACTION_USERS);

		if($query->num_rows()>0)
		{
			$res = $query->result_array();
			return $res[0]['ftu_response'];
		}
		else
		{
			return 0;
		}
	}

	//added by ashwini 14-01-2014
	function deleteTrasaction($user_id,$fetch_transaction_id)
	{
		//$status_values = array('0','1','2','3');
		$this->db->select('fetch_id')->where('fetch_id', $fetch_transaction_id);
		$this->db->where('fetch_creator_id', $user_id);
		$query = $this->db->get(TABLE_FETCH_TRANSACTION);

		if($query->num_rows()>0)
		{
			$this->db->where('fetch_id', $fetch_transaction_id);
			$this->db->where('fetch_status <>','4');
			$this->db->delete(TABLE_FETCH_TRANSACTION); 
			return TRUE;
		}
		else 
		{
			$this->db->where('ftu_transaction_id', $fetch_transaction_id);
			$this->db->where('ftu_user_id',$user_id);
			$this->db->delete(TABLE_FETCH_TRANSACTION_USERS); 
			return TRUE;
		}
			
	}


		
	function addTransactionMessage($data_insert)
	{
		$this->db->insert(FETCH_CHAT, $data_insert);
		return TRUE;
	}
	
	function getChatHistory($fetch_id,$select=NULL,$where_str=NULL)
	{
		if($select)
			$this->db->select($select);

		$this->db->where('fc_transaction_id', $fetch_id);
		$this->db->order_by('fc_created_date','asc');

		if(@$where_str)
			$this->db->where($where_str);

		$query = $this->db->get(FETCH_CHAT);
		$res = $query->result_array();

		if(!empty($res))
		{
			return $res;	
		}
	}

	function deleteChatHistory($fetch_id)
	{
		$this->db->where('fc_transaction_id', $fetch_id);

		$this->db->delete(FETCH_CHAT);

		return TRUE;
	}

	function deleteTrasactionSingleUser($fetch_id,$user_id)
	{
		$this->db->where('ftu_transaction_id', $fetch_id);
		$this->db->where('ftu_user_id', $user_id);
		$this->db->delete(TABLE_FETCH_TRANSACTION_USERS);
		return TRUE;
	}
}

?>