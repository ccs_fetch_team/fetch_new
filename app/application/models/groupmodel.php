<?php
class Groupmodel extends CI_Model
{
	function __Construct()
	{
		parent::__Construct();
	}
	
	function addGroup($data_insert)
	{
		$data_insert['group_created_on'] = date('Y-m-d H:i:s');
		$data_insert['group_updated_on'] = date('Y-m-d H:i:s');
		$this->db->insert(TABLE_GROUP_MASTER, $data_insert);
		return TRUE;
	}

	function updateGroup($group_id, $data_update)
	{
		$data_update['group_updated_on'] = date('Y-m-d H:i:s');
		$this->db->where('group_id', $group_id);
		$this->db->update(TABLE_GROUP_MASTER, $data_update);
		return TRUE;
	}

	function deleteGroup($group_id, $group_owner)
	{
		if($group_id)
			$this->db->where('group_id', $group_id);
		if($group_owner)
			$this->db->where('group_owner', $group_owner);			

		$this->db->delete(TABLE_GROUP_MASTER);
		return TRUE;
	}
	
	function getGroupsList($user_id)
	{
		$this->db->select('group_id, group_name')->where('group_owner', $user_id);
		$query = $this->db->get(TABLE_GROUP_MASTER);
		
		$res = $query->result_array();
		return $res;
	}

	function addGroupMembers($group_id, $members_list)
	{
		if($group_id && !empty($members_list))
		{
			foreach($members_list as $k=>$v)
			{
				$member_data = array(	'gm_group_id' => $group_id,
										'gm_user_id' => $v,
									);

				$this->db->insert(TABLE_GROUP_MEMBERS, $member_data);
			}
			return TRUE;
		}
		return FALSE;
	}

	function getGroupMembers($group_id)
	{
		$result = array();
		$res = array();
		if($group_id)
		{
			$this->db->select('gm_user_id');
			$query = $this->db->where('gm_group_id', $group_id)->get(TABLE_GROUP_MEMBERS);
			$res = $query->result_array();
			
			foreach($res as $k=>$v)
			{
				$result[] = $v['gm_user_id'];
			}
		}
		return $result;
	}
	
	function deleteGroupMembers($group_id)
	{
		if($group_id)
		{
			$this->db->where('gm_group_id', $group_id);
			$this->db->delete(TABLE_GROUP_MEMBERS);
			return TRUE;
		}
		return FALSE;
	}

	/*
	 * This method is used to check if group already exists.
	 */
	function groupAlreadyExists($groupData, $group_owner=NULL)
	{
		if( !empty($groupData) )
		{
			if($group_owner)
				$groupData['group_owner'] = $group_owner;

			$query = $this->db->get_where(TABLE_GROUP_MASTER, $groupData);

			if($query->num_rows()>0) //Email already exists
			{
				return FALSE; 
			}
			return TRUE;
		}
	}
}
?>