<?php
class Usermodel extends CI_Model
{
	function __Construct()
	{
		parent::__Construct();
	}

	function update($userID, $data_update, $where_arr=NULL)
	{
		if($userID)
		{
			if( !empty($where_arr) )
				$this->db->where($where_arr);

			$this->db->where('pk_user_app_info_id', $userID);
			$this->db->update(TABLE_USER_MASTER, $data_update);
			return TRUE;
		}
		return FALSE;
	}

	function updatePersonalInfo($user_id, $data)
	{
		$this->db->where('fk_user_app_info_id', $user_id);
		$this->db->update(TABLE_PERSONAL_INFO, $data);
		return TRUE;
	}

	function getPersonalInfo($user_id, $select=NULL)
	{
		if($user_id)
		{
			$result = array();

			if(@$select)
				$this->db->select($select);

			$query = $this->db->where('fk_user_app_info_id', $user_id)->get(TABLE_PERSONAL_INFO);

			if($query->num_rows()>0)
			{
				$res = $query->result_array();
				return $res[0];
			}
		}
		return NULL;
	}

	function changePassword($userID, $oldPassword, $newPassword)
	{
		$this->db->select('*');
		$where_arr = array('pk_user_app_info_id' =>$userID,'password' => $oldPassword );
		$query = $this->db->get_where(TABLE_USER_MASTER, $where_arr);

		if($query->num_rows()==1)
		{
			$data = array( 'password' => $newPassword,
						   'updated_date' => date('Y-m-d H:i:s'),
						);

			$this->update($userID, $data, $where_arr);
			return 1;
		}
		else
		{
			return 2;
		}
	}

	function updatePals($user_id, $update_data, $where_arr=NULL)
	{
		//$this->db->where('');
	}

	//function added by Ashwini
	function getUserDeviceInfo($user_id, $select=NULL)
	{
		if($user_id)
		{
			$result = array();

			if(@$select)
				$this->db->select($select);

			$query = $this->db->where('fk_user_app_info_id', $user_id)->get(USER_DEVICE);

			if($query->num_rows()>0)
			{
				$res = $query->result_array();
				return $res;
			}
		}
		return NULL;
	}

	function getUserAppInfo($user_id, $select=NULL)
	{
		$res = array();

		if($select)
			$this->db->select($select);

		$this->db->where('pk_user_app_info_id', $user_id);
		$query = $this->db->get(TABLE_USER_MASTER);

		if($query->num_rows > 0)
		{
			$res = $query->result_array();
			return $res[0];
		}
		return $res;
	}

	function getBlockedPals($user_id, $select=NULL)
	{
		if($user_id)
		{
			$result = array();

			if(@$select)
				$this->db->select($select);
			$query = $this->db->where('fk_user_app_info_id', $user_id)->get('user_blocked_pals');
			$newarray = array();
			if($query->num_rows()>0)
			{
				$res = $query->result_array();
				//return $res;
				//print_r($res); exit;
				$newarray = array();
				$i=0;
				foreach ($res as $idres)
				{
					$newarray[$i]['blocked_fetch_id'] = $idres['fk_user_app_info_pal_id'];
					
					//retrieve name
					$this->db->select('name');
					$query1 = $this->db->where('fk_user_app_info_id', $idres['fk_user_app_info_pal_id'])->get(PERSONAL_INFO);
					if($query1->num_rows()>0)
					{
						$result1 = $query1->result_array();
						$newarray[$i]['blocked_user_name'] = $result1[0]['name'];
					}
					//retrieve phone number 
					$this->db->select('phone_no');
					$query2 = $this->db->where('pk_user_app_info_id', $idres['fk_user_app_info_pal_id'])->get(TABLE_USER_MASTER);
					if($query2->num_rows()>0)
					{
						$result2 = $query2->result_array();
						$newarray[$i]['blocked_user_no'] = $result2[0]['phone_no'];
					}
					$i++;
				}
				return $newarray;				
			}
		}

		return NULL;

	}

	function Unblockpals($user_id,$change_block_user_id,$blockflag)
	{
		if($user_id)
		{ 
			if($blockflag=='b')
			{
				//echo $unblock_user_id; //exit;
				$ary=explode(',', $change_block_user_id);
				//print_r($ary); exit;
				$date = date("Y-m-d");
				foreach($ary as $blockid)
				{
					$data = array(
						  'fk_user_app_info_id' => $user_id,
				    	  'fk_user_app_info_pal_id' => $blockid,
						  'date' => $date
				            	);
					$this->db->insert(BLOCKED_PALS,$data);
				}
				
			}
			else if($blockflag=='u')
			{
				$unblockary=explode(',', $change_block_user_id);
				foreach ($unblockary as $unblockid)
				{
					$result = array();
					$this->db->delete('user_blocked_pals', array('fk_user_app_info_id' => $user_id,'fk_user_app_info_pal_id' => $unblockid)); 		
				}
			}
		}
		return TRUE;
	}

	function updateUserDevices($userID, $data_update, $where_arr=NULL)
	{
		if( !empty($where_arr) )
			$this->db->where($where_arr);

		$this->db->where('fk_user_app_info_id', $userID);
		$this->db->update(USER_DEVICE, $data_update);

		if($this->db->affected_rows()==1)
			return TRUE;
		else
			return FALSE;
	}

	function addUser($data_insert)
	{
		$this->db->insert(TABLE_USER_MASTER, $data_insert);
		return TRUE;
	}

	function addUserPersonalInfo($data_insert)
	{
		$this->db->insert(TABLE_PERSONAL_INFO, $data_insert);
		return TRUE;
	}

	function updateUserDevice($userID, $data_update, $where_arr=NULL)
	{
		if($userID)
		{
			if( !empty($where_arr) )
				$this->db->where($where_arr);

			$this->db->where_in('fk_user_app_info_id', $userID);
			$this->db->update(USER_DEVICE, $data_update);
			return TRUE;
		}
		return FALSE;
	}

	function getUserInfo($em,$select=NULL,$where_str=NULL)
	{
		$res = array();
		if($select)
		{
			$this->db->select($select);
		}
		if($where_str)
		{
			$this->db->where('('.$where_str.')', NULL, false);
		}
		$query = $this->db->get(TABLE_USER_MASTER);
		if($query->num_rows > 0)
		{
			$res = $query->result_array();
			return $res[0];
		}
		return $res;
	}
	
	function updateUserAppInfo($user_id, $data)
	{
		$this->db->where('pk_user_app_info_id', $user_id);
		$this->db->update(TABLE_USER_MASTER, $data);
		return TRUE;
	}
}
?>