<?php
class Transactionmodel extends CI_Model
{
	function __Construct()
	{
		parent::__Construct();
	}

	function add($data_insert)
	{
		$this->db->insert(TABLE_FETCH_TRANSACTION, $data_insert);
		return TRUE;
	}
	
	function update($fetch_id, $data_update, $where_arr=NULL)
	{
		if($where_arr)
		{
			$this->db->where($where_arr);
		}
		
		$this->db->where('fetch_id', $fetch_id);
		$this->db->update(TABLE_FETCH_TRANSACTION, $data_update);
		return TRUE;
	}

	function getTransactionDetails($fetch_id, $select=NULL)
	{
		if($select)
			$this->db->select($select);

		$this->db->where('fetch_id', $fetch_id);
		$query = $this->db->get(TABLE_FETCH_TRANSACTION);
		$res = $query->result_array();
		return $res[0]; 
	}

	function addTransactionUsers($data_insert)
	{
		$this->db->insert(TABLE_FETCH_TRANSACTION_USERS, $data_insert);
		return TRUE;
	}
	
	function updateTransactionUsers($fetch_id, $user_id, $data_update)
	{
		$this->db->where('ftu_transaction_id', $fetch_id);
		$this->db->where('ftu_user_id', $user_id);
		$this->db->update(TABLE_FETCH_TRANSACTION_USERS, $data_update);
		return TRUE;
	}

	function deleteTrasactionUsers($fetch_id)
	{
		$this->db->where('ftu_transaction_id', $fetch_id);
		$this->db->delete(TABLE_FETCH_TRANSACTION_USERS);
		return TRUE;
	}

	function getTransactionUsers($fetch_id, $user_id=NULL, $select=NULL, $where_str=NULL)
	{
		if($select)
			$this->db->select($select);
			
		if($where_str)
			$this->db->where('('.$where_str.')', NULL, false);

		$this->db->where('ftu_transaction_id', $fetch_id);
		
		if($user_id)
			$this->db->where('ftu_user_id!=', $user_id,false);

		$query = $this->db->get(TABLE_FETCH_TRANSACTION_USERS);
		$res = $query->result_array();
		return $res;
	}
	
	function getTransactionResponseCount($fetch_id, $status=NULL)
	{
		$this->db->where('ftu_transaction_id', $fetch_id);
		if($status)
			$this->db->where('ftu_response', $status);
			
		$query = $this->db->get(TABLE_FETCH_TRANSACTION_USERS);
		return $query->num_rows();
	}

	function getUserTransactionResponse($fetch_id, $user_id)
	{
		$this->db->select('ftu_response')->where('ftu_transaction_id', $fetch_id);
		$this->db->where('ftu_user_id', $user_id);
		$query = $this->db->get(TABLE_FETCH_TRANSACTION_USERS);
		
		if($query->num_rows()>0)
		{
			$res = $query->result_array();
			return $res[0]['ftu_response'];
		}
		else
		{
			return 0;
		}
	}
}
?>