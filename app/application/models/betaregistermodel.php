<?php
class Betaregistermodel extends CI_Model
{
	function __Construct()
	{
		parent::__Construct();
	}
	
	function registration($phone,$countrycode,$name,$email,$password,$city,$gender,$dob,$fbid,$accesstoken)

    {

    	try 

    	{

    		if($phone!="" && $countrycode!="" && $name!="" && $email!="" && $password!="" && $city!="")
    		{
    			if(is_numeric($phone)&& is_numeric($countrycode) && filter_var($email, FILTER_VALIDATE_EMAIL))
				{
					//checking for duplicate phone numbers
					$this->db->select('pk_user_app_info_id');
		        	$this->db->where('phone_no', $phone);
		        	$query = $this->db->get(TABLE_USER_MASTER);
		        	$res1 = $query->result_array();

		        	//checking for duplicate email
		        	$this->db->select('pk_user_app_info_id');
		        	$this->db->where('email_id', $email);
		        	$query = $this->db->get(TABLE_USER_MASTER);
		        	$res2 = $query->result_array();

		    		if(count($res1)>0 && count($res2)>0)
			        {
			        	$result['Result']['ErrorCode']='4';
		    			$result['Result']['ErrorMessage']="Duplicate records";
		    			$result['Result']['Flag']="b";
		    			echo json_encode($result);	        
			        }
					elseif (count($res1)>0)
			        {
			        	$result['Result']['ErrorCode']='4';
		    			$result['Result']['ErrorMessage']="Duplicate records";
		    			$result['Result']['Flag']="m";
		    	       	echo json_encode($result);
			        }
			        elseif (count($res2)>0)
			        {
			        	$result['Result']['ErrorCode']='4';
		    			$result['Result']['ErrorMessage']="Duplicate records";	
		    			$result['Result']['Flag']="e";
		    			echo json_encode($result);
			        }
			        else 
			        {
			        	$pwd=base64_encode($password);
			        	$currentdate = date("Y-m-d");

			        	$data1 = array(
						    'email_id' => $email ,
						    'password' => $pwd,
			        	    'country_code' =>$countrycode,
			        	    'phone_no' => $phone,
						    'created_date' => $currentdate,
			        		'social_id' => $fbid
						);

						$res1 = $this->db->insert(TABLE_USER_MASTER, $data1);
						$last_insert_id = $this->db->insert_id();

			        	$data3 = array(
						   'fk_user_app_info_id' => $last_insert_id,
						   'name' => $name,
			        		'city' => $city,
			        		'gender' => $gender,
			        		'dob' => $dob,
			        		'accesstoken' => $accesstoken
						);

						$res3 = $this->db->insert(PERSONAL_INFO, $data3);
						$result['Result']['ErrorCode']='0';
			    		$result['Result']['ErrorMessage']="Success";
				        echo json_encode($result);

						if($res1==1 && $res3==1)
						{
								//upload the image
								if (isset($_FILES['photo']))
								{
									//check the uploaded image size
									$file_size = $_FILES['photo']['size'];

									//echo "<pre>";
									//print_r($_FILES);
									//die();

									if(($file_size > 2097152) && ($file_size)==0)
									{
										$result['Result']['ErrorCode']='7';
										$result['Result']['ErrorMessage']="Image file too large";	 
								    }

								    else
								    {
									    //move the image
										$tmp_image_location =  "../images/";
										if(move_uploaded_file($_FILES['photo']['tmp_name'],BASE_URL.$tmp_image_location.$last_insert_id.".jpg"))
										{
											$result['Result']['ErrorCode']='0';
											$result['Result']['ErrorMessage']="Success";	
											$result['Result']['imageurl']="http://www.fetch-apps.com/Fetch/app/images/";
											//$result['Result']['imageurl']=BASE_URL."".$tmp_image_location;	
										}
										else 
										{
											$result['Result']['ErrorCode']='5';
										  	$result['Result']['ErrorMessage']="Image not uploaded";	
										}
								    }
								}

								//generate the activation code and update the status in the db
								$random = rand(1111, 9999);

								//update code in db
						       	$data = array('phone_activation_code'   => $random,
						       				  'phone_activation_status' => 1	
						       				);

								$this->db->where('pk_user_app_info_id', $last_insert_id);
								//$this->db->where('phone_registration_id', $phonereg);
								$flag=$this->db->update(TABLE_USER_MASTER, $data); 
						}
			        }
			    }
				else 
				{
					$result['Result']['ErrorCode']='2';
		    		$result['Result']['ErrorMessage']="Invalid input parameters";	
			        echo json_encode($result);
				}
    		}
    		else
    		{
    			$result['Result']['ErrorCode']='3';
		        $result['Result']['ErrorMessage']="Phone,Country code,Name,Email and password are Mandatory";
		        echo json_encode($result);
    		}
    	}
    	catch (Exception $e)
    	{
    		$result['Result']['ErrorCode']='-4';
    		$result['Result']['ErrorMessage']="Generic Error";	
	        echo json_encode($result);
    	}
    }
}
?>