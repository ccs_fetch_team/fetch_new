<?php
class Commfuncmodel extends CI_Model
{
	function __Construct()
	{
		parent::__Construct();
	}
	
	//fetch google cordinates from address
	function getDuration($fromLat,$fromLongi,$toLat,$toLongi)
	{
		$url = 'http://maps.googleapis.com/maps/api/directions/json?origin='.$fromLat.','.$fromLongi.'&destination='.$toLat.','.$toLongi.'&sensor=false';

		$ch = curl_init();
		ob_start();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_exec($ch);
		curl_close($ch);
		$data = ob_get_contents();
		ob_end_clean();

		$json_data = json_decode($data);
		 
		$data1 = $this->objectToArray($json_data);
                log_message("ERROR","getDuration".print_r($data1,TRUE));
		$results = $data1['routes'];
		$data1 = $this->objectToArray($data1['routes'][0]);
		$data1 = $this->objectToArray($data1['legs'][0]);
		
		$dur_arr = $this->objectToArray($data1['duration']);
		$distance_arr = $this->objectToArray($data1['distance']); 
		
		$result = array ( 'duration' => round($dur_arr['value']/60, 0),
						  'distance' => round($distance_arr['value']/1000, 2),
						);

		return $result;
	}
	
	function objectToArray($d)
	{
		if (is_object($d)) {
			// Gets the properties of the given object
			// with get_object_vars function
			$d = get_object_vars($d);
		}

		// Return array
		return $d;
	}

	function getUserImage($user_id)
	{
		$dir = DIR_USER_IMAGES;
		$file_name = $dir.$user_id.'.jpg';
		$image_path = '';
		
		if( file_exists($file_name) ) 
		{
			$image_path = base_url().'images/'.$user_id.'.jpg';
		}
		else
		{
			$image_path = NULL;
		}
		return $image_path;
	}

	function getFetchImage($fetch_photo)
	{
		$dir = realpath(DIR_FETCH_PHOTOS);
		$file_name = $dir.'/'.$fetch_photo;
		$image_path = '';
		
		if( file_exists($file_name) ) 
			$image_path = base_url().'fetch_photos/'.$fetch_photo;
		else
			$image_path = NULL;

		return $image_path;
	}

	function getCountries()
	{
		$this->db->select('cc_country');
		$query = $this->db->get('ccg_countries');
		
		if($query->num_rows()>0)
		{
			$res = $query->result_array();
			$country_list = array();
			foreach($res as $k=>$v)
			{
				//echo "<pre>"; print_r($v); //die('<hr>');
				$country_list[$v['cc_country']] = $v['cc_country'];
			}
			return $country_list;
		}
		else
		{
			return 0;
		}
	}
	
	
	/* =========== This function is used to send mail forgot password =========== */
	function sendEmail_old($to, $from, $sub, $mail_body, $old_arr, $new_arr)
	{
		$config_mail['mailtype'] = 'html';

		$this->load->library('email',$config_mail);

		$subject=$sub;

		$message=$mail_body;//$email_data["mail_body"];
		$this->email->from($from, '');
		$this->email->to($to);

		$message=str_replace($old_arr,$new_arr,$message);

		//echo "<hr>".$to.br(1).$subject.br(2).$message; die('<hr>yyyy');

		$this->email->subject($subject);
		$this->email->message($message);
		$this->email->send();

		return TRUE;
	}

	/* =========== This function is used to send mail forgot password =========== */
	function sendEmail($to, $from, $sub, $mail_body, $old_arr, $new_arr)
	{
		$config_mail['protocol'] = 'mail';
		$config_mail['smtp_port'] = 26;
		$config_mail['mailtype'] = 'html';

		$this->load->library('email',$config_mail);
		$subject=$sub;

		$message=$mail_body;
		$this->email->from($from, '');
		$this->email->to($to);

		$message=str_replace($old_arr,$new_arr,$message);

		$this->email->subject($subject);
		$this->email->message($message);
		$this->email->send();

		//$this->email->print_debugger(); echo "<hr>";
		//echo $to.br(1).$from.br(1).$sub.br(1).$message; die('<hr>');
		return TRUE;
	}
}
?>