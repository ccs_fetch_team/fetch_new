<?php

class Fetchmodel extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->model('Usermodel', '', TRUE);
        $this->load->model('Transactionmodel', '', TRUE);
        $this->load->model('Commfuncmodel', '', TRUE);
        $this->lang->load('message', 'english');
    }

    function login($em, $password, $devicereg, $devicetype, $flag) {
        try {
            if ($em != "" && $password != "" && $devicetype != "" && $devicereg != "" && $flag != "") {
                $encodedpassword = base64_encode($password);
                $this->db->select('pk_user_app_info_id');

                if ($flag == "e") {
                    $this->db->where('email_id', $em)->where('password', $encodedpassword);
                } else if ($flag == "m") {
                    $this->db->where('phone_no', $em)->where('password', $encodedpassword);
                }

                $query = $this->db->get(TABLE_USER_MASTER);
                $res = $query->result_array();

                if (count($res) > 0) {
                    $tmp_image_location = "images/";
                    $userid = $res[0]['pk_user_app_info_id'];
                    $this->db->select('pk_user_app_info_id');
                    $this->db->where('pk_user_app_info_id', $userid)->where('phone_activation_status', 1);
                    $query = $this->db->get(TABLE_USER_MASTER);
                    $res2 = $query->result_array();

                    if (count($res2) > 0) {
                        $result['Result']['ErrorCode'] = '0';
                        $result['Result']['ErrorMessage'] = "Success";
                        $result['Result']['userid'] = $userid;
                        //$result['Result']['imageurl']=BASE_URL."".$tmp_image_location;
                        //$result['Result']['imageurl']="http://www.fetch-apps.com/Fetch/app/images/";
                        // update the device id in database
                        $this->checkDeviceId($userid, $devicetype, $devicereg);
                    } else {
                        $result['Result']['ErrorCode'] = '6';
                        $result['Result']['ErrorMessage'] = "User Registered but snot activated";
                    }
                    echo json_encode($result);
                } else {
                    $result['Result']['ErrorCode'] = '1';
                    $result['Result']['ErrorMessage'] = "Failure";
                    echo json_encode($result);
                }
            } else {
                $result['Result']['ErrorCode'] = '3';
                $result['Result']['ErrorMessage'] = "em,password,devicereg,devicetype,flag are Mandatory";
                echo json_encode($result);
            }
        } catch (Exception $e) {
            $result['Result']['ErrorCode'] = '-4';
            $result['Result']['ErrorMessage'] = "Generic Failure";
            echo json_encode($result);
        }
    }

    function registration($phone, $countrycode, $name, $email, $password, $city, $gender, $dob, $fbid, $accesstoken) {
        try {
            if ($phone != "" && $countrycode != "" && $name != "" && $email != "" && $password != "" && $city != "") {
                if (is_numeric($phone) && is_numeric($countrycode) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    //checking for duplicate phone numbers
                    $this->db->select('pk_user_app_info_id');
                    $this->db->where('phone_no', $phone);
                    $query = $this->db->get(TABLE_USER_MASTER);
                    $res1 = $query->result_array();

                    //checking for duplicate email
                    $this->db->select('pk_user_app_info_id');
                    $this->db->where('email_id', $email);
                    $query = $this->db->get(TABLE_USER_MASTER);
                    $res2 = $query->result_array();

                    if (count($res1) > 0 && count($res2) > 0) {
                        $result['Result']['ErrorCode'] = '4';
                        $result['Result']['ErrorMessage'] = "Duplicate records";
                        $result['Result']['Flag'] = "b";
                        echo json_encode($result);
                    } elseif (count($res1) > 0) {
                        $result['Result']['ErrorCode'] = '4';
                        $result['Result']['ErrorMessage'] = "Duplicate records";
                        $result['Result']['Flag'] = "m";
                        echo json_encode($result);
                    } elseif (count($res2) > 0) {
                        $result['Result']['ErrorCode'] = '4';
                        $result['Result']['ErrorMessage'] = "Duplicate records";
                        $result['Result']['Flag'] = "e";
                        echo json_encode($result);
                    } else {
                        $pwd = base64_encode($password);
                        $currentdate = date("Y-m-d");

                        $data1 = array('email_id' => $email,
                            'password' => $pwd,
                            'country_code' => $countrycode,
                            'phone_no' => $phone,
                            'created_date' => $currentdate,
                            'social_id' => $fbid
                        );

                        $res1 = $this->db->insert(TABLE_USER_MASTER, $data1);

                        $last_insert_id = $this->db->insert_id();

                        if ($fbid != "") {
                            $headers = get_headers('https://graph.facebook.com/' . $fbid . '/picture', 1);
                            // just a precaution, check whether the header isset...
                            if (isset($headers['Location'])) {
                                $url = $headers['Location']; // string
                            } else {
                                $url = false; // nothing there? .. weird, but okay!
                            }
                            $destination = '/home1/capstoq5/public_html/fetch_new/app/images/' . $last_insert_id . '.jpg';
                            $content = file_get_contents($url);
                            $fp = fopen($destination, "w");
                            fwrite($fp, $content);
                            fclose($fp);
                        }

                        $data3 = array('fk_user_app_info_id' => $last_insert_id,
                            'name' => $name,
                            'city' => $city,
                            'gender' => $gender,
                            'dob' => $dob,
                            'accesstoken' => $accesstoken
                        );

                        $res3 = $this->db->insert(PERSONAL_INFO, $data3);

                        $result['Result']['ErrorCode'] = '0';
                        $result['Result']['ErrorMessage'] = "Success";
                        echo json_encode($result);

                        if ($res1 == 1 && $res3 == 1) {
                            //upload the image
                            if (isset($_FILES['photo'])) {
                                //check the uploaded image size
                                $file_size = $_FILES['photo']['size'];

                                if (($file_size > 2097152) && ($file_size) == 0) {
                                    $result['Result']['ErrorCode'] = '7';
                                    $result['Result']['ErrorMessage'] = "Image file too large";
                                } else {
                                    //move the image
                                    $tmp_image_location = "../images/";

                                    if (move_uploaded_file($_FILES['photo']['tmp_name'], BASE_URL . $tmp_image_location . $last_insert_id . ".jpg")) {
                                        $result['Result']['ErrorCode'] = '0';
                                        $result['Result']['ErrorMessage'] = "Success";
                                        $result['Result']['imageurl'] = "http://www.fetch-apps.com/Fetch/app/images/";
                                        //$result['Result']['imageurl']=BASE_URL."".$tmp_image_location;	
                                    } else {
                                        $result['Result']['ErrorCode'] = '5';
                                        $result['Result']['ErrorMessage'] = "Image not uploaded";
                                    }
                                }
                            }

                            //generate the activation code and update the status in the db
                            $random = rand(1111, 9999);

                            //update code in db
                            $data = array('phone_activation_code' => $random);

                            $this->db->where('pk_user_app_info_id', $last_insert_id);
                            //$this->db->where('phone_registration_id', $phonereg);

                            $flag = $this->db->update(TABLE_USER_MASTER, $data);

                            //sending the activation code via sms
                            $phonenew = $countrycode . $phone;

                            $from = 'FETCH';
                            $to = $phonenew;
                            //$message = array( 'text' => 'Hi '.$name.'! Your verification code is '.$random.'.');
                            $message = array('text' => 'Welcome to Fetch-Apps! Here is your verification code: ' . $random . '');

                            $response = $this->nexmo->send_message($from, $to, $message);
                        }
                    }
                } else {
                    $result['Result']['ErrorCode'] = '2';
                    $result['Result']['ErrorMessage'] = "Invalid input parameters";
                    echo json_encode($result);
                }
            } else {
                $result['Result']['ErrorCode'] = '3';
                $result['Result']['ErrorMessage'] = "Phone,Country code,Name,Email and password are Mandatory";
                echo json_encode($result);
            }
        } catch (Exception $e) {
            $result['Result']['ErrorCode'] = '-4';
            $result['Result']['ErrorMessage'] = "Generic Error";
            echo json_encode($result);
        }
    }

    //socialregistration
    function socialregistration($phone, $countrycode, $name, $email, $city, $id, $flag) {
        try {
            if ($phone != "" && $countrycode != "" && $name != "" && $email != "" && $id != "" && $flag != "") {
                if (ctype_alpha($name) && is_numeric($phone) && is_numeric($countrycode) && filter_var($email, FILTER_VALIDATE_EMAIL) && ctype_alpha($city)) {
                    //checking for duplicate phone numbers
                    $this->db->select('pk_user_app_info_id');
                    $this->db->where('phone_no', $phone);
                    $query = $this->db->get(TABLE_USER_MASTER);
                    $res1 = $query->result_array();

                    //checking for duplicate email
                    $this->db->select('pk_user_app_info_id');
                    $this->db->where('email_id', $email);
                    $query = $this->db->get(TABLE_USER_MASTER);
                    $res2 = $query->result_array();

                    if (count($res1) > 0 && count($res2) > 0) {
                        $result['Result']['ErrorCode'] = '4';
                        $result['Result']['ErrorMessage'] = "Duplicate records";
                        $result['Result']['Flag'] = "b";
                        echo json_encode($result);
                    } elseif (count($res1) > 0) {
                        $result['Result']['ErrorCode'] = '4';
                        $result['Result']['ErrorMessage'] = "Duplicate records";
                        $result['Result']['Flag'] = "m";
                        echo json_encode($result);
                    } elseif (count($res2) > 0) {
                        $result['Result']['ErrorCode'] = '4';
                        $result['Result']['ErrorMessage'] = "Duplicate records";
                        $result['Result']['Flag'] = "e";
                        echo json_encode($result);
                    } else {
                        //$pwd=base64_encode($password);
                        $currentdate = date("Y-m-d");
                        $ctidnum = rand(111, 999); //
                        $ctid = $name . $ctidnum;

                        $pseudonum = substr($phone, 6);
                        $pseudoname = $name . $pseudonum;

                        $data1 = array('email_id' => $email,
                            'country_code' => $countrycode,
                            'phone_no' => $phone,
                            'social_id' => $id,
                            'registered_through' => $flag,
                        );

                        $res1 = $this->db->insert(TABLE_USER_MASTER, $data1);

                        $last_insert_id = $this->db->insert_id();

                        if ($city != "") {
                            $data3 = array('fk_user_app_info_id' => $last_insert_id,
                                'name' => $name,
                                'city' => $city
                            );
                        } else {
                            $data3 = array('fk_user_app_info_id' => $last_insert_id,
                                'name' => $name
                            );
                        }

                        $res3 = $this->db->insert(PERSONAL_INFO, $data3);

                        $result['Result']['ErrorCode'] = '0';
                        $result['Result']['ErrorMessage'] = "Success";
                        echo json_encode($result);

                        if ($res1 == 1) {
                            //generate the activation code and update the status in the db
                            $random = rand(1111, 9999);

                            //update code in db
                            $data = array('phone_activation_code' => $random);

                            $this->db->where('pk_user_app_info_id', $last_insert_id);
                            $flag = $this->db->update(TABLE_USER_MASTER, $data);

                            //sending the activation code via sms
                            $phonenew = $countrycode . $phone;
                            $from = '918484069295';
                            $to = $phonenew;
                            //$message = array( 'text' => 'Hi '.$name.'! Your verification code is '.$random.'.');
                            $message = array('text' => 'Welcome to Fetch-Apps! Here is your verification code: ' . $random . '');

                            $response = $this->nexmo->send_message($from, $to, $message);
                        }
                    }
                } else {
                    $result['Result']['ErrorCode'] = '2';
                    $result['Result']['ErrorMessage'] = "Invalid input parameters";
                    echo json_encode($result);
                }
            } else {
                $result['Result']['ErrorCode'] = '3';
                $result['Result']['ErrorMessage'] = "Phone,Country code,Name,Email,city,id and flag are Mandatory";
                echo json_encode($result);
            }
        } catch (Exception $e) {
            $result['Result']['ErrorCode'] = '-4';
            $result['Result']['ErrorMessage'] = "Generic Error";
            echo json_encode($result);
        }
    }

    function activation($code, $phone, $devicereg, $devicetype) {
        try {
            if ($code != "" && $phone != "" && $devicereg != "" && $devicetype != "") {
                if (is_numeric($code) && is_numeric($phone)) {
                    $this->db->select('*');
                    $this->db->where('phone_activation_code', $code)->where('phone_no', $phone);
                    $query = $this->db->get(TABLE_USER_MASTER);
                    $res = $query->result_array();

                    if (count($res) > 0) {
                        $userid = $res[0]['pk_user_app_info_id'];

                        $data = array('phone_activation_status' => 1);

                        $this->db->where('phone_activation_code', $code);
                        $this->db->where('phone_no', $phone);

                        if ($this->db->update(TABLE_USER_MASTER, $data)) {
                            $data2 = array('fk_user_app_info_id' => $userid,
                                'device_type' => $devicetype,
                                'phone_registration_id' => $devicereg
                            );

                            $this->db->insert(USER_DEVICE, $data2);

                            $result['Result']['ErrorCode'] = '0';
                            $result['Result']['ErrorMessage'] = "Success";
                            $result['Result']['userid'] = $userid;
                            echo json_encode($result);
                        } else {
                            $result['Result']['ErrorCode'] = '1';
                            $result['Result']['ErrorMessage'] = "Failure";
                            echo json_encode($result);
                        }
                    } else {
                        $result['Result']['ErrorCode'] = '-2';
                        $result['Result']['ErrorMessage'] = "No Records Found";
                        echo json_encode($result);
                    }
                } else {
                    $result['Result']['ErrorCode'] = '2';
                    $result['Result']['ErrorMessage'] = "Invalid Input parameters";
                    echo json_encode($result);
                }
            } else {
                $result['Result']['ErrorCode'] = '3';
                $result['Result']['ErrorMessage'] = "Phone,Activation code are Mandatory";
                echo json_encode($result);
            }
        } catch (Exception $e) {
            $result['Result']['ErrorCode'] = '-4';
            $result['Result']['ErrorMessage'] = "Generic Error";
            echo json_encode($result);
        }
    }

    function resendcode($phone) {
        try {
            if ($phone != "") {
                if (is_numeric($phone)) {
                    $this->db->select('*');
                    $this->db->where('phone_no', $phone);
                    $query = $this->db->get(TABLE_USER_MASTER);
                    $res = $query->result_array();

                    if (count($res) > 0) {
                        $userid = $res[0]['pk_user_app_info_id'];
                        $countrycode = $res[0]['country_code'];
                        $newph = $countrycode . $phone;

                        $random = rand(1111, 9999);

                        //update code in db
                        $data = array('phone_activation_code' => $random);

                        $this->db->where('pk_user_app_info_id', $userid);
                        $flag = $this->db->update(TABLE_USER_MASTER, $data);

                        //retrieving name
                        $this->db->select('name');
                        $this->db->where('fk_user_app_info_id', $userid);
                        $query2 = $this->db->get(PERSONAL_INFO);
                        $res2 = $query2->result_array();

                        if (count($res2) > 0) {
                            $name = $res2[0]['name'];

                            //sending message
                            $from = '918484069295';
                            $to = $newph;

                            //$message = array( 'text' => 'Hi '.$name.'! Your verification code is '.$random.'.' );
                            $message = array('text' => 'Welcome to Fetch-Apps! Here is your verification code: ' . $random . '');

                            $response = $this->nexmo->send_message($from, $to, $message);

                            $result['Result']['ErrorCode'] = '0';
                            $result['Result']['ErrorMessage'] = "Success";
                            echo json_encode($result);
                        } else {
                            $result['Result']['ErrorCode'] = '-2';
                            $result['Result']['ErrorMessage'] = "No Records Found";
                            echo json_encode($result);
                        }
                    } else {
                        $result['Result']['ErrorCode'] = '-2';
                        $result['Result']['ErrorMessage'] = "No Records Found";
                        echo json_encode($result);
                    }
                } else {
                    $result['Result']['ErrorCode'] = '2';
                    $result['Result']['ErrorMessage'] = "Invalid Input parameters";
                    echo json_encode($result);
                }
            } else {
                $result['Result']['ErrorCode'] = '3';
                $result['Result']['ErrorMessage'] = "Phone is Mandatory";
                echo json_encode($result);
            }
        } catch (Exception $e) {
            $result['Result']['ErrorCode'] = '-4';
            $result['Result']['ErrorMessage'] = "Generic Error";
            echo json_encode($result);
        }
    }

    function forgot($em, $flag) {
        try {
            if ($em != "" && $flag != "") {
                $this->db->select('*');

                if ($flag == "e") {
                    $this->db->where('email_id', $em);
                } else if ($flag == "m") {
                    $this->db->where('phone_no', $em);
                }

                $query = $this->db->get(TABLE_USER_MASTER);
                $res = $query->result_array();

                if (count($res) > 0) {
                    $pass = $res[0]['password'];
                    $countrycode = $res[0]['country_code'];
                    $email = $res[0]['email_id'];
                    $pwd = base64_decode($pass);

                    //if email sent
//                    if ($flag == "e") {
                    // subject
                    $subject = 'Your Password for fetch';

                    // message
                    $message = "Your password is '" . $pwd . "'";

                    // To send HTML mail, the Content-type header must be set
                    $headers = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                    // Additional headers
                    $headers .= '';

                    // Mail it
                    if ($pwd != "") {
                        if (mail($email, $subject, $message, $headers)) {
                            $result['Result']['ErrorCode'] = '0';
                            $result['Result']['ErrorMessage'] = "Success";
                        }
                    } else {
                        $result['Result']['ErrorCode'] = '1';
                        $result['Result']['ErrorMessage'] = "Failure";
                    }
//                    }
                    //if mobile sent
//                    else if ($flag == "m") {
//                        //sending message
//                        $from = '918484069295';
//                        $to = $countrycode . $em;
//
//                        $message = array('text' => 'Your password is ' . $pwd . '.');
//
//                        $response = $this->nexmo->send_message($from, $to, $message);
//
//                        $result['Result']['ErrorCode'] = '0';
//                        $result['Result']['ErrorMessage'] = "Success";
//                    }
                    echo json_encode($result);
                } else {
                    $result['Result']['ErrorCode'] = '-2';
                    $result['Result']['ErrorMessage'] = "No Records found";
                    echo json_encode($result);
                }
            } else {
                $result['Result']['ErrorCode'] = '3';
                $result['Result']['ErrorMessage'] = "em and flag are Mandatory";
                echo json_encode($result);
            }
        } catch (Exception $e) {
            $result['Result']['ErrorCode'] = '-4';
            $result['Result']['ErrorMessage'] = "Generic Error";
            echo json_encode($result);
        }
    }

    function upload($userid) {
        try {
            if ($userid != "") {
                if (is_numeric($userid)) {
                    $this->db->select('*');
                    $this->db->where('pk_user_app_info_id', $userid);
                    $query = $this->db->get(TABLE_USER_MASTER);
                    //$res = $query->result_array();

                    /*
                      if($query->num_rows()>0)
                      {
                      echo "<pre>"; print_r($_FILES); die('<hr>yyyy');
                      //move the image
                      $tmp_image_location = realpath(DIR_USER_IMAGES);

                      if(move_uploaded_file($_FILES['photo']['tmp_name'],$tmp_image_location.$userid.".jpg"))
                      {
                      $result['Result']['ErrorCode']='0';
                      $result['Result']['ErrorMessage']="Success";
                      $result['Result']['imageurl']=base_url()."images/";
                      //$result['Result']['imageurl']=BASE_URL."".$tmp_image_location;
                      }
                      else
                      {
                      $result['Result']['ErrorCode']='5';
                      $result['Result']['ErrorMessage']="Image not uploaded";
                      }
                      echo json_encode($result);
                      }
                     */
                    //Code for uploading file to temporary directory
                    /* =========== Begins Here =========== */
                    if (@$_FILES['photo']["name"] != "") {
                        $ext = substr($_FILES['photo']["name"], strrpos($_FILES['photo']["name"], ".") + 1);
                        $ext = strtolower($ext);

                        $types_arr = explode("|", lang('allowed_types'));

                        if (in_array($ext, $types_arr)) {
                            if ($_FILES['photo']['size'] <= MAX_FILE_SIZE) {
                                $uploaddir = realpath(DIR_USER_IMAGES) . '/';
                                $uploadfile = $userid . '.' . $ext;

                                if (move_uploaded_file($_FILES['photo']['tmp_name'], $uploaddir . $uploadfile)) {
                                    $photo_arr["photo"]["file_name"] = $uploadfile;
                                    $photo_arr["photo"]['file_ext'] = $ext;
                                    $photo_arr["photo"]["name"] = $uploadfile;
                                    $success = 'y';

                                    $result['Result']['ErrorCode'] = '0';
                                    $result['Result']['ErrorMessage'] = "Success";
                                    $result['Result']['imageurl'] = base_url() . "images/";
                                    echo json_encode($result);
                                } else {
                                    $result['Result']['ErrorCode'] = '2';
                                    $result['Result']['ErrorMessage'] = "Error in file uploading";
                                    $success = "n";
                                    echo json_encode($result);
                                }
                            } else {
                                $result['Result']['ErrorCode'] = '2';
                                $result['Result']['ErrorMessage'] = "File size exceeds";
                                echo json_encode($result);
                            }
                        } else {
                            $success = "n";
                            $result['Result']['ErrorCode'] = '2';
                            $result['Result']['ErrorMessage'] = "Invalid file type";
                            echo json_encode($result);
                        }
                    } else {
                        $result['Result']['ErrorCode'] = '-2';
                        $result['Result']['ErrorMessage'] = "No Records found";
                        echo json_encode($result);
                    }
                } else {
                    $result['Result']['ErrorCode'] = '2';
                    $result['Result']['ErrorMessage'] = "Invalid Input parameters";
                    echo json_encode($result);
                }
            } else {
                $result['Result']['ErrorCode'] = '3';
                $result['Result']['ErrorMessage'] = "User ID is Mandatory";
                echo json_encode($result);
            }
        } catch (Exception $e) {
            $result['Result']['ErrorCode'] = '-4';
            $result['Result']['ErrorMessage'] = "Generic Error";

            echo json_encode($result);
        }
    }

    function contact($fromfetchid, $phonearray) {
        try {
            if ($phonearray != "" && $fromfetchid != "") {
                if (is_numeric($fromfetchid)) {
                    $this->db->select('pk_user_app_info_id,phone_no');
                    $this->db->where_in('pk_user_app_info_id', $fromfetchid);
                    $query = $this->db->get(TABLE_USER_MASTER);
                    $checkid = $query->result_array();

                    if (count($checkid) > 0) {
                        $ary = explode(',', $phonearray);
                        //echo "<pre> user input";print_r($ary);echo "</pre>"; die();
                        $infetch[] = "";
                        //infetch
                        //code added by ashwini on 22nd nov 2013
                        $twodigit = array();
                        $tworemain = array();
                        $threedigit = array();
                        $threeremain = array();
                        foreach ($ary as $arrayk) {
                            $twodigit[] = substr($arrayk, 0, 2);
                            $tworemain[] = substr($arrayk, 2);
                            $threedigit[] = substr($arrayk, 0, 3);
                            $threeremain[] = substr($arrayk, 3);
                        }

                        //code ends here
                        $this->db->select('pk_user_app_info_id,phone_no');
                        $this->db->where_in('phone_no', $ary);
                        $this->db->or_where_in('country_code', $twodigit)->where_in('phone_no', $tworemain);
                        $this->db->or_where_in('country_code', $threedigit)->where_in('phone_no', $threeremain);
                        $query = $this->db->get(TABLE_USER_MASTER);
                        $infetch = $query->result_array();

                        if (count($infetch) > 0) {
                            $flag = 0;
                        } else {
                            $infetch[] = "";
                        }

                        // retrieve only phone no to array_map reset
                        $this->db->select('phone_no');
                        $this->db->where_in('phone_no', $ary);
                        $this->db->or_where_in('country_code', $twodigit)->where_in('phone_no', $tworemain);
                        $this->db->or_where_in('country_code', $threedigit)->where_in('phone_no', $threeremain);
                        $query = $this->db->get(TABLE_USER_MASTER);
                        $infetchres = $query->result_array();
                        $ary2 = array_map("reset", $infetchres);
                        //echo "***arraymap infetch ***";echo "<pre>";print_r($infetch);echo "</pre>";
                        $newarray[] = "";
                        if (count($ary2) > 0) {
                            $this->db->select('pk_user_app_info_id');
                            $this->db->where_in('phone_no', $ary2);
                            $this->db->or_where_in('country_code', $twodigit)->where_in('phone_no', $tworemain);
                            $this->db->or_where_in('country_code', $threedigit)->where_in('phone_no', $threeremain);
                            $query = $this->db->get(TABLE_USER_MASTER);
                            $blockedid = $query->result_array();
                            $blockedidres = array_map("reset", $blockedid);

                            $this->db->select('fk_user_app_info_pal_id');
                            $this->db->where('fk_user_app_info_id', $fromfetchid);
                            $this->db->where_in('fk_user_app_info_pal_id', $blockedidres);
                            $query = $this->db->get(BLOCKED_PALS);
                            $res3 = $query->result_array();
                            $blockedidfinal = array_map("reset", $res3); //checking present in blocked table
                            $res4 = array();
                            if (count($res3) > 0) {
                                $this->db->select('pk_user_app_info_id,phone_no');
                                $this->db->where_in('pk_user_app_info_id', $blockedidfinal);
                                $query = $this->db->get(TABLE_USER_MASTER);
                                $res4 = $query->result_array();
                            } else {
                                $res4 = array();
                            }
                        } else {
                            $notinfetch = $ary2;
                            $ary2[] = "";
                            $res4[] = "";
                        }

                        $newinfetch = array();
                        $s = 0;

                        if (!empty($infetch)) {
                            foreach ($infetch as $dbinfetch) {
                                foreach ($ary as $userinputarray) {
                                    $tworemain = substr($userinputarray, 2);
                                    $threeremain = substr($userinputarray, 3);
                                    $phone_num = $dbinfetch['phone_no'];
                                    if ($phone_num == $userinputarray) {
                                        $newinfetch[$s]['pk_user_app_info_id'] = $dbinfetch['pk_user_app_info_id'];
                                        $newinfetch[$s]['phone_no'] = $userinputarray;
                                        //echo "phone number equals = "; print_r($newinfetch);
                                        $s++;
                                        break;
                                    } elseif ($phone_num == $tworemain) {
                                        $newinfetch[$s]['pk_user_app_info_id'] = $dbinfetch['pk_user_app_info_id'];
                                        $newinfetch[$s]['phone_no'] = $userinputarray;
                                        //echo "two phone number equals = "; print_r($newinfetch);
                                        $s++;
                                        break;
                                    } elseif ($phone_num == $threeremain) {
                                        $newinfetch[$s]['pk_user_app_info_id'] = $dbinfetch['pk_user_app_info_id'];
                                        $newinfetch[$s]['phone_no'] = $threeremain;
                                        $s++;
                                        break;
                                    }
                                }
                            }
                        } else {
                            $newinfetch = array();
                        }

                        $newblocked = array();
                        $k = 0;

                        if (!empty($res4) && !empty($ary)) {
                            foreach ($res4 as $dbinblocked) {
                                //print_r($dbinblocked['phone_no']);die();
                                foreach ($ary as $userinputarray) {
                                    $tworemain = substr($userinputarray, 2);
                                    $threeremain = substr($userinputarray, 3);
                                    $phone_blocked = $dbinblocked['phone_no'];
                                    if ($phone_blocked == $userinputarray) {
                                        $newblocked[$k]['pk_user_app_info_id'] = $dbinblocked['pk_user_app_info_id'];
                                        $newblocked[$k]['phone_no'] = $userinputarray;
                                        //echo "phone number equals = "; print_r($newinfetch);
                                        $k++;
                                        break;
                                    } elseif ($phone_blocked == $tworemain) {
                                        $newblocked[$k]['pk_user_app_info_id'] = $dbinblocked['pk_user_app_info_id'];
                                        $newblocked[$k]['phone_no'] = $userinputarray;
                                        //echo "two phone number equals = "; print_r($newinfetch);
                                        $k++;
                                        break;
                                    } elseif ($phone_blocked == $threeremain) {
                                        $newblocked[$k]['pk_user_app_info_id'] = $dbinblocked['pk_user_app_info_id'];
                                        $newblocked[$k]['phone_no'] = $threeremain;
                                        $k++;
                                        break;
                                    }
                                }
                            }
                        } else {
                            $newblocked = array();
                        }

                        $infetcharray = array_map("reset", $newinfetch);
                        $blockedarray = array_map("reset", $newblocked);
                        $diff = array_diff($infetcharray, $blockedarray);
                        $n = array_values($diff);
                        //print_r($n);
                        $new_infetch_array = array();
                        foreach ($newinfetch as $new_infetch) {
                            if (in_array($new_infetch['pk_user_app_info_id'], $n)) {
                                $new_infetch_array[] = $new_infetch;
                            }
                        }
                        $new_infetch_array[] = $checkid[0];

                        $result['ErrorCode'] = '0';
                        $result['ErrorMessage'] = "Success";
                        $result['in_fetch'] = $new_infetch_array;
                        $result['blocked_contacts'] = $newblocked;
                        $new['Result'] = array($result);
                        echo json_encode($new);
                    } else {
                        $result['Result']['ErrorCode'] = '-2';
                        $result['Result']['ErrorMessage'] = "No Records found";
                        echo json_encode($result);
                    }
                } else {
                    $result['Result']['ErrorCode'] = '2';
                    $result['Result']['ErrorMessage'] = "Invalid Input parameters";
                    echo json_encode($result);
                }
            } else {
                $result['Result']['ErrorCode'] = '3';
                $result['Result']['ErrorMessage'] = "from_fetch_id,phone_array is Mandatory";
                echo json_encode($result);
            }
        } catch (Exception $e) {
            $result['Result']['ErrorCode'] = '-4';
            $result['Result']['ErrorMessage'] = "Generic Error";
            echo json_encode($result);
        }
    }

    function createfetchrequest($fromfetchid, $tofetchid, $datetime, $fromlat, $fromlong, $fromaddress) {

        try {

            if ($fromfetchid != "" && $tofetchid != "" && $datetime != "" && $fromlat != "" && $fromlong != "" && $fromaddress != "") {

                if (is_numeric($fromfetchid)) {

                    $to_id = explode(",", $tofetchid);

                    //print_r($to_id);

                    $len = count($to_id);

                    for ($i = 0; $i < $len; $i++) {

                        $data = array(
                            'from_FKregister_id' => $fromfetchid,
                            'to_FKregister_id' => $to_id[$i],
                            'c_date' => $datetime,
                            'from_lat' => $fromlat,
                            'from_long' => $fromlong,
                            'from_address' => $fromaddress
                        );

                        $this->db->insert('fetch_transaction', $data);
                    }

                    $res = array();

                    $this->db->select('pk_user_app_info_id');

                    $this->db->where_in('pk_user_app_info_id', $to_id);

                    $query = $this->db->get(TABLE_USER_MASTER);

                    $res = $query->result_array();

                    if (count($res) > 0) {

                        //print_r($res);

                        $newarray = array_map("reset", $res);

                        //retrieving the registration id and device type of fetchee to send notification

                        $this->db->select('fk_user_app_info_id,phone_registration_id,device_type');

                        $this->db->where_in('fk_user_app_info_id', $newarray);

                        $query = $this->db->get(USER_DEVICE);

                        $res2 = $query->result_array();

                        //print_r($res2); 

                        if (count($res2) > 0) {

                            for ($j = 0; $j < count($res2); $j++) {

                                //retrieving fetcher's name

                                $this->db->select('name');

                                $this->db->where_in('fk_user_app_info_id', $fromfetchid);

                                $query = $this->db->get(PERSONAL_INFO);

                                $fetcheename = $query->result_array();

                                if (count($fetcheename) > 0) {

                                    $name = $fetcheename['0']['name'];
                                }

                                $registrationid = $res2[$j]['phone_registration_id'];

                                $registration_ids = array($registrationid);

                                $devicetype = $res2[$j]['device_type'];

                                $message = "You have a new fetch request from " . $name . "";

                                $message = array("price" => $message);

                                if ($devicetype == 0) {

                                    //send notification to android

                                    print_r($registration_ids);

                                    print_r($message);

                                    die();

                                    send_notification($registrationids, $message);
                                } else {

                                    //send notification to ios
                                }
                            }
                        } else {

                            $result['Result']['ErrorCode'] = '-2';

                            $result['Result']['ErrorMessage'] = "No Records found";

                            echo json_encode($result);
                        }
                    } else {

                        $result['Result']['ErrorCode'] = '-2';

                        $result['Result']['ErrorMessage'] = "No Records found";

                        echo json_encode($result);
                    }
                } else {

                    $result['Result']['ErrorCode'] = '2';

                    $result['Result']['ErrorMessage'] = "Invalid Input parameters";

                    echo json_encode($result);
                }
            } else {

                $result['Result']['ErrorCode'] = '3';

                $result['Result']['ErrorMessage'] = "from_fetch_id,phone_array is Mandatory";

                echo json_encode($result);
            }
        } catch (Exception $e) {

            $result['Result']['ErrorCode'] = '-4';

            $result['Result']['ErrorMessage'] = "Generic Error";

            echo json_encode($result);
        }
    }

    function send_notification($registrationIDs, $message = "") {

        //echo "hie";

        $apiKey = "AIzaSyBR2HmV443r7FluNLjSRQqI49xtwIYK29A"; //new latest

        $message = json_encode($message);

        // Set POST variables

        $url = 'https://android.googleapis.com/gcm/send';



        $fields = array(
            'registration_ids' => $registrationIDs,
            'data' => array("message" => $message),
        );



        $headers = array(
            'Authorization: key=' . $apiKey,
            'Content-Type: application/json'
        );



        // Open connection

        $ch = curl_init();



        // Set the url, number of POST vars, POST data

        curl_setopt($ch, CURLOPT_URL, $url);



        curl_setopt($ch, CURLOPT_POST, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);



        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));



        // Execute post

        $result = curl_exec($ch);

        //print_r($result); die();
        //print_r($result);
        // Close connection



        curl_close($ch);



        return (json_decode($result));
    }

    //send notification to ios

    function send_ios_notification($registrationIDs, $message = "") {

        $deviceToken = $registrationIDs;

        $passphrase = 'arvindravi991';

        $message = json_encode($message);

        ////////////////////////////////////////////////////////////////////////////////

        $ctx = stream_context_create();

        stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck.pem');

        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);



        // Open a connection to the APNS server

        $fp = stream_socket_client(
                'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 120, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);



        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);

        echo 'Connected to APNS' . PHP_EOL;





        // Create the payload body

        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default'
        );



        // Encode the payload as JSON

        $payload = json_encode($body);



        // Build the binary notification

        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;



        // Send it to the server

        $result = fwrite($fp, $msg, strlen($msg));





        /*

          if (!$result)

          echo 'Message not delivered' . PHP_EOL;

          else

          echo 'Message successfully delivered' . PHP_EOL;

         */

        // Close the connection to the server

        fclose($fp);



        if (!$result)
            return false;
        else
            return true;
    }

    //block a particular fetch user

    function blockcontact($fromfetchid, $tofetchid) {

        try {

            if ($fromfetchid != "" && $tofetchid != "") {

                if (is_numeric($fromfetchid) && is_numeric($tofetchid)) {

                    //check whether user registered or not

                    $this->db->select('*');

                    $this->db->where('pk_user_app_info_id', $fromfetchid);

                    $query = $this->db->get(TABLE_USER_MASTER);

                    $result1 = $query->result_array();



                    $this->db->select('*');

                    $this->db->where('pk_user_app_info_id', $tofetchid);

                    $query = $this->db->get(TABLE_USER_MASTER);

                    $result2 = $query->result_array();

                    if (count($result1) > 0 && count($result2) > 0) {

                        $this->db->select('*');

                        $this->db->where('fk_user_app_info_id', $fromfetchid);

                        $this->db->where('fk_user_app_info_pal_id', $tofetchid);

                        $query = $this->db->get('user_blocked_pals');

                        $alreadypresent = $query->result_array();

                        if (count($alreadypresent) > 0) {

                            $result['Result']['ErrorCode'] = '4';

                            $result['Result']['ErrorMessage'] = "Duplicate Records";

                            echo json_encode($result);
                        } else {

                            $date = date("Y-m-d");

                            $data = array(
                                'fk_user_app_info_id' => $fromfetchid,
                                'fk_user_app_info_pal_id' => $tofetchid,
                                'date' => $date
                            );

                            $this->db->insert('user_blocked_pals', $data);

                            $result['Result']['ErrorCode'] = '0';

                            $result['Result']['ErrorMessage'] = "Success";

                            echo json_encode($result);
                        }
                    } else {

                        $result['Result']['ErrorCode'] = '-2';

                        $result['Result']['ErrorMessage'] = "No Records found";

                        echo json_encode($result);
                    }
                } else {

                    $result['Result']['ErrorCode'] = '2';

                    $result['Result']['ErrorMessage'] = "Invalid Input parameters";

                    echo json_encode($result);
                }
            } else {

                $result['Result']['ErrorCode'] = '3';

                $result['Result']['ErrorMessage'] = "from_fetch_id,to_fetch_id are Mandatory";

                echo json_encode($result);
            }
        } catch (Exception $e) {

            $result['Result']['ErrorCode'] = '-4';

            $result['Result']['ErrorMessage'] = "Generic Error";

            echo json_encode($result);
        }
    }

    //set a particular user as favourite

    function favouritecontact($fromfetchid, $tofetchid) {

        try {

            if ($fromfetchid != "" && $tofetchid != "") {

                if (is_numeric($fromfetchid) && is_numeric($tofetchid)) {

                    //check whether user registered or not

                    $this->db->select('*');

                    $this->db->where('pk_user_app_info_id', $fromfetchid);

                    $query = $this->db->get(TABLE_USER_MASTER);

                    $result1 = $query->result_array();



                    $this->db->select('*');

                    $this->db->where('pk_user_app_info_id', $tofetchid);

                    $query = $this->db->get(TABLE_USER_MASTER);

                    $result2 = $query->result_array();

                    if (count($result1) > 0 && count($result2) > 0) {

                        $this->db->select('*');

                        $this->db->where('fk_user_app_info_id', $fromfetchid);

                        $this->db->where('fk_user_app_info_pal_id', $tofetchid);

                        $query = $this->db->get('user_favourite_pals');

                        $alreadypresent = $query->result_array();

                        if (count($alreadypresent) > 0) {

                            $result['Result']['ErrorCode'] = '4';

                            $result['Result']['ErrorMessage'] = "Duplicate Records";

                            echo json_encode($result);
                        } else {

                            $date = date("Y-m-d");

                            $data = array(
                                'fk_user_app_info_id' => $fromfetchid,
                                'fk_user_app_info_pal_id' => $tofetchid,
                                'date' => $date
                            );

                            $this->db->insert('user_favourite_pals', $data);

                            $result['Result']['ErrorCode'] = '0';

                            $result['Result']['ErrorMessage'] = "Success";

                            echo json_encode($result);
                        }
                    } else {

                        $result['Result']['ErrorCode'] = '-2';

                        $result['Result']['ErrorMessage'] = "No Records found";

                        echo json_encode($result);
                    }
                } else {

                    $result['Result']['ErrorCode'] = '2';

                    $result['Result']['ErrorMessage'] = "Invalid Input parameters";

                    echo json_encode($result);
                }
            } else {
                $result['Result']['ErrorCode'] = '3';
                $result['Result']['ErrorMessage'] = "from_fetch_id,to_fetch_id are Mandatory";
                echo json_encode($result);
            }
        } catch (Exception $e) {
            $result['Result']['ErrorCode'] = '-4';
            $result['Result']['ErrorMessage'] = "Generic Error";
            echo json_encode($result);
        }
    }

    //edit profile

    function editprofile($userid, $name,$last_name, $status, $phone, $email, $dob, $gender, $city) {

        try {

            if ($userid != "") {

                if (is_numeric($userid)) {

                    //check whether user registered or not

                    $this->db->select('*');

                    $this->db->where('pk_user_app_info_id', $userid);

                    $query = $this->db->get(TABLE_USER_MASTER);

                    $result1 = $query->result_array();



                    if (count($result1) > 0) {

                        //update phone,email

                        $data = array(
                            'email_id' => $email,
                            'phone_no' => $phone
                        );

                        $this->db->where('pk_user_app_info_id', $userid);

                        $this->db->update(TABLE_USER_MASTER, $data);

                        //	if($gender=="m"){ $f=0;} //0- male and 1- female
                        //else { $f=1;}

                        if ($status != "") {

                            $data3 = array(
                                'name' => $name,
                                'last_name'=>$last_name,
                                'city' => $city,
                                'dob' => $dob,
                                'status' => $status,
                                'gender' => $gender
                            );
                        } else {

                            $data3 = array(
                                'name' => $name,
                                'last_name'=>$last_name,
                                'city' => $city,
                                'dob' => $dob,
                                'gender' => $gender
                            );
                        }



                        $this->db->where('fk_user_app_info_id', $userid);

                        $this->db->update(PERSONAL_INFO, $data3);

                        // Image upload  start
                        //Image upload End

                        if (@$_FILES['photo']["name"] != "") {
                            $ext = substr($_FILES['photo']["name"], strrpos($_FILES['photo']["name"], ".") + 1);
                            $ext = strtolower($ext);

                            $types_arr = explode("|", lang('allowed_types'));

                            if (in_array($ext, $types_arr)) {
                                if ($_FILES['photo']['size'] <= MAX_FILE_SIZE) {
                                    $uploaddir = realpath(DIR_USER_IMAGES) . '/';
                                    $uploadfile = $userid . '.' . $ext;

                                    if (move_uploaded_file($_FILES['photo']['tmp_name'], $uploaddir . $uploadfile)) {
                                        $photo_arr["photo"]["file_name"] = $uploadfile;
                                        $photo_arr["photo"]['file_ext'] = $ext;
                                        $photo_arr["photo"]["name"] = $uploadfile;
                                        $success = 'y';

                                        $result['Result']['ErrorCode'] = '0';
                                        $result['Result']['ErrorMessage'] = "Success";
                                        $result['Result']['UserId'] = $userid;
                                        $result['Result']['imageurl'] = base_url() . "images/" . $uploadfile;
                                        //echo json_encode($result);
                                    } else {
                                        $result['Result']['ErrorCode'] = '2';
                                        $result['Result']['ErrorMessage'] = "Error in file uploading";
                                        $success = "n";
                                        //echo json_encode($result);
                                    }
                                } else {
                                    $result['Result']['ErrorCode'] = '2';
                                    $result['Result']['ErrorMessage'] = "File size exceeds";
                                    //echo json_encode($result);
                                }
                            } else {
                                $success = "n";
                                $result['Result']['ErrorCode'] = '2';
                                $result['Result']['ErrorMessage'] = "Invalid file type";
                                //echo json_encode($result);
                            }
                        } else {
                            $result['Result']['ErrorCode'] = '0';
                            $result['Result']['ErrorMessage'] = "Success";
                        }
                        echo json_encode($result);
                    } else {
                        $result['Result']['ErrorCode'] = '-2';
                        $result['Result']['ErrorMessage'] = "No Records found";
                        echo json_encode($result);
                    }
                } else {
                    $result['Result']['ErrorCode'] = '2';
                    $result['Result']['ErrorMessage'] = "Invalid Input parameters";
                    echo json_encode($result);
                }
            } else {
                $result['Result']['ErrorCode'] = '3';
                $result['Result']['ErrorMessage'] = "userid are Mandatory";
                echo json_encode($result);
            }
        } catch (Exception $e) {
            $result['Result']['ErrorCode'] = '-4';
            $result['Result']['ErrorMessage'] = "Generic Error";
            echo json_encode($result);
        }
    }

    //ETA refresh
    function ETArefresh($from_fetch_id, $to_fetch_id, $time) {

        try {

            if ($from_fetch_id != "" && $to_fetch_id != "" && $time != "") {

                if (is_numeric($from_fetch_id) && is_numeric($to_fetch_id)) {

                    //check whether user registered or not

                    $this->db->select('*');

                    $this->db->where('status', 1);

                    $this->db->where('from_FKregister_id', $from_fetch_id);

                    $this->db->or_where('from_FKregister_id', $to_fetch_id);

                    $this->db->where('to_FKregister_id', $to_fetch_id);

                    $this->db->or_where('to_FKregister_id', $from_fetch_id);

                    $this->db->order_by("fetch_transaction_id", "desc");

                    $this->db->limit(1);

                    $query = $this->db->get('fetch_transaction');

                    $result1 = $query->result_array();



                    if (count($result1) > 0) {

                        //retrieve fetch transaction id

                        $fetchtransactionid = $result1['0']['fetch_transaction_id'];



                        //update fetch_ETA

                        $data = array(
                            'fetch_ETA' => $time
                        );

                        $this->db->where('fetch_transaction_id', $fetchtransactionid);

                        $this->db->update('fetch_transaction', $data);



                        $result['Result']['ErrorCode'] = '0';

                        $result['Result']['ErrorMessage'] = "Success";

                        echo json_encode($result);
                    } else {

                        $result['Result']['ErrorCode'] = '-2';

                        $result['Result']['ErrorMessage'] = "No Records found";

                        echo json_encode($result);
                    }
                } else {

                    $result['Result']['ErrorCode'] = '2';

                    $result['Result']['ErrorMessage'] = "Invalid Input parameters";

                    echo json_encode($result);
                }
            } else {

                $result['Result']['ErrorCode'] = '3';

                $result['Result']['ErrorMessage'] = "from_fetch_id,to_fetch_id,time are Mandatory";

                echo json_encode($result);
            }
        } catch (Exception $e) {

            $result['Result']['ErrorCode'] = '-4';

            $result['Result']['ErrorMessage'] = "Generic Error";

            echo json_encode($result);
        }
    }

    //fetchhistory
    function fetchhistory($userid, $page_no) {

        try {

            if ($userid != "" && $page_no != "") {

                if (is_numeric($userid) && is_numeric($page_no)) {

                    $date = date("Y-m-d");

                    //retrieve data

                    $this->db->select('*');

                    $status = array('0', '1', '2', '3', '5');

                    $this->db->where_in('fetch_status', $status);

                    $this->db->where('DATE_FORMAT(fetch_datetime,"%Y-%m-%d") < "' . $date . '"');

                    $this->db->where('(fetch_creator_id=' . $userid . ' OR ftu_user_id=' . $userid . ')');


                    if ($page_no == 0) {

                        $limitset = $page_no;
                    } else {

                        $limitset = $page_no * NO_OF_RECORDS_PER_PAGE;
                    }

                    $this->db->limit(NO_OF_RECORDS_PER_PAGE, $limitset);

                    $this->db->join('fetch_trasaction_users', 'fetch_transaction_new.fetch_id = fetch_trasaction_users.ftu_transaction_id');

                    $this->db->order_by('fetch_datetime', 'desc');

                    $query = $this->db->get('fetch_transaction_new');

                    $result1 = $query->result_array();

                    //echo "<pre>"; print_r($result1); echo "</pre>"; die();
                    //$r= $this->db->last_query();
                    //echo $r;die();

                    $newarry = array();

                    if (count($result1) > 0) {

                        $userrole = "";

                        for ($i = 0; $i < count($result1); $i++) {

                            $var = $result1[$i]['fetch_creator_id'];
                            if ($userid == $var) {
                                $userrole = '0'; //user is fetch creator
                            } else {

                                $userrole = '1'; //user is fetch user
                            }

                            //check whether user is fetcher or not and update the output accordingly



                            if ($userrole == 0) { //fetch creator
                                $newarry[$i]['fetch_id'] = $result1[$i]['ftu_user_id'];
                            } else if ($userrole == 1) {//fetch user
                                $newarry[$i]['fetch_id'] = $var;
                            }

                            //$newarry[$i]['fetch_id'] = $result1[$i]['to_FKregister_id'];

                            $to_id = $newarry[$i]['fetch_id']; //$result1[$i]['to_FKregister_id'];
                            //retrieve name

                            $this->db->select('name');

                            $this->db->where('fk_user_app_info_id', $to_id);

                            $query = $this->db->get('users_personal_info');

                            $resultname = $query->result_array();

                            if (count($resultname) > 0) {

                                $newarry[$i]['name'] = $resultname['0']['name'];
                            } else {

                                $newarry[$i]['name'] = "";
                            }

                            //retrieving phone_no added 28th no 2013
                            $this->db->select('phone_no');

                            $this->db->where('pk_user_app_info_id', $to_id);

                            $query = $this->db->get(TABLE_USER_MASTER);

                            $resultphoneno = $query->result_array();

                            if (count($resultphoneno) > 0) {

                                $newarry[$i]['phone_no'] = $resultphoneno['0']['phone_no'];
                            } else {

                                $newarry[$i]['phone_no'] = "";
                            }


                            //ends here
                            $newarry[$i]['fetch_flag'] = $result1[$i]['fetch_flag'];

                            $newarry[$i]['status'] = $result1[$i]['fetch_status'];

                            $newarry[$i]['fetch_date'] = $result1[$i]['fetch_datetime'];

                            $newarry[$i]['fetch_location'] = $result1[$i]['fetch_location_name'];



                            $imageurl = 'http://www.fetch-apps.com/Fetch/app/images/';

                            $filename = $imageurl . $newarry[$i]['fetch_id'] . '.jpg';

                            $d = @get_headers($filename);

                            if ($d[0] == 'HTTP/1.1 404 Not Found') {

                                // $exists = false;
                                $newarry[$i]['imageurl'] = "";
                            } else {

                                // $exists = true;
                                $newarry[$i]['imageurl'] = "http://www.fetch-apps.com/Fetch/app/images/" . $newarry[$i]['fetch_id'] . ".jpg";
                            }
                        }
                        //echo "<pre>"; print_r($newarry); echo "</pre>"; 

                        $result['Result']['ErrorCode'] = '0';

                        $result['Result']['ErrorMessage'] = "Success";

                        $result['Result']['history'] = $newarry;

                        echo json_encode($result);
                    } else {

                        $result['Result']['ErrorCode'] = '-2';

                        $result['Result']['ErrorMessage'] = "No Records found";

                        echo json_encode($result);
                    }
                } else {

                    $result['Result']['ErrorCode'] = '2';

                    $result['Result']['ErrorMessage'] = "Invalid Input parameters";

                    echo json_encode($result);
                }
            } else {
                $result['Result']['ErrorCode'] = '3';
                $result['Result']['ErrorMessage'] = "userid and page_no are Mandatory";
                echo json_encode($result);
            }
        } catch (Exception $e) {
            $result['Result']['ErrorCode'] = '-4';
            $result['Result']['ErrorMessage'] = "Generic Error";
            echo json_encode($result);
        }
    }

    //retrieve edit profile data
    function retrieveeditprofile($userid) {
        try {
            if ($userid != "") {
                if (is_numeric($userid)) {
                    $newarry = array();

                    //retrieve email and phone_no
                    $this->db->select('*');

                    $this->db->where('pk_user_app_info_id', $userid);
                    $query = $this->db->get(TABLE_USER_MASTER);
                    $result1 = $query->result_array();

                    if (count($result1) > 0) {
                        $email = $result1[0]['email_id'];
                        $phone = $result1[0]['phone_no'];

                        $this->db->select('*');
                        $this->db->where('fk_user_app_info_id', $userid);
                        $query = $this->db->get(PERSONAL_INFO);
                        $result2 = $query->result_array();

                        if (count($result2) > 0) {
                            $name = $result2[0]['name'];
                            $last_name = $result2[0]['last_name'];
                            $city = $result2[0]['city'];
                            $gender = $result2[0]['gender'];
                            $status = $result2[0]['status'];
                            $dob = $result2[0]['dob'];
                            $carRegistrationNo = $result2[0]['carRegistrationNo'];
                            $carLicense = $result2[0]['carLicense'];
                            $carModel = $result2[0]['carModel'];
                            $carColor = $result2[0]['carColor'];
                            $carMake = $result2[0]['carMake'];
                            $carPhoto = $result2[0]['carPhoto'];
                        } else {
                            $name = "";
                            $city = "";
                            $gender = "";
                            $status = "";
                            $dob = "";
                            $carRegistrationNo = "";
                            $carLicense = "";
                            $carModel = "";
                            $carColor = "";
                            $carMake = "";
                            $carPhoto = "";
                        }

                        $newarry[0]['name'] = $name;
                        $newarry[0]['last_name'] = $last_name;
                        $newarry[0]['email'] = $email;
                        $newarry[0]['city'] = $city;
                        $newarry[0]['mobile'] = $phone;
                        $newarry[0]['gender'] = $gender;
                        //$newarry[0]['status'] = $status;
                        $newarry[0]['dob'] = $dob;
                        $newarry[0]['carRegistrationNo'] = $carRegistrationNo;
                        $newarry[0]['carLicense'] = $carLicense;
                        $newarry[0]['carModel'] = $carModel;
                        $newarry[0]['carColor'] = $carColor;
                        $newarry[0]['carMake'] = $carMake;
//                        $newarry[0]['carPhoto'] = $carPhoto;
                        $newarry[0]['carPhotoUrl']=  base_url()."/user_car_images/".$carPhoto;



                        $this->load->model('Commfuncmodel');

                        $newarry[0]['imageurl'] = $this->Commfuncmodel->getUserImage($userid);
                        $imgurl = $this->Commfuncmodel->getUserImage($userid);

                        $result['Result']['ErrorCode'] = '0';
                        $result['Result']['ErrorMessage'] = "Success";
                        if ($email == "" && $name == "" && $city == "" && $phone == "" && $gender == "" && $dob == "" && $imgurl == "") {
                            $result['Result']['isProfileSet'] = 0;
                        } else {
                            $result['Result']['isProfileSet'] = 1;
                        }
                        $result['Result']['data'] = $newarry;
                        echo json_encode($result);
                    } else {
                        $result['Result']['ErrorCode'] = '-2';
                        $result['Result']['ErrorMessage'] = "No Records found";
                        echo json_encode($result);
                    }
                } else {
                    $result['Result']['ErrorCode'] = '2';
                    $result['Result']['ErrorMessage'] = "Invalid Input parameters";
                    echo json_encode($result);
                }
            } else {
                $result['Result']['ErrorCode'] = '3';
                $result['Result']['ErrorMessage'] = "userid is Mandatory";
                echo json_encode($result);
            }
        } catch (Exception $e) {
            $result['Result']['ErrorCode'] = '-4';
            $result['Result']['ErrorMessage'] = "Generic Error";
            echo json_encode($result);
        }
    }

    //schedular
    function schedular($userid, $time_zone) {
        try {
            if ($userid != "") {
                if (is_numeric($userid)) {
                    $date = date("Y-m-d");
                    //retrieve data
                    $select = 'fetch_id, fetch_flag, fetch_creator_id,fetch_location_name,fetch_to_lat,fetch_to_longi,fetch_datetime,fetch_timezone,fetch_message,fetch_image';
                    $this->db->select(TABLE_FETCH_TRANSACTION . '.*')->distinct();
                    $status = array('0', '1', '2', '3', '4');
                    $this->db->where_in('fetch_status', $status);
//                    $this->db->where('DATE_FORMAT(fetch_datetime,"%Y-%m-%d") >= 2014-05-23');
                    $this->db->where('DATE_FORMAT(fetch_datetime,"%Y-%m-%d") >= "' . $date . '"');
                    $this->db->where('(fetch_creator_id=' . $userid . ' OR ftu_user_id=' . $userid . ')');
                    $this->db->join(TABLE_FETCH_TRANSACTION_USERS, TABLE_FETCH_TRANSACTION . '.fetch_id = ' . TABLE_FETCH_TRANSACTION_USERS . '.ftu_transaction_id', 'left');
                    $this->db->order_by('fetch_datetime', 'asc');
                    $query = $this->db->get('fetch_transaction_new');
                    $result1 = $query->result_array();

                    //echo "<pre>"; print_r($result1)."<hr>"; //die();

                    if (!empty($result1)) {
                        $dir = realpath(DIR_FETCH_PHOTOS);
                        foreach ($result1 as $k => $v) {
                            if ($v['fetch_image']) {
                                $result1[$k]['fetch_image'] = $this->Commfuncmodel->getFetchImage($v['fetch_image']);
                            }
                            $select1 = 'ftu_user_id';
                            $transaction_users = $this->Transactionmodel->getTransactionUsers($v['fetch_id'], $userid, $select1);
                            if ($userid == $v['fetch_creator_id']) {
                                foreach ($transaction_users as $k1 => $v1) {
                                    $user_data = $this->Usermodel->getPersonalInfo($v1['ftu_user_id'], 'name');
                                    $user_data1 = $this->Usermodel->getUserAppInfo($v1['ftu_user_id'], 'phone_no');
                                    //echo "info";print_r($user_data); br(2); //die();
                                    $result1[$k]['fetch_users'][] = array('user_id' => $v1['ftu_user_id'],
                                        'name' => $user_data['name'],
                                        'phone_no' => $user_data1['phone_no'],
                                        'user_image' => $this->Commfuncmodel->getUserImage($v1['ftu_user_id']),
                                    );
                                }
                            } else {
                                $user_data = $this->Usermodel->getPersonalInfo($v['fetch_creator_id'], 'name');
                                $user_data1 = $this->Usermodel->getUserAppInfo($v['fetch_creator_id'], 'phone_no');
                                //We need to display fetch_status according to the viewers response
                                $fetch_status = $this->Transactionmodel->getUserTransactionResponse($v['fetch_id'], $userid);
                                //print_r($fetch_status); die();

                                if ($fetch_status == 1 && $v['fetch_status'] == 4) //If user has 
                                    $result1[$k]['fetch_status'] = '4';
                                else
                                    $result1[$k]['fetch_status'] = $fetch_status;

                                //print_r($result1);die();
                                //echo $v['fetch_id']; die('yyy');
                                $result1[$k]['creator_name'] = $user_data['name'];
                                $result1[$k]['creator_phone'] = $user_data1['phone_no'];
                                $result1[$k]['creator_image'] = $this->Commfuncmodel->getUserImage($v['fetch_creator_id']);
                            }




                            $date_old = new DateTime($v['fetch_datetime'], new DateTimeZone("UTC"));
                            $time_old = $date_old->format('Y-m-d H:i:s');
//                            log_message('ERROR', "OLD TIME".date_default_timezone_get().":" . $time_old);

                            $date_old->setTimezone(new DateTimeZone($time_zone));
                            $time_new = $date_old->format('Y-m-d H:i:s');
//                             log_message('ERROR', "NEW TIME".date_default_timezone_get(). ":" . $time_new);
                            $result1[$k]['fetch_datetime'] = $time_new;
//                            return $time;
                        }

                        $result['Result']['ErrorCode'] = '0';
                        $result['Result']['ErrorMessage'] = "Success";
                        $result['Result']['schedular'] = $result1;
                        echo json_encode($result);
                    }
                    else {
                        $result['Result']['ErrorCode'] = '-2';
                        $result['Result']['ErrorMessage'] = "No Records found";
                        echo json_encode($result);
                    }
                } else {
                    $result['Result']['ErrorCode'] = '2';
                    $result['Result']['ErrorMessage'] = "Invalid Input parameters";
                    echo json_encode($result);
                }
            } else {
                $result['Result']['ErrorCode'] = '3';
                $result['Result']['ErrorMessage'] = "userid is Mandatory";
                echo json_encode($result);
            }
        } catch (Exception $e) {
            $result['Result']['ErrorCode'] = '-4';
            $result['Result']['ErrorMessage'] = "Generic Error";
            echo json_encode($result);
        }
    }

    /*
      function schedular_old($userid)
      {
      try
      {
      if($userid!="")
      {
      if( is_numeric($userid))
      {
      $date = date("Y-m-d");
      //retrieve data
      $this->db->select('*');
      $status= array('0','1','2','3');
      $this->db->where_in('fetch_status',$status);
      $this->db->where('DATE_FORMAT(fetch_datetime,"%Y-%m-%d") > "'.$date.'"');
      $this->db->where('(fetch_creator_id='.$userid.' OR ftu_user_id='.$userid.')');
      $this->db->join('fetch_trasaction_users', 'fetch_transaction_new.fetch_id = fetch_trasaction_users.ftu_transaction_id');
      $query = $this->db->get('fetch_transaction_new');
      $result1 = $query->result_array();
      //echo "<pre>"; print_r($result1); echo "</pre>";

      $newarry = array();

      if(count($result1)>0)

      {
      for($i=0;$i<count($result1);$i++)
      {	//old schedular
      $var = $result1[$i]['fetch_creator_id'];

      if($userid==$var)
      {
      $userrole='0'; //user is fetch creator
      }
      else
      {
      $userrole='1'; //user is fetch user
      }

      //check whether user is fetcher or not and update the output accordingly
      if($userrole==0) //fetch creator
      {
      $newarry[$i]['to_fetch_id'] = $result1[$i]['ftu_user_id'];
      }
      else if($userrole==1)//fetch user
      {
      $newarry[$i]['to_fetch_id'] = $var;
      }
      //$newarry[$i]['fetch_id'] = $result1[$i]['to_FKregister_id'];
      $to_id = $newarry[$i]['to_fetch_id'];

      //retrieve name
      $this->db->select('name');
      $this->db->where('fk_user_app_info_id',$to_id);
      $query = $this->db->get('users_personal_info');
      $resultname = $query->result_array();

      if(count($resultname)>0)
      {
      $newarry[$i]['name']= $resultname['0']['name'];
      }
      else
      {
      $newarry[$i]['name']= "";
      }

      $imageurl='http://www.fetch-apps.com/Fetch/app/images/';
      $filename = $imageurl.$newarry[$i]['to_fetch_id'].'.jpg';

      $d = @get_headers($filename);

      if($d[0] == 'HTTP/1.1 404 Not Found')
      {
      // $exists = false;
      $newarry[$i]['imageurl']="";
      }
      else
      {
      // $exists = true;
      $newarry[$i]['imageurl']="http://www.fetch-apps.com/Fetch/app/images/".$newarry[$i]['to_fetch_id'].".jpg";
      }

      $newarry[$i]['fetch_flag'] = $result1[$i]['fetch_flag'];
      $newarry[$i]['status'] = $result1[$i]['fetch_status'];
      $newarry[$i]['fetch_date'] = $result1[$i]['fetch_creation_date'];
      $newarry[$i]['fetch_location'] = $result1[$i]['fetch_location_name'];

      //$newarry[$i]['imageurl']="http://www.fetch-apps.com/Fetch/app/images/".$newarry[$i]['to_fetch_id'].".jpg";
      }

      $result['Result']['ErrorCode']='0';
      $result['Result']['ErrorMessage']="Success";
      $result['Result']['schedular']=$newarry;
      echo json_encode($result);
      }
      else
      {
      $result['Result']['ErrorCode']='-2';
      $result['Result']['ErrorMessage']="No Records found";
      echo json_encode($result);
      }
      }
      else
      {
      $result['Result']['ErrorCode']='2';
      $result['Result']['ErrorMessage']="Invalid Input parameters";
      echo json_encode($result);
      }
      }
      else
      {
      $result['Result']['ErrorCode']='3';
      $result['Result']['ErrorMessage']="userid is Mandatory";
      echo json_encode($result);
      }
      }
      catch (Exception $e)
      {
      $result['Result']['ErrorCode']='-4';
      $result['Result']['ErrorMessage']="Generic Error";
      echo json_encode($result);
      }
      }
     */

    function initiatefetchrequest($fetch_id) {
        //initiate the fetch transaction
        $data = array('fetch_status' => 4);

        $this->db->where('fetch_id', $fetch_id);
        if ($this->db->update('fetch_transaction_new', $data)) {
            $result['Result']['ErrorCode'] = '0';
            $result['Result']['ErrorMessage'] = "Success";
            echo json_encode($result);
        } else {
            $result['Result']['ErrorCode'] = '1';
            $result['Result']['ErrorMessage'] = "Failure";
            echo json_encode($result);
        }
    }

    function send_notification_android($registration_ids, $message) {
        //echo $registatoin_ids.",". $message."<br>";
        // Set POST variables

        $url = 'https://android.googleapis.com/gcm/send';



        $fields = array(
            'registration_ids' => $registration_ids,
            'data' => $message,
        );



        $headers = array(
            'Authorization: key = AIzaSyBR2HmV443r7FluNLjSRQqI49xtwIYK29A',
            'Content-Type: application/json'
        );



        // Open connection

        $ch = curl_init();



        // Set the url, number of POST vars, POST data

        curl_setopt($ch, CURLOPT_URL, $url);



        curl_setopt($ch, CURLOPT_POST, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);



        // Disabling SSL Certificate support temporarly

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);



        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        //echo $ch;
        // Execute post

        $result = curl_exec($ch);

        if ($result === FALSE) {

            die('Curl failed: ' . curl_error($ch));
        }



        // Close connection

        curl_close($ch);

        //echo $result;
    }

    function schedular_new($userid, $last_updated_time) {
        try {
            if ($userid != "") {
                if (is_numeric($userid)) {
                    $date = date("Y-m-d");
                    //retrieve data
                    $select = 'fetch_id, fetch_flag, fetch_creator_id,fetch_location_name,fetch_to_lat,fetch_to_longi,fetch_datetime,fetch_timezone,fetch_message,fetch_image';
                    $this->db->select(TABLE_FETCH_TRANSACTION . '.*')->distinct();
                    $status = array('0', '1', '2', '3', '4');
                    $this->db->where_in('fetch_status', $status);
                    $this->db->where('DATE_FORMAT(fetch_datetime,"%Y-%m-%d") > "' . $date . '"');
                    $this->db->where('DATE_FORMAT(fetch_updation_date,"%Y-%m-%d H:i:s") >= "' . $last_updated_time . '"');
                    $this->db->where('(fetch_creator_id=' . $userid . ' OR ftu_user_id=' . $userid . ')');
                    $this->db->join(TABLE_FETCH_TRANSACTION_USERS, TABLE_FETCH_TRANSACTION . '.fetch_id = ' . TABLE_FETCH_TRANSACTION_USERS . '.ftu_transaction_id', 'left');
                    $this->db->order_by('fetch_datetime', 'asc');
                    $query = $this->db->get('fetch_transaction_new');
                    $result1 = $query->result_array();

                    if (!empty($result1)) {
                        $dir = realpath(DIR_FETCH_PHOTOS);
                        foreach ($result1 as $k => $v) {
                            if ($v['fetch_image']) {
                                $result1[$k]['fetch_image'] = $this->Commfuncmodel->getFetchImage($v['fetch_image']);
                            }
                            $select1 = 'ftu_user_id';
                            $transaction_users = $this->Transactionmodel->getTransactionUsers($v['fetch_id'], $userid, $select1);
                            if ($userid == $v['fetch_creator_id']) {
                                foreach ($transaction_users as $k1 => $v1) {
                                    $user_data = $this->Usermodel->getPersonalInfo($v1['ftu_user_id'], 'name');
                                    $user_data1 = $this->Usermodel->getUserAppInfo($v1['ftu_user_id'], 'phone_no');
                                    //echo "info";print_r($user_data); die();
                                    $result1[$k]['fetch_users'][] = array('user_id' => $v1['ftu_user_id'],
                                        'name' => $user_data['name'],
                                        'phone_no' => $user_data1['phone_no'],
                                        'user_image' => $this->Commfuncmodel->getUserImage($v1['ftu_user_id']),
                                    );
                                }
                            } else {
                                $user_data = $this->Usermodel->getPersonalInfo($v['fetch_creator_id'], 'name');
                                $user_data1 = $this->Usermodel->getUserAppInfo($v['fetch_creator_id'], 'phone_no');
                                //We need to display fetch_status according to the viewers response
                                $fetch_status = $this->Transactionmodel->getUserTransactionResponse($v['fetch_id'], $userid);
                                //print_r($fetch_status); die();
                                $result1[$k]['fetch_status'] = $fetch_status;
                                //print_r($result1);die();
                                //echo $v['fetch_id']; die('yyy');
                                $result1[$k]['creator_name'] = $user_data['name'];
                                $result1[$k]['creator_phone'] = $user_data1['phone_no'];
                                $result1[$k]['creator_image'] = $this->Commfuncmodel->getUserImage($v['fetch_creator_id']);
                            }
                        }

                        $result['Result']['ErrorCode'] = '0';
                        $result['Result']['ErrorMessage'] = "Success";
                        $result['Result']['schedular'] = $result1;
                        echo json_encode($result);
                    } else {
                        $result['Result']['ErrorCode'] = '-2';
                        $result['Result']['ErrorMessage'] = "No Records found";
                        echo json_encode($result);
                    }
                } else {
                    $result['Result']['ErrorCode'] = '2';
                    $result['Result']['ErrorMessage'] = "Invalid Input parameters";
                    echo json_encode($result);
                }
            } else {
                $result['Result']['ErrorCode'] = '3';
                $result['Result']['ErrorMessage'] = "userid is Mandatory";
                echo json_encode($result);
            }
        } catch (Exception $e) {
            $result['Result']['ErrorCode'] = '-4';
            $result['Result']['ErrorMessage'] = "Generic Error";
            echo json_encode($result);
        }
    }

    function contactNew($fromfetchid, $phonearray) {
        try {
            if ($phonearray != "" && $fromfetchid != "") {
                if (is_numeric($fromfetchid)) {
                    $this->db->select('pk_user_app_info_id');
                    $this->db->where_in('pk_user_app_info_id', $fromfetchid);
                    $query = $this->db->get(TABLE_USER_MASTER);
                    $checkid = $query->result_array();
                    if (count($checkid) > 0) {
                        $ary = explode(',', $phonearray);
                        //echo "<pre> user input";print_r($ary);echo "</pre>"; die();
                        $infetch[] = "";
                        //infetch
                        //code added by ashwini on 22nd nov 2013
                        $twodigit = array();
                        $tworemain = array();
                        $threedigit = array();
                        $threeremain = array();
                        foreach ($ary as $arrayk) {
                            $twodigit[] = substr($arrayk, 0, 2);
                            $tworemain[] = substr($arrayk, 2);
                            $threedigit[] = substr($arrayk, 0, 3);
                            $threeremain[] = substr($arrayk, 3);
                        }
                        //code ends here
                        $this->db->select('pk_user_app_info_id,phone_no');
                        $this->db->where_in('phone_no', $ary);
                        $this->db->or_where_in('country_code', $twodigit)->where_in('phone_no', $tworemain);
                        $this->db->or_where_in('country_code', $threedigit)->where_in('phone_no', $threeremain);
                        $query = $this->db->get(TABLE_USER_MASTER);
                        $infetch = $query->result_array();
                        if (count($infetch) > 0) {
                            $flag = 0;
                        } else {
                            $infetch[] = "";
                        }

                        // retrieve only phone no to array_map reset
                        $this->db->select('phone_no');
                        $this->db->where_in('phone_no', $ary);
                        $this->db->or_where_in('country_code', $twodigit)->where_in('phone_no', $tworemain);
                        $this->db->or_where_in('country_code', $threedigit)->where_in('phone_no', $threeremain);
                        $query = $this->db->get(TABLE_USER_MASTER);
                        $infetchres = $query->result_array();
                        $ary2 = array_map("reset", $infetchres);
                        //echo "***arraymap infetch ***";echo "<pre>";print_r($infetch);echo "</pre>";
                        $newarray[] = "";
                        if (count($ary2) > 0) {
                            $this->db->select('pk_user_app_info_id');
                            $this->db->where_in('phone_no', $ary2);
                            $this->db->or_where_in('country_code', $twodigit)->where_in('phone_no', $tworemain);
                            $this->db->or_where_in('country_code', $threedigit)->where_in('phone_no', $threeremain);
                            $query = $this->db->get(TABLE_USER_MASTER);
                            $blockedid = $query->result_array();
                            $blockedidres = array_map("reset", $blockedid);

                            $this->db->select('fk_user_app_info_pal_id');
                            $this->db->where('fk_user_app_info_id', $fromfetchid);
                            $this->db->where_in('fk_user_app_info_pal_id', $blockedidres);
                            $query = $this->db->get(BLOCKED_PALS);
                            $res3 = $query->result_array();
                            $blockedidfinal = array_map("reset", $res3); //checking present in blocked table
                            $res4 = array();
                            if (count($res3) > 0) {
                                $this->db->select('pk_user_app_info_id,phone_no');
                                $this->db->where_in('pk_user_app_info_id', $blockedidfinal);
                                $query = $this->db->get(TABLE_USER_MASTER);
                                $res4 = $query->result_array();
                            } else {
                                //$res4[]="";
                                $res4 = array();
                            }
                        } else {
                            $notinfetch = $ary2;
                            $ary2[] = "";
                            $res4[] = "";
                        }

                        $newinfetch = array();
                        $s = 0;

                        if (!empty($infetch)) {
                            foreach ($infetch as $dbinfetch) {
                                foreach ($ary as $userinputarray) {
                                    $tworemain = substr($userinputarray, 2);
                                    $threeremain = substr($userinputarray, 3);
                                    $phone_num = $dbinfetch['phone_no'];
                                    if ($phone_num == $userinputarray) {
                                        $newinfetch[$s]['pk_user_app_info_id'] = $dbinfetch['pk_user_app_info_id'];
                                        $newinfetch[$s]['phone_no'] = $userinputarray;
                                        //echo "phone number equals = "; print_r($newinfetch);
                                        $s++;
                                        break;
                                    } elseif ($phone_num == $tworemain) {
                                        $newinfetch[$s]['pk_user_app_info_id'] = $dbinfetch['pk_user_app_info_id'];
                                        $newinfetch[$s]['phone_no'] = $userinputarray;
                                        //echo "two phone number equals = "; print_r($newinfetch);
                                        $s++;
                                        break;
                                    } elseif ($phone_num == $threeremain) {
                                        $newinfetch[$s]['pk_user_app_info_id'] = $dbinfetch['pk_user_app_info_id'];
                                        $newinfetch[$s]['phone_no'] = $threeremain;
                                        $s++;
                                        break;
                                    }
                                }
                            }
                        } else {
                            $newinfetch = array();
                        }

                        $newblocked = array();
                        $k = 0;

                        if (!empty($res4) && !empty($ary)) {
                            foreach ($res4 as $dbinblocked) {
                                //print_r($dbinblocked['phone_no']);die();
                                foreach ($ary as $userinputarray) {
                                    $tworemain = substr($userinputarray, 2);
                                    $threeremain = substr($userinputarray, 3);
                                    $phone_blocked = $dbinblocked['phone_no'];
                                    if ($phone_blocked == $userinputarray) {
                                        $newblocked[$k]['pk_user_app_info_id'] = $dbinblocked['pk_user_app_info_id'];
                                        $newblocked[$k]['phone_no'] = $userinputarray;
                                        //echo "phone number equals = "; print_r($newinfetch);
                                        $k++;
                                        break;
                                    } elseif ($phone_blocked == $tworemain) {
                                        $newblocked[$k]['pk_user_app_info_id'] = $dbinblocked['pk_user_app_info_id'];
                                        $newblocked[$k]['phone_no'] = $userinputarray;
                                        //echo "two phone number equals = "; print_r($newinfetch);
                                        $k++;
                                        break;
                                    } elseif ($phone_blocked == $threeremain) {
                                        $newblocked[$k]['pk_user_app_info_id'] = $dbinblocked['pk_user_app_info_id'];
                                        $newblocked[$k]['phone_no'] = $threeremain;
                                        $k++;
                                        break;
                                    }
                                }
                            }
                        } else {
                            $newblocked = array();
                        }

                        $infetcharray = array_map("reset", $newinfetch);
                        $blockedarray = array_map("reset", $newblocked);
                        $diff = array_diff($infetcharray, $blockedarray);
                        $n = array_values($diff);
                        //print_r($n);
                        $new_infetch_array = array();
                        foreach ($newinfetch as $new_infetch) {
                            if (in_array($new_infetch['pk_user_app_info_id'], $n)) {
                                $new_infetch_array[] = $new_infetch;
                            }
                        }
                        $result['ErrorCode'] = '0';
                        $result['ErrorMessage'] = "Success";
                        $result['in_fetch'] = $new_infetch_array;
                        $result['blocked_contacts'] = $newblocked;
                        $new['Result'] = array($result);
                        echo json_encode($new);
                    } else {
                        $result['ErrorCode'] = '-2';
                        $result['ErrorMessage'] = "No Records found";
                        $new['Result'] = array($result);
                        echo json_encode($new);
                    }
                } else {
                    $result['ErrorCode'] = '2';
                    $result['ErrorMessage'] = "Invalid Input parameters";
                    $new['Result'] = array($result);
                    echo json_encode($new);
                }
            } else {
                $result['ErrorCode'] = '3';
                $result['ErrorMessage'] = "from_fetch_id,phone_array is Mandatory";
                $new['Result'] = array($result);
                echo json_encode($new);
            }
        } catch (Exception $e) {
            $result['ErrorCode'] = '-4';
            $result['ErrorMessage'] = "Generic Error";
            $new['Result'] = array($result);
            echo json_encode($new);
        }
    }

    function checkDeviceId($userid, $devicetype, $devicereg) {
        //echo "hie";
        $this->db->select('fk_user_app_info_id');
        $this->db->where('fk_user_app_info_id !=', $userid)->where('device_type', $devicetype)->where('phone_registration_id', $devicereg);
        $query = $this->db->get(USER_DEVICE);
        $res = $query->result_array();
        $old_userid = array();
        $res3 = array();
        if (count($res) >= 1) {
            $old_userid = array_map("reset", $res);
            //print_r($old_userid);
            $data_update = array('phone_registration_id' => '');
            $this->Usermodel->updateUserDevice($old_userid, $data_update);
        }

        $this->db->select('fk_user_app_info_id');
        $this->db->where('fk_user_app_info_id', $userid);
        $query = $this->db->get(USER_DEVICE);
        $res3 = $query->result_array();
        if (count($res3) > 0) {
            $data = array('device_type' => $devicetype,
                'is_logged_in' => 1,
                'phone_registration_id' => $devicereg
            );

            $this->db->where('fk_user_app_info_id', $userid);
            $this->db->update(USER_DEVICE, $data);
        } else {
            $data = array('device_type' => $devicetype,
                'is_logged_in' => 1,
                'phone_registration_id' => $devicereg,
                'fk_user_app_info_id' => $userid
            );

            $this->db->insert(USER_DEVICE, $data);
        }
    }

    //forgot password updated
    function forgotPassword($em, $flag) {
        try {
            if ($em != "" && $flag != "") {
                $result = array();
                $this->db->select('*');

                if ($flag == "e") {
                    $this->db->where('email_id', $em);
                } else if ($flag == "m") {
                    $this->db->where('phone_no', $em);
                }

                $query = $this->db->get(TABLE_USER_MASTER);
                $res = $query->result_array();

                if (count($res) > 0) {
                    $email = $res[0]['email_id'];
                    $pass = $res[0]['password'];
                    $pwd = base64_decode($pass);


                    // subject
                    $subject = 'Your Password for fetch';

                    // message
                    $message = "Your password is '" . $pwd . "'";

                    // To send HTML mail, the Content-type header must be set
                    $headers = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                    // Additional headers
                    $headers .= '';

                    // Mail it
                    if ($pwd != "") {
                        if (mail($email, $subject, $message, $headers)) {
                            $result['Result']['ErrorCode'] = '0';
                            $result['Result']['ErrorMessage'] = "Success";
                        } else {
                            $result['Result']['ErrorCode'] = '1';
                            $result['Result']['ErrorMessage'] = "Failure";
                        }
                    } else {
                        $result['Result']['ErrorCode'] = '1';
                        $result['Result']['ErrorMessage'] = "Failure";
                    }
                } else {
                    $result['Result']['ErrorCode'] = '-2';
                    $result['Result']['ErrorMessage'] = "No Records found";
                }
            } else {
                $result['Result']['ErrorCode'] = '3';
                $result['Result']['ErrorMessage'] = "em and flag are Mandatory";
            }
            echo json_encode($result);
        } catch (Exception $e) {
            $result['Result']['ErrorCode'] = '-4';
            $result['Result']['ErrorMessage'] = "Generic Error";
            echo json_encode($result);
        }
    }

    function registerUser($data_insert) {
        $phone = $data_insert['phone_no'];
        $email = $data_insert['email_id'];

        //checking for duplicate phone numbers
        $this->db->select('pk_user_app_info_id');
        $this->db->where('phone_no', $phone);
        $query = $this->db->get(TABLE_USER_MASTER);
        $res1 = $query->result_array();

        //checking for duplicate email
        $this->db->select('pk_user_app_info_id');
        $this->db->where('email_id', $email);
        $query = $this->db->get(TABLE_USER_MASTER);
        $res2 = $query->result_array();

        if (count($res1) > 0 && count($res2) > 0) {
            //both
            return 0;
        } elseif (count($res1) > 0) {
            // mobile
            return 1;
        } elseif (count($res2) > 0) {
            //email
            return -1;
        } else {
            $this->db->insert(TABLE_USER_MASTER, $data_insert);
            $user_id = $this->db->insert_id();
            return $user_id;
        }
    }

    function log_err($user_id, $device_id, $err_msg, $timezone) {
        $error_id = null;
        // subject
        $subject = 'User App crash error';
        $email = 'nachare.reena8@gmail.com';

        $message = null;
        $date = date('m/d/Y h:i:s a', time());
        // To send HTML mail, the Content-type header must be set
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        // Additional headers
        $headers .= '';
        $data = array(
            'user_id' => $user_id,
            'device_id' => $device_id,
            'err_msg' => $err_msg,
            'timezone' => $timezone,
        );
        try {
            $this->db->insert('tbl_err_log', $data);
            $error_id = $this->db->insert_id();

            // message
        } catch (Exception $ae) {

            log_message('ERROR', $ae->getMessage());
        }
        $message = "Error No ($error_id) (" . $date . ") <br> App Failed on Device Id (<b>$device_id</b>) registerd by UserId (<b>$user_id</b>) . <br> "
                . "<p><h3>Message :</h3>$err_msg.<br> Timezone:$timezone .</p>";
        $message_log = 'err_no(' . $error_id . '), \n\r App Failed on device_id(' . $device_id . ') registerd by UserId(' . $user_id . ")"
                . "\n\r TimeZone($timezone)\n\r Error MSG(" . $date . ")($err_msg) ";

        if (mail($email, $subject, $message, $headers)) {
            log_message('ERROR', $message_log);
            return true;
        } else {
            return false;
        }
    }

}

?>