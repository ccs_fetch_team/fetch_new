<?php

class Fetch extends CI_Controller 

{

	function __Construct()

	{

		parent::__Construct();

	}
	
	public function login()
	{
		$this->load->view('login.php');
	}
	public function registration()
	{
		$this->load->view('registration.php');
	}
	public function socialregistration()
	{
		$this->load->view('socialregistration.php');
	}
	public function activation()
	{
		$this->load->view('activation.php');
	}

	public function forgot()
	{
		$this->load->view('forgot.php');
	}
	public function resend()
	{
		$this->load->view('resend.php');
	}
	public function upload()
	{
		$this->load->view('upload.php');
	}
	public function contact()
	{
		$this->load->view('contact.php');
	}
	public function createfetchrequest()
	{
		$this->load->view('createfetchrequest.php');
	}
	public function blockcontact()
	{
		$this->load->view('blockcontact');
	}
	public function favouritecontact()
	{
		$this->load->view('favouritecontact');
	}
	public function editprofile()
	{
		$this->load->view('editprofile.php');
	}
	public function ETArefresh()
	{
		$this->load->view('ETArefresh.php');
	}
	public function fetchhistory()
	{
		$this->load->view('fetchhistory.php');
	}
	public function retrieveeditprofile()
	{
		$this->load->view('retrieveeditprofile.php');
	}
	public function schedular()
	{
		$this->load->view('schedular.php');	
	}
	public function initiatefetchrequest()
	{
		$this->load->view('initiatefetchrequest.php');	
	}
	public function testnotification()
	{
		$this->load->view('testnotification.php');	
	}
	public function callogin()
	{
    	$this->load->model('Fetchmodel');
		$em = $_POST['em'];
		$password = $_POST['password'];
		$devicereg = $_POST['devicereg'];
		$devicetype =  $_POST['devicetype'];
		$flag = $_POST['flag'];
		$res = $this->Fetchmodel->login($em,$password,$devicereg,$devicetype,$flag);
		print_r($res);
	}
	public function callregister()
	{
	   	$this->load->model('Fetchmodel');
		$phone = $_POST['phone'];
		$name = $_POST['name'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		$countrycode= $_POST['countrycode'];
		$city = $_POST['city'];
		$gender = $_POST['gender'];
		$dob = $_POST['dob'];
		$fbid= $_POST['fbid'];
		$accesstoken= $_POST['accesstoken'];
		$res = $this->Fetchmodel->registration($phone,$countrycode,$name,$email,$password,$city,$gender,$dob,$fbid,$accesstoken);
		print_r($res);
	}
	public function callsocialregister()
	{
	   	$this->load->model('Fetchmodel');
		$phone = $_POST['phone'];
		$name = $_POST['name'];
		$email = $_POST['email'];
		$countrycode= $_POST['countrycode'];
		$city = $_POST['city'];
		$id = $_POST['id'];
		$flag = $_POST['flag'];
		$res = $this->Fetchmodel->socialregistration($phone,$countrycode,$name,$email,$city,$id,$flag);
		print_r($res);
	}
	function callupload()
	{
		$userid=$_POST['userid'];
		$this->load->model('Fetchmodel');
		$res = $this->Fetchmodel->upload($userid);
		print_r($res);
	}
   	public function callactivation()
	{
		$phone = $_POST['phone'];
		$code = $_POST['code'];
		$devicereg = $_POST['devicereg'];
		$devicetype =  $_POST['devicetype'];
		$this->load->model('Fetchmodel');
		$res = $this->Fetchmodel->activation($code,$phone,$devicereg,$devicetype);
		print_r($res);
	}

    public function callforgot()
	{
		$em = $_POST['em'];
		$flag = $_POST['flag'];
		$this->load->model('Fetchmodel');
		$res = $this->Fetchmodel->forgot($em,$flag);
		print_r($res);
	}
	
	public function callresend()
	{
		$phone = $_POST['phone'];
		$this->load->model('Fetchmodel');
		$res = $this->Fetchmodel->resendcode($phone);
		print_r($res);
	}
	
	public function callcontact()
	{
		$fromfetchid = $_POST['from_fetch_id'];
		$phonearray = $_POST['phone_array'];
		$this->load->model('Fetchmodel');
		$res = $this->Fetchmodel->contact($fromfetchid,$phonearray);
		print_r($res);
	}
	public function callcreatefetchrequest()
	{
		$fromfetchid = $_POST['from_fetch_id'];
		$tofetchid = $_POST['to_fetch_id'];
		$datetime = $_POST['datetime'];
		$fromlat = $_POST['fromlat'];
		$fromlong = $_POST['fromlong'];
		$fromaddress = $_POST['fromaddress'];
		$this->load->model('Fetchmodel');
		$res = $this->Fetchmodel->createfetchrequest($fromfetchid,$tofetchid,$datetime,$fromlat,$fromlong,$fromaddress);
		print_r($res);
	}
	public function callblockcontact()
	{
		$fromfetchid = $_POST['from_fetch_id'];
		$tofetchid = $_POST['to_fetch_id'];
		$this->load->model('Fetchmodel');
		$res = $this->Fetchmodel->blockcontact($fromfetchid,$tofetchid);
		print_r($res);
	}
	public function callfavouritecontact()
	{
		$fromfetchid = $_POST['from_fetch_id'];
		$tofetchid = $_POST['to_fetch_id'];
		$this->load->model('Fetchmodel');
		$res = $this->Fetchmodel->favouritecontact($fromfetchid,$tofetchid);
		print_r($res);
	}
	public function callretrieveeditprofile()
	{
		$userid = $_POST['userid'];
		$this->load->model('Fetchmodel');
		$res = $this->Fetchmodel->retrieveeditprofile($userid);
		print_r($res);	
	}
	public function calleditprofile()
	{
		$userid = $_POST['userid'];
		$name = $_POST['name'];
		$status = $_POST['status'];
		$phone = $_POST['phone'];
		$email = $_POST['email'];
		$dob = $_POST['dob'];
		$gender = $_POST['gender'];
		$city = $_POST['city'];
		
					
		$this->load->model('Fetchmodel');
		$res = $this->Fetchmodel->editprofile($userid,$name,$status,$phone,$email,$dob,$gender,$city);
		print_r($res);
	}
	public function callETArefresh()
	{
		$fromfetchid = $_POST['from_fetch_id'];
		$tofetchid = $_POST['to_fetch_id'];
		$time = $_POST['time'];
		$this->load->model('Fetchmodel');
		$res = $this->Fetchmodel->ETArefresh($fromfetchid,$tofetchid,$time);
		print_r($res);
	}
	public function callfetchhistory()
	{
		$userid = $_POST['userid'];
		$page_no = $_POST['page_no'];
		$this->load->model('Fetchmodel');
		$res = $this->Fetchmodel->fetchhistory($userid,$page_no);
		print_r($res);
	}
	public function callschedular()
	{
		$userid = $_POST['userid'];
		$this->load->model('Fetchmodel');
		$res = $this->Fetchmodel->schedular($userid);
		print_r($res);
	}
	public function callinitiatefetchrequest()
	{
		$fetch_id= $_POST['fetch_id'];
		$this->load->model('Fetchmodel');
		$res = $this->Fetchmodel->initiatefetchrequest($fetch_id);
		print_r($res);
	}
	public function calltestnotification()
	{
		$regid = $_POST['regid'];
		$registrationids = array($regid);
		$msg = $_POST['msg'];
		$devicetype= $_POST['devicetype'];
		if($devicetype==0)
		{
			$message="You have a new fetch request from ".$msg."";
			$message = array("price" => $message); 
				        			
			$this->load->model('Fetchmodel');
			$res = $this->Fetchmodel->send_notification_android($registrationids, $message);
			print_r($res);
		}
		else if ($devicetype==1)
		{
			$message="You have a new fetch request from ".$msg."";
			$message = array("price" => $message); 
				        			
			$this->load->model('Fetchmodel');
			$res = $this->Fetchmodel->send_ios_notification($registrationids, $message);
			print_r($res);
		}
		
		
	}
}
?>